<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: comment.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
class public_controller extends console_controller
{
	function init(){
      parent::init();
    }
    function top_action()
	{
		include $this->bidcms_template('topframe');
	}
	function left_action()
	{
		include $this->bidcms_template('leftframe');
	}
	function switch_action()
	{
		include $this->bidcms_template('switchframe');
	}
	function main_action()
	{
		include $this->bidcms_template('mainframe');
	}
}