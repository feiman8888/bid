<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: comment.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
class email_controller extends console_controller
{
	function init(){
      parent::init();
      $this->dir = ROOT_PATH.'data/email';
    }
	function index_action()
	{
		if($this->bidcms_submit_check('commit'))
		{
			$subject=trim($this->post['subject']);
			$content=trim($this->post['content']);
			$email=explode("\n",str_replace("\r\n","\n",$_POST['emaillist']));
			foreach($email as $v)
			{
				if(!empty($v))
				{
					$tag=array('{email_user}','{email_date}','{site_root}','{site_name}');
					$sou=array(trim($v),date('Y-m-d H:i:s'),SITE_ROOT,$GLOBALS['setting']['site_title']);
					$content=str_replace($tag,$sou,$content);
					$this->email(trim($v),$subject,$content);
				}
			}
			sheader('index.php?con=email',3,'邮件发送完毕','redirect',true);
		}
		else
		{
			
			$template=scandir($this->dir);
			
			foreach($template as $v)
			{
				if($v!='.'&& $v!='..'&& is_file($this->dir.'/'.$v))
				{
					$v=explode('.',$v);
					$temp[]=$v[0];
				}
			}
            include $this->bidcms_template('email_form');
		}
	}
	function emaillist_action()
	{
		$emaillist=$this->bidcms_model('user_base')->get_page();
		
		foreach($emaillist as $v)
		{
			if(strpos($v['email'],'@'))
			{
				$email[]=$v['email'];
			}
		}
		echo implode(',',$email);
		exit;
	}
	function emailcontent_action()
	{
		if(!empty($this->get('file')))
		{
			$nefile=$this->dir.'/'.$this->get('file').'.txt';
			if(is_file($nefile))
			{
				if(function_exists('file_get_contents'))
				{
					echo file_get_contents($nefile);
				}
				else
				{
					$handle = fopen($nefile, "r");
					while (!feof($handle)) {
						$buffer = fgets($fd, 4096);
						echo $buffer;
					}
					fclose($handle);
				}
			}
			else
			{
				echo '文件不存在';
			}
		}
		else
		{
			echo '';
		}
		exit;
	}
}