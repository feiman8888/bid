<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class console_controller extends controller
{
	function init()
	{
        $GLOBALS['session']->set('adminid',1);
        $GLOBALS['setting'] = $this->bidcms_model('system_setting')->get_cache_list();
        $GLOBALS['cate'] = $this->bidcms_model('goods_cate')->get_cache_list(); 
        $GLOBALS['article_cate'] = $this->bidcms_model('article_cate')->get_cache_list(); 
        if($GLOBALS['action']=='admin_ajax'|| $GLOBALS['action']=='admin_delete')
		{
			if(!$GLOBALS['session']->get('adminid'))
			{
				exit('未登录');
			}
		}
		if($GLOBALS['action']!='login'&& $GLOBALS['action']!='logout')
		{

			if(!$GLOBALS['session']->get('adminid'))
			{
				sheader('index.php?con='.$GLOBALS['setting']['adminpath'].'&act=login');
			}
		}
	}
	 //编辑器
	public function edit($content='',$textareaid='content',$textareaname='content',$textwidth='621px',$textheight='457px',$autosave=0,$showtextarea='none')
	{
		$str='<textarea name="'.$textareaname.'" id="'.$textareaid.'" style="display:'.$showtextarea.';">'.$content.'</textarea><iframe src="'.SITE_ROOT.'/editor/editor.htm?id='.$textareaid.'&ReadCookie='.$autosave.'" frameBorder="0" marginHeight="0" marginWidth="0" scrolling="No" width="'.$textwidth.'" height="'.$textheight.'"></iframe>';
		return $str;
	}
	public function post($val='',$default=''){
		return $this->bidcms_request($val,$default,'post');
	}
	public function input($val='',$default=''){
		return $this->bidcms_request($val,$default,'input');
	}
	public function get($val='',$default=''){
		return $this->bidcms_request($val,$default,'get');
	}
    //单个上传
    public function upload($upfile,$uploaddir='',$customfile='',$thumbinfo=array())
    {
        include_once ROOT_PATH.'/inc/classes/upload.class.php';
        $up=new upload($upfile);
        $up->updir=$uploaddir?$uploaddir:$GLOBALS['setting']['site_upload_dir'];
        if($up->checkStatus() && $up->checkType() && $up->checkSize()){
            if($file=$up->execute($customfile))
            {
                if($thumbinfo)
                {
                    $up->setThumb($file,str_replace('.','_s.',$file),$thumbinfo['width'],$thumbinfo['height']);
                }
                $thumb=$file;
            }
        	return $thumb;
        }
    }
    //批量上传
    public function attach($upfile,$uploaddir='',$thumbinfo=array()) {
        $thumb=array();
        if($_FILES)
        {
            include_once ROOT_PATH.'/inc/classes/upload.class.php';
            $count=count($_FILES[$upfile]['name']);
            foreach($_FILES[$upfile] as $k=>$v)
            {
              for($i=0;$i<$count;$i++)
              {
                $f['tmpupload'.$i][$k]=$v[$i];
              }

            }
            $_FILES=$f;
            foreach($f as $k=>$v)
            {
              $up=new upload($k);
              $up->updir=$uploaddir?$uploaddir:$GLOBALS['setting']['site_upload_dir'];
              if($up->checkStatus() && $up->checkType() && $up->checkSize()){
                  if($file=$up->execute())
                  {
                    if($thumbinfo)
                    {
                      $up->setThumb($file,str_replace('.','_s.',$file),$thumbinfo['width'],$thumbinfo['height']);
                    }
                    $thumb[]=$file;
                  }
              }

            }
            return $thumb;
        }
    }
    //发送短信
    function sms($mobile,$content,$code = ''){
      if(!empty($mobile)){
        if(!empty($code)){
        	$this->bidcms_model('sms')->insert_data(array('mobile'=>$mobile,'code'=>$code,'updatetime'=>time()));
        }
        $user = $GLOBALS['setting']['sms_user'];
        $api='http://gateway.woxp.cn:6630/utf8/web_api/';
        $password=md5($GLOBALS['setting']['sms_password']);
        $sms_kind=10783;
        $url=$GLOBALS['setting']['sms_api'].'?x_eid='.$sms_kind.'&x_uid='.$user.'&x_pwd_md5='.$password.'&x_ac=10&x_gate_id=300&x_target_no='.$mobile.'&x_memo='.str_replace(' ','',$content);
        return $GLOBALS['curl']->get($url);
      }
      return false;
    }
    //发送邮件
    //收件人,标题，内容
    function email($smtpemailto,$mailsubject,$mailbody,$mailtype="HTML"){
          include (ROOT_PATH.'/inc/classes/email.class.php');
          $smtpserver = $GLOBALS['setting']['email_smtp'];//SMTP服务器
          $smtpserverport = $GLOBALS['setting']['email_port'];//SMTP服务器端口
          $smtpuser = $GLOBALS['setting']['email_user'];//SMTP服务器的用户帐号
          $smtppass = $GLOBALS['setting']['email_password'];//SMTP服务器的用户密码
          if($smtpserver && $smtpserverport && $smtpuser && $smtppass)
          {
              $smtp = new smtp($smtpserver,$smtpserverport,true,$smtpuser,$smtppass);//这里面的一个true是表示使用身份验证,否则不使用身份验证.
              $smtp->debug = true;//是否显示发送的调试信息
              return $smtp->sendmail($smtpemailto, $GLOBALS['setting']['email'], $mailsubject, $mailbody, $mailtype);
          }
          return false;
    }
}
