<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: user.class.php 2010-08-24 10:42 $
*/
if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
header("Content-type:text/html;charset=utf-8");
class user_controller extends front_controller
{
	public function init()
	{
        parent::init();
		$needlogin=array('index','pay','chargelist','favorite','gain','address','addressmodify','recommend','password','goods','goodsmodify','complete','mobile','intro','bidlist','returnblist','charge','message','feedback','pmdelete','pmview','mobilesuccess','sms','card');
		$act=$GLOBALS['action'];
		if(in_array($act,$needlogin))
		{
			if($GLOBALS['userinfo']['uid'] == 0)
			{
				sheader(url('user','login'),1,'请先登录');
			}
		}
		
	}
	function index_action()
	{
		$returnlog=array();
		$countbuy=$GLOBALS['userinfo']['countbuy'];
		$bid_mod=$this->bidcms_model('goods_bidlog');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>10,'url'=>SITE_ROOT.'/index.php?con=user','example'=>2);
		$bidinfo=$bid_mod->where(array('uid'=>$GLOBALS['userinfo']['uid']))->get_page($showpage);
        $pageinfo = $this->bidcms_parse_page($bid_mod->where(array('uid'=>$GLOBALS['userinfo']['uid']))->get_count(),$showpage);
		foreach($bidinfo as $k=>$v)
		{
			$container=['goods_id'=>$v['gid']];
			$goods_mod=$this->bidcms_model('goods_complete');
			$goods=$goods_mod->where($container)->get_one();
			if($goods)
			{
				$goods['bidlog']=$bid_mod->where(['gid'=>$v['gid']])->get_count();
				$bidinfo[$k]['goods']=$goods;
			}
			else
			{
				$goods_mod=$this->bidcms_model('goods_base');
				$goods=$goods_mod->where($container)->get_one();
				if($goods)
				{
					$goods['bidlog']=$bid_mod->where(['gid'=>$v['gid']])->get_count();
					$goods['goods_status']='-1';
					$bidinfo[$k]['goods']=$goods;
				}
				else
				{
					continue;
				}
			}
			$returnlog_mod=$this->bidcms_model('user_returnmoney');
			$returnlog=$returnlog_mod->where(['uid'=>$GLOBALS['userinfo']['uid'],'gid'=>$v['gid']])->get_one();
			
			$bidinfo[$k]['returnlog']=$returnlog;

		}
        
		$pagetitle='我参与的竞拍－用户中心';
		include $this->bidcms_template();
	}
    function check_register_action(){
        $dotype=$this->get('dotype');
        if(!empty($this->post('val')))
        {
          switch($dotype)
          {
            case 'username':
              $id=$this->_checkuser($this->post('val'));
              if(!$id)
              {
                $error="{'datastatus':'failed','message':'haved'}";
              }
              else
              {
                $error="{'datastatus':'success','message':'ok'}";
              }
              break;
            case 'email':
              $id=$this->_checkemail($this->post('val'));
              if($id == 0)
              {
                $error="{'datastatus':'failed','message':'haved'}";
              }
              elseif($id == 2)
              {
                $error="{'datastatus':'failed','message':'error'}";
              }
              elseif($id == 1)
              {
                $error="{'datastatus':'success','message':'ok'}";
              }
              break;
            case 'mobile':
              $mobilecheck=$this->_checkmobile($this->post('val'));
              if($mobilecheck==2)
              {
                $error="{'datastatus':'failed','message':'haved'}";
              }
              elseif($mobilecheck==0)
              {
                $error="{'datastatus':'failed','message':'error'}";
              }
              else
              {
                $error="{'datastatus':'success','message':'ok'}";
              }
              break;
            case 'imgcode':
              if($GLOBALS['setting']['site_imgcode'])
              {
                if(!$this->_checkcode($this->post('val')))
                {
                  $error="{'datastatus':'failed','message':'验证码不正确'}";
                }
              }
              else
              {
                $error="{'datastatus':'success','message':''}";
              }
              break;
          }
          exit($error);
        }
    }
	function register_action()
	{
        global $cookie;
		if($GLOBALS['userinfo']['uid']>0)
		{
            $this->bidcms_success('注册成功','',url('user','index'));
		}
        $register_limit = isset($GLOBALS['setting']['register_limit'])?$GLOBALS['setting']['register_limit']:0;
		//限制ip注册次数
		if($register_limit > 0)
		{
			$usercount=$this->bidcms_model('user_base')->where(['updatetime'=>['lt',(time()+86400)],'updatetime'=>['gt',(time()-86400)],'ip'=>real_ip()])->get_count('uid');
			if($usercount > $register_limit)
			{
                $this->bidcms_error('同一IP 24小时之内只允许注册'.$register_limit.'次');
			}
		}
        $cname = $cookie->get('cname');
		if($this->bidcms_submit_check($cname))
		{
			if($GLOBALS['setting']['site_imgcode'])
			{
				$code=$this->post('imgcode');
				if($code)
				{
					if(!$this->_checkcode($code))
					{
                        $this->bidcms_error('验证码不正确');
					}
				}
				else
				{	
					$this->bidcms_error('验证码不正确');
				}
			}
			$data['username']=$this->post('username');
			$data['email']=$this->post('useremail');
			$data['pwd']=md52($this->post('userpassword'));
			
			$data['hash']=substr(md52(microtime()),3,10);
			$data['mobilev']=substr(md52(microtime()),3,6);
			$data['verification']= isset($GLOBALS['setting']['site_verification'])?$GLOBALS['setting']['site_verification']:1; //是否免验证
			
			$data['usertype']='normaluser';
			$data['ip'] = real_ip();
			if($GLOBALS['session']->get('friendid'))
			{
				$data['friendid']=intval($GLOBALS['session']->get('friendid'));
			}
			$data['updatetime']=time();
			$data['mobile']=$this->post('mobile');
			$data['usercard']=intval($GLOBALS['setting']['site_givemoney']); //推广期赠送现金券
			
			$data['score']=intval($GLOBALS['setting']['site_givescore']);
			$data['lastip']=$data['ip'];
			$data['lastlogin']=$data['updatetime'];
			$data['ucid']=$uid;
			if($data['verification']==1)
			{
				$data['money']=floatval($GLOBALS['setting']['site_giverealmoney']);
			}
			if($muid=$this->bidcms_model('user_base')->insert_data($data))
			{
				$pm = $data['username'].'您好！欢迎你来到“'.$GLOBALS['setting']['site_title'].'”！通过手机注册就可以免费获得'.$GLOBALS['setting']['site_giverealmoney'].'个'.$GLOBALS['setting']['site_money_name'].'进行竞拍。还不知如何竞拍吗？赶快去“新手上路”里看看竞拍规则吧!';

				$this->bidcms_model('user_status')->insert_data(
                    array(
                        'uid'=>$muid,
                        'title'=>"刚刚加入".$GLOBALS['setting']['site_title'],
                        'username' => $data['username'],
                        'updatetime' => time()
                    )    
                );
                $this->bidcms_model('user_pm')->insert_data(
                    array(
                        'from_uid'=>$muid,
                        'from_uid'=>$touid,
                        'title'=>"刚刚加入".$GLOBALS['setting']['site_title'],
                        'username' => $data['username'],
                        'content' => $pm,
                        'updatetime' => time()
                    )    
                );
				$GLOBALS['session']->set('uid',$muid);
				if(!empty($data['money']) && $GLOBALS['setting']['site_giverealmoney']>0)
				{
                    //添加资金操作日志记录代码
                    $this->bidcms_model('user_moneylog')->insert_data(
                       array(
                        'get'=>$GLOBALS['setting']['site_giverealmoney'],
                        'put'=>0,
                        'title'=>"注册赠送".$GLOBALS['setting']['site_money_name'],
                        'uid'=>$muid,
                        'updatetime'=>time(),
                        'payment'=>'系统',
                        'money'=>$data['money'],
                        'username'=>$data['username']
                       )
                    );
                    $this->bidcms_success('注册成功','',url('user','mobilesuccess'));
				}
				else
				{
                  $this->bidcms_success('注册成功','',url('user','mobile'));
				}
			}
          $this->bidcms_error('未注册成功');
		}
		
		else
		{    
            $pagetitle='用户注册';
            $cname = 'commit'.rand(1000,9999);
            $cookie->set('cname',$cname);
            //已结束
            $goods_success = $this->bidcms_model('goods_complete')->fields(array('goods_name', 'goods_id', 'thumb', 'currentuser', 'currentuid', 'marketprice', 'nowprice', 'goods_status', 'content'))->where(array('goods_status' => array('lt', 3)))->get_page(5, 'order by lasttime desc');
            include $this->bidcms_template();
		}
	}

	function login_action()
	{
        global $cache;
		if($this->bidcms_submit_check('commit'))
		{
			    if(isset($GLOBALS['setting']['site_logincode']) && $GLOBALS['setting']['site_logincode']>0)
				{
					$code=$this->post('verify');
					if($code) {
						if(!$this->_checkcode($code)) {
							$this->bidcms_error('验证码不正确');
						}
					}
					else {	
                        $this->bidcms_error('验证码不能为空');
					}
				}
				$data['username'] = $this->post('username');
          
				$user=$this->bidcms_model('user_base')->where(['username'=>$data['username']])->whereor(['mobile'=>$data['username']])->get_one();
          
                if($user['pwd'] == md52($this->post('password')) && $user['usertype']!='adminuser')
				{
					$ldata['lastip']=real_ip();
					$ldata['lastlogin']=time();
					if($ldata['lastip']!=$user['ip'] || empty($user['address']))
					{
						$ldata['ip'] = $ldata['lastip'];
						$ldata['address'] = $ldata['lastip'];
					}
					
					$GLOBALS['session']->set(array('uid'=>$user['uid'],'username'=>$user['username']));
					
					
					$cache->delete('user'.($user['uid']%10).':'.$user['uid']);
					
					$this->bidcms_model('user_base')->where('uid',$user['uid'])->update_data($ldata);
					
                    $this->bidcms_success('登录成功');
                }
                $this->bidcms_error('登录失败');
		}
		else
		{
			$referer=isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'';
			$alipaylogin=$this->_alipaylogin();
			$pagetitle='用户登录';
			include $this->bidcms_template();
		}
	}
	
	function logout_action()
	{
        global $cache;
		$cache->delete('user'.($GLOBALS['userinfo']['uid']%10).':'.$GLOBALS['userinfo']['uid']);
		$GLOBALS['session']->destroy(array('uid'=>0));
		sheader(url('index','index'),3,'成功退出系统');
	}
	//手机验证页面
	function mobile_action()
	{
        $cache;
		if($GLOBALS['userinfo']['verification'] == 1) {
			sheader(url('user','mobilesuccess'));
		} else {
			if($this->bidcms_submit_check('commit'))
			{
				$mobilecode = $this->post('txt_checkcode');
				$data['verification']=1;
				if($GLOBALS['setting']['site_giverealmoney'] > 0)
				{
					$data['money']=$GLOBALS['setting']['site_giverealmoney']+$GLOBALS['userinfo']['money'];
				}
                if($this->check_sms($GLOBALS['userinfo']['mobile'],$mobilecode)){
                    //局部更新缓存
                    $GLOBALS['userinfo']['money'] = $data['money'];
                    $GLOBALS['userinfo']['verification'] = $data['verification'];
                    $cache->set('user'.($GLOBALS['userinfo']['uid']%10).':'.$GLOBALS['userinfo']['uid'],$GLOBALS['userinfo']);
                  
                    $this->bidcms_model('user_base')->where(['uid'=>$GLOBALS['userinfo']['uid']])->update_data($data);
                    $GLOBALS['cache']->delete('user'.($GLOBALS['userinfo']['uid']%10).':'.$GLOBALS['userinfo']['uid']);
					if($GLOBALS['setting']['site_giverealmoney']>0) {
						$pm = $GLOBALS['userinfo']['username'].'您好！您已经通过手机验证，'.$GLOBALS['setting']['site_giverealmoney'].'个免费'.$GLOBALS['setting']['site_money_name'].'已即时充入你的账户，请查收！现在就可以点击首页参加竞拍了。想增加胜出机会吗？快去帮助中心查看“竞拍秘籍”吧！ ';
                        $this->bidcms_model('user_status')->insert_data(
                            array(
                                'uid'=>$GLOBALS['userinfo']['uid'],
                                'title'=>"刚刚通过了手机验证",
                                'username' => $GLOBALS['userinfo']['username'],
                                'updatetime'=>time()
                            )    
                        );
                        $this->bidcms_model('user_pm')->insert_data(
                            array(
                                'from_uid'=>0,
                                'to_uid'=>$GLOBALS['userinfo']['uid'],
                                'title'=>"刚刚通过了手机验证",
                                'content'=>"刚刚通过了手机验证",
                                'username' => $GLOBALS['userinfo']['username'],
                               'updatetime'=>time()
                            )    
                        );
						//添加资金操作日志记录代码
                        $this->bidcms_model('user_moneylog')->insert_data(
                           array(
                            'get'=>$GLOBALS['setting']['site_giverealmoney'],
                            'put'=>0,
                            'title'=>"手机验证通过赠送".$GLOBALS['setting']['site_money_name'],
                            'uid'=>$GLOBALS['userinfo']['uid'],
                            'updatetime'=>time(),
                            'payment'=>'系统',
                            'money'=>floatval($GLOBALS['setting']['site_giverealmoney']),
                            'username'=>$GLOBALS['userinfo']['username']
                           )
                        );
					}
					sheader(url('user','mobilesuccess'),3,'验证成功');
					
				} else {
					sheader(url('user','mobile'),3,'验证失败');
				}

			}
			else
			{
                //已结束
                $pagetitle='手机验证';
				include $this->bidcms_template();
			}
		}
	}
    //发送验证码
	function sendsms_action()
	{
        if($GLOBALS['userinfo']['uid'] > 0 && !empty($GLOBALS['userinfo']['mobile'])){
          $code = rand(1000,9999);
          $content = '您好！欢迎您来到'.$GLOBALS['setting']['site_title'].'您的验证码为：'.$code;
          $res = $this->sms($GLOBALS['userinfo']['mobile'],$content,$code);
          if($res == 1){
            $this->bidcms_success(array('datastatus'=>1));
          }
        }
	}
	//验证成功
	function mobilesuccess_action()
	{
        //已结束
        $pagetitle='验证成功';
        $status = $this->bidcms_model('user_status')->where(['uid'=>['neq',$GLOBALS['userinfo']['uid']]])->wherelike(['title'=>'购买'])->get_page(16,'order by rand()');
		include $this->bidcms_template('success');
	}
	//忘记密码
	function forget_action()
	{
		if($this->bidcms_submit_check('commit'))
		{
			include(ROOT_PATH.'/models/fetion.php');
			$fetion=new fetion();
			$mobile=trim($this->post('mobile'));
			$checkcode=trim($this->post('checkcode'));
			if(!$this->_checkcode($checkcode))
			{
				sheader(url('user','forget'),7,'验证码错误');
			}
			if($mobile)
			{
				$user=$this->bidcms_model('user_base')->where('mobile',$mobile)->get_one();
				if($user['mobilecount'])
				{
					$mobilecount=explode('-',$user['mobilecount']);
					if(time()-$mobilecount[0]<86400 && $mobilecount[1]>5)
					{
						sheader(url('user','login'),7,'24小时之内验证超过限制次数');
					}
				}
				else
				{
					$mobilecount=time().'-0';
				}
				$hash=substr(md52(microtime()),rand(0,10),6);
				if($user)
				{
					$this->bidcms_model('user_base')->where('uid',$user['uid'])->update_data(array('mobilev'=>$hash,'mobilecount'=>$mobilecount[0].'-'.($mobilecount[1]+1)));
					try
					{
						$content=$user['username']."您好，".$GLOBALS['setting']['site_title']."找回密码验证为:".$hash.",验证码使用一次后失效。";
						
						$result=$fetion->send($mobile,$content);
						if($result==1)
						{
							$GLOBALS['session']->set(array('mobile'=>$mobile));
							sheader(url('user','changpassword'),7,'验证码发送成功，请注意查收');
						}
						else
						{
							sheader(url('user','forget'),3,'验证码发送失败,原因：'.$GLOBALS['sms_error'][$result]);
						}
					}
					catch(Exception $e)
					{
						sheader(url('user','forget'),3,$e);
					}
				}
				else
				{
					sheader(url('user','forget'),3,'没有此用户');
				}
			}
			else
			{
				sheader(url('user','forget'),3,'没有此用户');
			}
		}
		else
		{
			$pagetitle='忘记密码';
			include $this->bidcms_template();
		}
	}
	//找回密码
	function changpassword_action()
	{
		if($this->bidcms_submit_check('commit'))
		{
			if($GLOBALS['session']->get('mobile'))
			{
				
				$password1=$this->bidcms_request('password');
				$password2=$this->bidcms_request('password2');
				$mobilecode=$this->bidcms_request('mobilecode');
				$container=["mobile"=>$GLOBALS['session']->get('mobile'),"mobilev"=>$mobilecode];
				$user=$this->bidcms_model('user_base')->where($container)->get_one();
				if(!empty($password1) && $password1==$password2)
				{
					if($user && $this->bidcms_model('user_base')->where($container)->update_data(array('pwd'=>md52($password1),'mobilev'=>substr(md5(microtime()),rand(0,20),6))))
					{
						sheader(url('user','login'),3,'密码修改成功');
					}
					else
					{
						sheader(url('user','changpassword'),3,'密码修改失败');
					}
				}
				else
				{
					header(url('user','changpassword'),3,'两次密码不一致');
				}
			}
			else
			{
				sheader(url('user','forget'),3,'验证信息为空');
			}
		}
		else
		{
			$pagetitle='忘记密码';
			include $this->bidcms_template();
		}
	}
	//更改资料
	function intro_action()
	{
        global $cache;
		$container=['uid'=>$GLOBALS['userinfo']['uid']];
        
		if($this->bidcms_submit_check('commit'))
		{
			$data['identity']=$this->post('txt_idcard');
			$data['realname']=$this->post('txt_realname');
			$data['sex']=intval($this->post('rdo_sex'));
			$data['province']=$this->post('sel_province');
			$data['city']=$this->post('sel_city');
			$data['area']=$this->post('sel_area');
			$data['cusaddress']=$this->post('txt_address');
			$data['telphone']=$this->post('txt_tel');
			
			if($this->bidcms_model('user_base')->where($container)->update_data($data))
			{
				$cache->delete('user'.($GLOBALS['userinfo']['uid']/10).':'.$GLOBALS['userinfo']['uid']);
				$this->bidcms_model('user_status')->insert_data(
                    array(
                        'uid'=>$GLOBALS['userinfo']['uid'],
                        'title'=>"刚刚更改了资料",
                        'username' => $GLOBALS['session']->get('username')
                    )    
                );
				sheader(url('user','intro'),3,'资料修改成功');
			}
			else
			{
				sheader(url('user','intro'),3,'修改失败');
			}
		}
		else
		{
			$pagetitle='个人资料更新';
			$userintro=$this->bidcms_model('user_base')->where($container)->get_one();
            
			include $this->bidcms_template();
		}
	}
    function avatar_action(){
        if($GLOBALS['userinfo']['uid']>0){
          $uid = $GLOBALS['userinfo']['uid'];
          include_once ROOT_PATH.'/inc/classes/upload.class.php';
          $up = new upload('file-0');
          $up->updir = ROOT_PATH.'data/upload/avatar/'.($uid%10);
          if($up->checkStatus() && $up->checkType() && $up->checkSize()){
              $file=$up->execute($uid);
              $this->bidcms_success(array('name'=>str_replace(ROOT_PATH,'',$file)),'上传成功');
          }
        }
    }
    
	//更改密码 
	function password_action()
	{
		if($this->bidcms_submit_check('commit'))
		{
			$newpwd=$this->post('newPwd');
			$newpwd2=$this->post('renewPwd');
			
			
			if($newpwd!=''&& $newpwd==$newpwd2)
			{
				$oldpassword=$this->post('oldPwd');
				$container=['uid'=>$GLOBALS['userinfo']['uid'],'pwd'=>md52($oldpassword)];
				$user=$this->bidcms_model('user_base')->where($container)->get_one();
				if($user)
				{
					$data['pwd']=md52($newpwd);
				}
				else
				{
					sheader(url('user','password'),3,'密码输入错误，你无权修改此密码');
				}
			}
			if($data && $this->bidcms_model('user_base')->where('uid',$GLOBALS['userinfo']['uid'])->update_data($data))
			{
				uc_user_edit($user['username'], $oldpassword, $newpwd, $user['email']);
				sheader(url('user','password'),3,'密码修改成功');
			}
			else
			{
				sheader(url('user','password'),3,'密码修改成功');
			}
		}
		else
		{
			$pagetitle='更改资料';
			include $this->bidcms_template();
		}
	}
	//验证代金券
	function card_action()
	{
		if($this->bidcms_submit_check('commit'))
		{
			$cardnumber=$this->bidcms_request('cardnumber');
			$cardcode=$this->bidcms_request('cardcode');
			$container=['cardnumber'=>$cardnumber,'cardcode'=>$cardcode,'cardstatus'=>0];
			
			$card_mod=$this->bidcms_model('user_card');
			$cardinfo=$card_mod->where($container)->get_one();
			
			if($cardinfo)
			{
				if($cardinfo['cardvalue']>0)
				{
					$this->bidcms_model('user_base')->where('uid',$GLOBALS['userinfo']['uid'])->update_add_data('money',floatval(abs($cardinfo['cardvalue'])));
                    $this->bidcms_model('user_card')->where($container)->update_data(['cardstatus'=>1,'username'=>$GLOBALS['userinfo']['username'],'uid'=>$GLOBALS['userinfo']['uid']]);
					//添加资金操作日志记录代码
                    $this->bidcms_model('user_moneylog')->insert_data(
                       array(
                        'get'=>$cardinfo['cardvalue'],
                        'put'=>0,
                        'title'=>"代金券验证通过赠送".$cardinfo['cardvalue'],
                        'uid'=>$GLOBALS['userinfo']['uid'],
                        'updatetime'=>time(),
                        'payment'=>'系统',
                        'money'=>floatval($GLOBALS['userinfo']['money']+$cardinfo['cardvalue']),
                        'username'=>$GLOBALS['userinfo']['username']
                       )
                    );
					sheader(url('user','chargelist'),5,'此代金券面值为'.$cardinfo['cardvalue'].',已充入您的帐户中');
				}
				else
				{
					sheader(url('user','chargelist'),5,'此代金券面值为0');
				}
			}
			else
			{
				sheader(url('user','card'),2,'代金券不存在，请重新输入');
			}
		}
		else
		{
			$pagetitle='代金券验证';
			include $this->bidcms_template();
		}
	}
	//充值
	function charge_action()
	{
		
		$charge_mod=$this->bidcms_model('user_charge');
		$pagetitle='充值中心';
		$charge_list=$charge_mod->get_page();
		include $this->bidcms_template();
	}
	//线下付款
	function pay2_action()
	{
		if($this->bidcms_submit_check('commit'))
		{
			$paylog_mod=$this->bidcms_model('user_paylog');
			$data['payment']=intval($this->post('payment'));
			$data['payuser']=$this->post('payuser');
			$data['paytime']=$this->post('paytime');
			$data['paymoney']=$this->post('paymoney');
			$data['paycard']=$this->post('paycard');
			$data['paytype']=$this->post('paytype');
			$data['tel']=$this->post('tel');
			$data['email']=$this->post('email');
			$data['content']=$this->post('content');
			if($paylog_mod->insert_data($data))
			{
				sheader(url('user','pay2'),2,'提交成功,我们会尽快给您答复');
			}
			else
			{
				sheader(url('user','pay2'),2,'提交失败');
			}
		}
		else
		{
			include $this->bidcms_template('payment');
		}
	}
	//充值付款
	function pay_action()
	{
		$moneyid=intval($this->bidcms_request('moneyid'));
		if($moneyid<=0)
		{
			sheader(url('user','charge'),3,'参数为空');
		}
		$charge_mod=$this->bidcms_model('user_charge');
		$charge=$charge_mod->where('id',$moneyid)->get_one();
		if($charge)
		{
			$cuserinfo=$this->bidcms_model('user_base')->where('uid',intval($GLOBALS['userinfo']['uid']))->get_one();
			$payment=!empty($this->bidcms_request('gateway'))?$this->bidcms_request('gateway'):'alipay';
			$orderinfo['service']='create_partner_trade_by_buyer';
			$orderinfo['subject']='充值'.$charge['title'].'('.$cuserinfo['username'].')';
			$orderinfo['body']=$orderinfo['subject'];
			$orderinfo['price']=$charge['nowprice']; //测试使用0.01,正确为$charge['nowprice']
			$orderinfo['order_no']=$cuserinfo['hash'].'_'.$cuserinfo['uid'].'_'.$charge['id'].'_'.substr(md5(microtime()),rand(0,10),6);
			$orderinfo['logistics_fee']		= "0.00";				//物流费用，即运费。
			$orderinfo['logistics_type']		= "EXPRESS";			//物流类型，三个值可选：EXPRESS（快递）、POST（平邮）、EMS（EMS）
			$orderinfo['logistics_payment']	= "SELLER_PAY";			//物流支付方式，两个值可选：SELLER_PAY（卖家承担运费）、BUYER_PAY（买家承担运费）
			$orderinfo['return_url']=SITE_ROOT.'/models/alipay/return.php';
			$orderinfo['notify_url']=SITE_ROOT.'/models/alipay/notify.php';
			$orderinfo['show_url']=url('user','charge');
			switch($payment)
			{
				case 'alipay': //支付宝
				
					include ROOT_PATH.'/tools/alipay.php';
					$pay_mod=new alipay();
					$pay_mod->init($orderinfo,$cuserinfo);
					$payurl=$pay_mod->create_url();
					sheader($payurl,2,'正离开'.$GLOBALS['setting']['site_title'].'前往支付宝网关进行支付');
					
				break;
				case 'bank': //银行转帐
					echo $GLOBALS['setting']['bank_bankinfo'];

				break;
			}
		}
		else
		{
			sheader(url('user','charge'),3,'请选择充值类型');
		}
	}
	//会员自主发布商品
	function goods_action()
	{
		//判断用户权限
		if($GLOBALS['userinfo']['usertype'] != 'selluser')
		{
			sheader(url('user','index'),1,'对不起，您的权限不足');
		}
		$goods_mod=$this->bidcms_model('goods_base');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>SITE_ROOT.'/index.php?con=user&act=goods','example'=>2);
		
		
		$goodslist=$goods_mod->where(array('hostuid'=>$GLOBALS['userinfo']['uid']))->get_page($showpage,'order by goods_id desc');
		if($goodslist)
		{
			$bidlog_mod=$this->bidcms_model('goods_bidlog');
			foreach($goodslist as $k=>$v)
			{
				$goodslist[$k]['moneycount']=$bidlog_mod->where(['gid'=>$v['goods_id']])->get_sum('money');
			}
		}
        $count = $goods_mod->where('hostuid',$GLOBALS['userinfo']['uid'])->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		$pagetitle='会员自主发布商品';
		include $this->bidcms_template();
	}
	//会员完成商品
	function complete_action()
	{
		//判断用户权限
		if($GLOBALS['userinfo']['usertype']!='selluser')
		{
			sheader(url('user','index'),1,'对不起，您的权限不足');
		}
		$showpage=array('isshow'=>1,'current_page'=>$this->get('page',0),'page_size'=>20,'url'=>SITE_ROOT.'/index.php?con=user&act=complete','example'=>2);
		$goods_mod=$this->bidcms_model('goods_complete');
		$goodslist=$goods_mod->where('hostuid',$GLOBALS['userinfo']['uid'])->get_page($showpage);
		if($goodslist)
		{
			$bidlog_mod=$this->bidcms_model('goods_bidlog');
			foreach($goodslist as $k=>$v)
			{
				$goodslist[$k]['moneycount']=$bidlog_mod->where(['gid'=>$v['goods_id']])->get_sum('money');
			}
		}
        $pagetitle = '竞拍完成';
        $count = $this->bidcms_model('goods_complete')->where('hostuid',$GLOBALS['userinfo']['uid'])->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template();
	}
	//会员自主发布商品修改
	function goodsmodify_action()
	{
		$updateid=$this->get('updateid',0);
		$goods_mod=$this->bidcms_model('goods_base');
		//判断用户权限
		if($GLOBALS['userinfo']['usertype']!='selluser')
		{
			sheader(url('user','index'),1,'对不起，您的权限不足');
		}
		//判断此商品是否有权限编辑
		if($this->bidcms_submit_check('commit'))
		{
			$data['goods_name']=$this->post('goods_name');
			$data['lasttime']=strtotime($this->post('lasttime'));
			$data['starttime']=strtotime($this->post('starttime'));
			$data['shutuid']='';
			
			$data['cateid']=intval($this->post('cateid'));
			$data['marketprice']=floatval($this->post('marketprice'));
			$data['nowprice']=floatval($this->post('nowprice'));
			$data['diffmoney']=floatval($this->post('diffmoney'));
			$data['diffprice']=floatval($this->post('diffprice'));
			$data['diffscore']=intval($this->post('diffscore'));
			
			$data['limitnumber']='';
			$data['islimit']=0;
			$data['content']=$this->post('content');

			$data['ispassed']=intval($GLOBALS['setting']['score_passed'])>0 && $guserinfo['score']>=intval($GLOBALS['setting']['score_passed']);
			if(!empty($this->post('details')))
			{
				$data['details']= htmlspecialchars($this->post('details'));
			}
			$data['updatetime']=time();
			$thumb=array();
			
			if($_FILES['local_thumb']['name'])
			{
				
				$oldthumb=array();
				if($updateid)
				{
					$oldthumb=$goods_mod->fields(array('thumb'))->where('goods_id',$updateid)->get_one();
					$oldthumb=explode(',',$oldthumb['thumb']);
					foreach($oldthumb as $k=>$v)
					{
						if(!is_file($v))
						{
							unset($oldthumb[$k]);
						}
					}
					
					if(count($oldthumb)<4)
					{
						$uploaddir='data/upload/'.date('Y').'/'.date('m');
						$thumb=$this->attach('local_thumb',$uploaddir);
					}
					else
					{
						for($j=4;$j<count($oldthumb);$j++)
						{
							if(isset($oldthumb[$j]) && is_file($oldthumb[$j]))
							{
								unlink($oldthumb[$j]);
							}
						}	
					}
				}
				
				$thumb=array_merge($thumb,$oldthumb);
				$data['thumb']=implode(',',$thumb);
			}
			
			if($updateid)
			{
				if($goods_mod->where(['goods_id'=>$updateid,'hostuid'=>$GLOBALS['userinfo']['uid']])->update_data($data))
				{
					$cachedata['goods_name']=$data['goods_name'];
					$cachedata['nowprice']=$data['nowprice'];
					$cachedata['lasttime']=$data['lasttime'];
					$cachedata['currentuser']=$data['currentuser'];
					$cachedata['currentuid']=$data['currentuid'];
					$cachedata['difftime']=$data['difftime'];
					$cachedata['addtime']=$data['addtime'];
					$cachedata['autobuy_time']=$data['autobuy_time'];
					$cachedata['auto_times']=$data['auto_times'];
					$cachedata['autobuy_money']=$data['autobuy_money'];
					$cachedata['diffmoney']=$data['diffmoney'];
					$cachedata['diffprice']=$data['diffprice'];
					$cachedata['diffscore']=$data['diffscore'];
					$goodscache=$this->bidcms_model('goods_cache');
					$goodsc_data=$goodscache->where('goods_id',$updateid)->get_one();
					
					if($goodsc_data)
					{
						$goodscache->where(['goods_id'=>$updateid])->update_data($cachedata);
					}
					else
					{
						$cachedata['goods_id']=$updateid;
						$goodscache->insert_data($cachedata);
					}
					sheader(url('user','goods'),3,'修改成功');
				}
				else
				{
					sheader(url('user','goods'),3,'数据未发生变化，修改失败');
				}
			}
			else
			{
				$data['hostuser']=$GLOBALS['userinfo']['username'];
				$data['hostuid']=$GLOBALS['userinfo']['uid'];
				$insertid=$goods_mod->insert_data($data);
				if($insertid)
				{
					$cachedata['goods_name']=$data['goods_name'];
					$cachedata['nowprice']=$data['nowprice'];
					$cachedata['lasttime']=$data['lasttime'];
					$cachedata['currentuser']=$data['currentuser'];
					$cachedata['currentuid']=$data['currentuid'];
					$cachedata['difftime']=$data['difftime'];
					$cachedata['addtime']=$data['addtime'];
					$cachedata['autobuy_time']=$data['autobuy_time'];
					$cachedata['auto_times']=$data['auto_times'];
					$cachedata['autobuy_money']=$data['autobuy_money'];
					$cachedata['diffmoney']=$data['diffmoney'];
					$cachedata['diffprice']=$data['diffprice'];
					$cachedata['diffscore']=$data['diffscore'];
					$goodscache=$this->bidcms_model('goods_cache');
                    $goodscache->where(['goods_id'=>$insertid])->save_data($cachedata,['goods_id'=>$insertid]);
					
					sheader(url('user','goods'),3,'添加成功');
				}
				else
				{
					sheader(url('user','goods'),3,'添加失败');
				}
			}
		}
		else
		{
            $pagetitle = '发布商品';
			if($updateid)
			{
				$container=['goods_id='=>$updateid,'hostuid'=>$GLOBALS['userinfo']['uid']];
				
				$goods=$goods_mod->where($container)->get_one();
				if($goods)
				{
					include $this->bidcms_template('goodsmodify');
				}
				else
				{
					sheader(url('user','goods'),1,'地址修改成功');
				}
			}
			else
			{
                $goods = $goods_mod->fields;
				include $this->bidcms_template();
			}
		}
		
	}
	//会员商品竞价情况
	function buylog_action()
	{
		$gid=intval($this->bidcms_request('gid'));
		$goods_mod=$this->bidcms_model('goods_base');
		
		if($gid)
		{
			
			$goodsinfo=$goods_mod->fields(array('goods_name','currentuser','goods_id','ispassed','lasttime'))->where('goods_id',$gid)->get_one();
			if($goodsinfo && !$goodsinfo['ispassed'])
			{
				sheader(SITE_ROOT.'/index.php',3,'商品未审核');
			}
			if(!$goodsinfo)
			{
				$complete_mod=$this->bidcms_model('goods_complete');
				$complete=1;
				$goodsinfo=$complete_mod->fields(array('goods_name','currentuser','goods_id','lasttime'))->where('goods_id',$gid)->get_one();
			}
			if($goodsinfo)
			{
				$bidlog_mod=$this->bidcms_model('goods_bidlog');
				$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>SITE_ROOT.'/index.php?con=user&act=buylog&gid'.$gid);
				$buylog_list=$bidlog_mod->where(array('gid'=>$gid))->get_page($showpage);
                $pagetitle=$goodsinfo['goods_name'].'[ 第'.$goodsinfo['goods_id'].'期 ]竞拍详细日志';
				include $this->bidcms_template();
			}
			else
			{
				sheader(SITE_ROOT.'/index.php',3,'商品不存在');
			}
			
		}
		else
		{
			sheader(SITE_ROOT.'/index.php',3,'商品不存在');
		}
				
	}
	//资金变动记录
	function chargelist_action()
	{
		$moneylog_mod=$this->bidcms_model('user_moneylog');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>SITE_ROOT.'/index.php?con=user&act=chargelist','example'=>2);
		$moneylog=$moneylog_mod->where(array('uid'=>$GLOBALS['userinfo']['uid']))->get_page($showpage,'order by updatetime desc');
		$pagetitle='资金变动记录';
        $count = $moneylog_mod->where(array('uid'=>$GLOBALS['userinfo']['uid']))->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template();
	}
	//竞拍记录
	function bidlist_action()
	{
		
		$bid_mod=$this->bidcms_model('goods_bidlog');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>SITE_ROOT.'/index.php?con=user&act=bidlist','example'=>2);
		$bidlog=$bid_mod->where(array('uid'=>$GLOBALS['userinfo']['uid']))->get_page($showpage,'order by updatetime desc');
		$pagetitle='竞拍记录';
        $count = count($bidlog)<$showpage['page_size']?0:$bid_mod->where(array('uid'=>$GLOBALS['userinfo']['uid']))->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template();
	}
	//返利记录
	function returnblist_action()
	{
		
		$return_mod=$this->bidcms_model('user_returnmoney');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>SITE_ROOT.'/index.php?con=user&act=returnblist','example'=>2);
		$returnlog=$return_mod->where(array('uid'=>$GLOBALS['userinfo']['uid']))->get_page($showpage,'order by updatetime desc');
		$pagetitle='竞拍返利记录';
        $count = count($returnlog)<$showpage['page_size']?0:$return_mod->where(array('uid'=>$GLOBALS['userinfo']['uid']))->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('returnblist');
	}
	//我的成果
	function gain_action()
	{
		$countbuy=$GLOBALS['userinfo']['countbuy'];
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>10,'url'=>SITE_ROOT.'/index.php?con=user&act=gain','example'=>2);
		$goods_mod=$this->bidcms_model('goods_complete');
		$goods_list=$goods_mod->where(array('currentuid'=>$GLOBALS['userinfo']['uid']))->get_page($showpage);
		$pageinfo = $this->bidcms_parse_page($goods_mod->where(array('currentuid'=>$GLOBALS['userinfo']['uid']))->get_count(),$showpage);
		
		foreach($goods_list as $k=>$v)
		{
			$buylog_mod=$this->bidcms_model('goods_bidlog');
			$buylog=$buylog_mod->where(['uid'=>$GLOBALS['userinfo']['uid'],'gid'=>$v['goods_id']])->get_one();
			$goods_list[$k]['count']=$buylog['count'];
			$goods_list[$k]['money']=$buylog['money'];

		}
		
		
		$pagetitle='我的成果－用户中心';
		include $this->bidcms_template('gain');
	}
	//邀请
	function recommend_action()
	{
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>SITE_ROOT.'/index.php?con=user&act=recommend','example'=>2);
		$userlist=$this->bidcms_model('user_base')->fields(array('username','updatetime'))->where(array('friendid'=>$GLOBALS['userinfo']['uid']))->get_page($showpage);
		$pagetitle='我的邀请';
        $count = $this->bidcms_model('user_base')->where(array('friendid'=>$GLOBALS['userinfo']['uid']))->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('recommend');
	}
	//站内信
	function message_action()
	{
        $showpage = array('current_page'=>$this->get('page',0),'page_size'=>20,'url'=>url('user','message'));
		$pmlist = $this->bidcms_model('user_pm')->where('to_uid',$GLOBALS['userinfo']['uid'])->get_page($showpage);
		$pagetitle='站内信';
        $count = $this->bidcms_model('user_pm')->where('to_uid',$GLOBALS['userinfo']['uid'])->get_count('id');
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('message');
	}
	//查看站内信
	function pmview_action()
	{
		$pmid=intval($this->get('id'));
		if($pmid)
		{
            $pm = $this->bidcms_model('user_pm')->where('id',$pmid)->get_one();
			if($pm['to_uid'] == $GLOBALS['userinfo']['uid']){
              $pagetitle='站内信';
              include $this->bidcms_template('pmview');
            }
		}
		else
		{
			sheader(url('user','message'),1,'参数为空');
		}
	}
	//删除站内信
	function pmdelete_action()
	{
		$pmid=$this->bidcms_request('id');
		if($pmid)
		{
			$pm=uc_pm_delete($GLOBALS['userinfo']['ucid'],'inbox',explode(',',$pmid));
			if($pm)
			{
			sheader(url('user','message'),1,'删除成功');
			}
			else
			{
				sheader(url('user','message'),1,'删除失败');
			}
		}
		else
		{
			sheader(url('user','message'),1,'参数为空');
		}
	}
	//我的意见
	function feedback_action()
	{
		$container=array('uid'=>$GLOBALS['userinfo']['uid']);
		$feed_mod=$this->bidcms_model('user_guestbook');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>SITE_ROOT.'/index.php?con=user&act=feedback','example'=>2);
		$feedback=$feed_mod->where($container)->get_page($showpage);
		$pagetitle='我的意见';
        $count = $feed_mod->where($container)->get_count('id');
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('feedback');
	}
	//短信订阅
	function sms_action()
	{
        $container=array('uid'=>$GLOBALS['userinfo']['uid']);
		$sms_mod=$this->bidcms_model('user_smslist');
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>SITE_ROOT.'/index.php?con=user&act=sms','example'=>2);
		$smslist=$sms_mod->where($container)->get_page($showpage);
		$pagetitle='我的短信订阅';
        $count = $sms_mod->where($container)->get_count('id');
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('smslist');
	}
	//收货地址
	function address_action()
	{
        $container=array('uid'=>$GLOBALS['userinfo']['uid']);
		$address_mod=$this->bidcms_model('user_address');
		$address=$address_mod->where($container)->get_page();
		$pagetitle='我的收货地址';
		include $this->bidcms_template('address');
	}
	//修改收货地址
	function addressmodify_action()
	{
		$address_mod=$this->bidcms_model('user_address');
		
		if($this->bidcms_submit_check('commit'))
		{   
            $updateid=intval($this->post('updateid'));
			$data['username']=$this->post('username');
			$data['telphone']=$this->post('telphone');
			$data['address']=$this->post('address');
			$referer=$this->post('to',url('user','address'));
			if($updateid) {
				if($address_mod->where(['id'=>$updateid])->update_data($data)) {
					sheader($referer,1,'地址修改成功');
				} else {
					sheader(url('user','address'),1,'未修改地址或修改失败');
				}
			} else {
				$data['uid']=$GLOBALS['userinfo']['uid'];
				if($address_mod->insert_data($data)) {
					sheader($referer,1,'地址添加成功');
				} else {
					sheader(url('user','address'),1,'地址添加失败');
				}
			}
		} else {
            $address = $address_mod->fields;
            $updateid=intval($this->get('updateid'));
			if($updateid) {
				$address=$address_mod->where('id',$updateid)->get_one();
			}
			$referer=isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'';
			$pagetitle='我的收货地址';
			include $this->bidcms_template('addressmodify');
		}
	}
	
	//函数部分
	function _checkemail($email)
	{
		$pattern="/[a-zA-z0-9]+@[a-zA-z0-9]+\.[a-z]{2,4}/i";
		$data=$this->bidcms_model('user_base')->where('email',$email)->get_one();
		if($data)
		{
			return 0;
		}
		elseif(!preg_match($pattern,$email))
		{
			return 2;
		}
		else
		{
			return 1;
		}
	}
	function _checkmobile($mobile)
	{
		$pattern="/^1[0-9]{10}$/";
		if(preg_match($pattern,$mobile))
		{
			$data=$this->bidcms_model('user_base')->where('mobile',$mobile)->get_one();
			if($data)
			{
				return 2;
			}
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function _checkuser($username)
	{
		$data=$this->bidcms_model('user_base')->where('username',$username)->get_one();
		if($data)
		{
			return false;
		}
		return true;
	}
	//支付宝登录
	function _alipaylogin($email='')
	{
		include ROOT_PATH.'/tools/alipay.php';
		$pay_mod=new alipay();
		$param=array(
        "service"			=> "user_authentication",	//接口名称，不需要修改
	    //获取配置文件(alipay_config.php)中的值
        "return_url"		=> SITE_ROOT.'/tools/alipay/returnlogin.php');
		$pay_mod->init($param);
		return $pay_mod->create_url();
	}
}