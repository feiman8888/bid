<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: comment.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
class comment_controller extends front_controller
{
	function comment_controller()
	{
		$this->group=$this->bidcms_model('groups_base');
		$this->comment_group=$this->bidcms_model('comment_group');
		$this->comment_site=$this->bidcms_model('comment_site');
		$this->tongji=$this->bidcms_model('goods_tongji');
	}

	//添加、修改评论
	function commentmodify_action()
	{
		if(submitcheck('commit'))
		{
			$data['uid']=$GLOBALS['session']->get('uid');
			$data['gid']=intval($_POST['gid']);
			$updateid=intval($_POST['updateid']);
			
			$comm=$this->comment_group->where(array("uid"=>$data['uid'],"gid"=>$data['gid']))->get_one();
			if($comm && !$updateid)
			{
				echo '<SCRIPT LANGUAGE="JavaScript">
					<!--
						alert("您已对此商品评论过了");
					//-->
					</SCRIPT>';
				exit;
			}
			
			$data['content']=global_addslashes(trim($_POST['content']));
			
			
			$data['updatetime']=time();
			if($GLOBALS['setting']['commentpassed'])
			{
				$data['ispassed']=0;
			}
			else
			{
				$data['ispassed']=1;
			}
			if($updateid>0)
			{
				if($comm)
				{
					$this->comment_group->where('id',$updateid)->update_data($data);
					echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={};objdata.dotype='update';objdata.id='$updateid';objdata.content='".$data['content']."';objdata.updatetime='".date('m月d日 H:i',$data['updatetime'])."';objdata.username='".$GLOBALS['cookie']['member']['email']."';objdata.uid='".$data['uid']."';objdata.allpoint='".$GLOBALS['cookie']['member']['all_point']."';parent.parseMycommentData(objdata);</script>";
				}
				else
				{
					echo '<SCRIPT LANGUAGE="JavaScript">
					<!--
						alert("你没有此权限");
					//-->
					</SCRIPT>';
				}
			}
			else
			{
				$insertid=$this->comment_group->insert_data($data);
				if($insertid)
				{
					//评论加1
					$this->bidcms_model('comment_groups')->where('id',$data['gid'])->update_add_data('commentcount');
					if($data['ispassed'])
					{
						echo "<SCRIPT LANGUAGE='JavaScript'>var objdata={};objdata.dotype='add';objdata.id='$insertid';objdata.content='".$data['content']."';objdata.updatetime='".date('m月d日 H:i',$data['updatetime'])."';objdata.username='".$GLOBALS['cookie']['member']['email']."';objdata.uid='".$data['uid']."';objdata.allpoint='".$GLOBALS['cookie']['member']['all_point']."';parent.parseMycommentData(objdata);</SCRIPT>";
					}
					else
					{
						echo '<SCRIPT LANGUAGE="JavaScript">
						<!--
							alert("评论已发表，请等待管理员审核");
						//-->
						</SCRIPT>';
					}
				}
				else
				{
					echo '<SCRIPT LANGUAGE="JavaScript">
						<!--
							alert("评论失败，请和管理员联系");
						//-->
						</SCRIPT>';
				}
			}
		}
		
	}
	//统计
	function tongji_action()
	{
		$gid=intval($this->bidcms_request('gid'));
		
		$type=trim($this->bidcms_request('type'));
		$uid=$GLOBALS['session']->get('uid');
		if($uid)
		{
			if($gid && in_array($type,array('isup','isdown','isbuy','isfav')))
			{
				$ginfo=$this->group->fields(array('id','siteid','subject'))->where('id',$gid)->get_one();
				$tinfo=$this->tongji->where('gid',$gid)->get_one();
				$favlist=$this->bidcms_model('user_favlist');
				$ddata['uid']=$uid;
				$ddata['gid']=$gid;
				$ddata['gname']=$ginfo['subject'];
				$ddata['dotype']=$type;
				if($favlist->where(array('gid'=>$gid,'dotype'=>$type,'uid'=>$uid))->get_one())
				{
					exit('您已经'.($type=='isup'?'支持':'收藏').'过了');
					
				}
				else
				{
					$favlist->insert_data($ddata);
					//收藏加1
                    $this->bidcms_model('comment_groups')->where('id',$gid)->update_add_data('favcount');
				}
				if($tinfo)
				{
					echo $this->bidcms_model('comment_tongji')->where('gid',$gid)->update_add_data($type);
				}
				else
				{
					$data[$type]=1;
					$data['gid']=$gid;
					$data['sid']=$ginfo['siteid'];
					echo $this->tongji->insert_data($data);
				}
			}
		}
		else
		{
			echo '请先登录';
		}
	}
	//取出指定网站历史团购
	function _grouphistory($siteid)
	{

	}

	//取出类似团购
	function _grouplike($group)
	{
	}

	
}