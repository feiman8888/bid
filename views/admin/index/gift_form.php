<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>商品管理</title>
</head>
<body>


<div id="man_zone">
   <form enctype="multipart/form-data" action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=giftmodify" method="post">
<INPUT TYPE="hidden" NAME="updateid" value="<?php echo $gift['id'];?>"><INPUT TYPE="hidden" NAME="commit" value="1">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">商品名称:</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt"  NAME="subject" value="<?php echo $gift['subject'];?>" size="50" ></td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">所需积分:</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt"  NAME="needcount"  value="<?php echo $gift['needcount'];?>"></td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">所属分类:</span></td>
      <td width="82%">
	  <SELECT NAME="cateid">
		<?php if($gift['cateid']){?><option value="<?php echo $gift['cateid'];?>"><?php echo $GLOBALS['cate'][$gift['cateid']]['catename'];?></option><?php }?>
		<?php foreach($GLOBALS['cate'] as $k=>$v){?>
		<option value="<?php echo $k;?>"><?php echo $v['catename'];?></option>
		<?php }?>
	  </SELECT>
	  </td>
    </tr>
    <tr>
      <td class="left_title_2">开始日期:</td>
      <td> <INPUT TYPE="text" class="normal_txt"  NAME="starttime" id="starttime" value="<?php echo date('Y-m-d H:i:s',$gift['starttime']?$gift['starttime']:time());?>"><INPUT TYPE="button" VALUE="选择" ONCLICK="HS_setDate(document.getElementById('starttime'))" class="normal_button"></td>
    </tr>
    <tr>
      <td class="left_title_2">结束日期:</td>
      <td> <INPUT TYPE="text" class="normal_txt"  NAME="lasttime" id="lasttime" value="<?php echo date('Y-m-d H:i:s',$gift['lasttime']?$gift['lasttime']:time()+86400);?>"><INPUT TYPE="button" VALUE="选择" ONCLICK="HS_setDate(document.getElementById('lasttime'))" class="normal_button"></td>
    </tr>
	<tr>
      <td class="left_title_1">参考价：</td>
      <td>￥<INPUT TYPE="text" class="normal_txt"  NAME="marketprice"  value="<?php echo $gift['marketprice'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">库存：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="giftcount"  value="<?php echo $gift['giftcount'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1" valign="top">缩略图：<INPUT TYPE="button" VALUE="+" class="normal_button" ONCLICK="addmthumb();"></td>
      <td>
	  <div id="thumbarea">
	  <div><INPUT TYPE="file" NAME="local_thumb[]" class="normal_button"></div>
	  </div>
	   <?php 
	   if($gift['thumb']){
	   $thumb=explode(',',$gift['thumb']);
	   foreach($thumb as $k=>$v){
	   ?>
	   <div style="float:left;width:100px;height:140px;overflow:hidden;text-align:center;" id="thumb<?php echo $k;?>"><img onclick="window.open(this.src,'','');" src="<?php echo $v;?>" height='100px'/><br/><INPUT TYPE="button" VALUE="删除" ONCLICK="deletethumb('<?php echo $v;?>','thumb<?php echo $k;?>');"></div>
	   <?php }}?>
	  </td>
    </tr>
	<tr>
	  <td class="left_title_2">供应商:</td>
	  <td><INPUT TYPE="text" NAME="company" class="normal_txt" value="<?php echo $gift['company'];?>"></td>
	</tr>
	<tr>
	  <td class="left_title_2">供应商信息:</td>
	  <td><textarea name="companyinfo"  cols="40" rows="8"><?php echo $gift['companyinfo'];?></textarea></td>
	</tr>
	<tr>
	  <td class="left_title_2">简介:</td>
	  <td><textarea name="notice"  cols="40" rows="8"><?php echo $gift['notice'];?></textarea></td>
	</tr>
	<tr>
	  <td class="left_title_2">详情:</td>
	  <td>
		<?php echo $this->edit($gift['details'],'details','details');?>
	  </td>
	</tr>
	
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
