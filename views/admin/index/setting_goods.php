<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<title>团购管理</title>
</head>
<body>

<div id="man_zone">
   <form enctype="multipart/form-data" action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=settingdata" method="post">
	<INPUT TYPE="hidden" NAME="commit" value="1">
	<INPUT TYPE="hidden" NAME="dotype" value="goods">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
	<tr>
      <td class="left_title_1">金币名称：</td>
      <td><INPUT TYPE="text" class="normal_txt" name="site_money_name" VALUE="<?php echo $GLOBALS['setting']['site_money_name'];?>" >
	 </td>
    </tr>
	<tr>
      <td class="left_title_1">每次扣除：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_diffmoney"  value="<?php echo $GLOBALS['setting']['site_diffmoney'];?>">(用户每点击一次扣除的金币数)</td>
    </tr>
	<tr>
      <td class="left_title_1">首次自动出价扣除：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_autobuymoney"  value="<?php echo isset( $GLOBALS['setting']['site_autobuymoney'])?$GLOBALS['setting']['site_autobuymoney']:1;?>">(本期自动出价扣除的金币数)</td>
    </tr>
	<tr>
      <td class="left_title_1">聊天发言扣除：</td>
      <td> 充过值的会员:<INPUT TYPE="text" class="normal_txt" NAME="site_chatmoney"  value="<?php echo isset($GLOBALS['setting']['site_chatmoney'])?$GLOBALS['setting']['site_chatmoney']:0;?>"> 未充过值的会员:<INPUT TYPE="text" class="normal_txt" NAME="site_chatmoney2"  value="<?php echo isset($GLOBALS['setting']['site_chatmoney2'])?$GLOBALS['setting']['site_chatmoney2']:1;?>">(聊天发言扣除的金币数)</td>
    </tr>
	<tr>
      <td class="left_title_1">商品价格升降幅度：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_diffprice"  value="<?php echo $GLOBALS['setting']['site_diffprice'];?>">(每次点击商品价格变动幅度，正数为上升，负数为下降)</td>
    </tr>
	<tr>
      <td class="left_title_1">出价奖励积分：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_diffscore"  value="<?php echo $GLOBALS['setting']['site_diffscore'];?>">(每次点击奖励积分)</td>
    </tr>
	<tr>
      <td class="left_title_1">邀请返利：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_recommendmoney"  value="<?php echo $GLOBALS['setting']['site_recommendmoney'];?>">%(好友首次充值，都可获得的提成,百分比)</td>
    </tr>
	<tr>
      <td class="left_title_1">上传头像奖励：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="avatar_give"  value="<?php echo isset($GLOBALS['setting']['avatar_give'])?$GLOBALS['setting']['avatar_give']:0;?>">0为不奖励，大于0为奖励相应金币</td>
    </tr>
	<tr>
      <td class="left_title_1">积分购买比例：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_scoremoney"  value="<?php echo isset($GLOBALS['setting']['site_scoremoney'])?$GLOBALS['setting']['site_scoremoney']:1;?>">(在用户使用积分兑换时，充值时1元钱抵多少积分，默认为1)</td>
    </tr>
	<tr>
      <td class="left_title_1">保价购买时金币抵现：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_buymoney"  value="<?php echo isset($GLOBALS['setting']['site_buymoney'])?$GLOBALS['setting']['site_buymoney']:100;?>">(在用户使用保价购买时多少金币抵1元钱,默认为100)</td>
    </tr>
	<tr>
      <td class="left_title_1">注册赠送金币：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_giverealmoney"  value="<?php echo $GLOBALS['setting']['site_giverealmoney'];?>">(如果免手机验证，注册时即送，如果为手机验证，则手机验证通过时赠送)</td>
    </tr>
	<tr>
      <td class="left_title_1">注册赠送现金券：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_givemoney"  value="<?php echo $GLOBALS['setting']['site_givemoney'];?>">(注册赠送现金券)</td>
    </tr>

	<tr>
      <td class="left_title_1">注册赠送积分：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_givescore"  value="<?php echo $GLOBALS['setting']['site_givescore'];?>">(注册赠送积分)</td>
    </tr>
	<tr>
      <td class="left_title_1">注册显示验证码：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_imgcode"  value="<?php echo $GLOBALS['setting']['site_imgcode'];?>">(1为显示，0为不显示)</td>
    </tr>
	<tr>
      <td class="left_title_1">24小时之内同一IP允许注册次数：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="register_limit"  value="<?php echo isset($GLOBALS['setting']['register_limit'])?$GLOBALS['setting']['register_limit']:0;?>">(0为不限制)</td>
    </tr>
	<tr>
      <td class="left_title_1">登录显示验证码：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="site_logincode"  value="<?php echo $GLOBALS['setting']['site_logincode'];?>">(1为显示，0为不显示)</td>
    </tr>
	<tr>
      <td class="left_title_1">晒单评论是否审核：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="comment_passed"  value="<?php echo $GLOBALS['setting']['comment_passed'];?>">(1为需要审核，0为不需要审核)</td>
    </tr>
	<tr>
      <td class="left_title_1">留言是否审核：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="guestbook_passed"  value="<?php echo $GLOBALS['setting']['guestbook_passed'];?>">(1为需要审核，0为不需要审核)</td>
    </tr>
	
	<tr>
      <td class="left_title_1">聊天是否审核：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="chat_needpassed"  value="<?php echo $GLOBALS['setting']['chat_needpassed'];?>">(1为需要审核，0为不需要审核)</td>
    </tr>
	<tr>
      <td class="left_title_1">开启自动竞拍：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="auto_buy" value="<?php echo $GLOBALS['setting']['auto_buy'];?>">(1为开启，0为关闭)</td>
    </tr>
	<tr>
      <td class="left_title_1">会员积分达到多少免审核：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="score_passed" value="<?php echo $GLOBALS['setting']['score_passed'];?>">(会员积分达到多少后发布商品时自动通过审核)</td>
    </tr>
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
