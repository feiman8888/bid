<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>类别管理</title>
</head>
<body>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function addauto()
{
	url="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=automodify&gid=<?php echo $gid;?>";
	$.get(url, function(data){
		$('#auto_area').html(data);
		$('#auto_area').show();
	}); 
}
function parseMyAutoData(data)
{
	var objdata=data;
	$('#auto0').after('<TR class="tr1" id="auto'+objdata.id+'"><TD width="80px">'+objdata.uid+'</TD><TD width="80px">'+objdata.username+'</TD><TD width="80px">'+objdata.number+'</TD><TD width="80px">'+objdata.second+'</TD><TD width="80px">'+objdata.maxnumber+'</TD><td width="70px" align="center"><A HREF="javascript:deleteVal(\'auto\',\''+objdata.id+'\',\'auto'+objdata.id+'\')">删除</A></td></TR>');
	$('#auto_area').hide();
}
//-->
</SCRIPT>
<div class="list">
<TABLE cellpadding="1" cellspacing="1" style="width:620px;margin:auto;">
<TR>
	<TH>会员id</TH>
	<TH>会员名</TH>
	<TH>出价次数</TH>
	<TH>指定出价时间</TH>
	<TH>多少次后出价</TH>
	<TH>操作</TH>
</TR>
<TR class="tr1" id="auto0">
	<TD width="80px">
	</TD>
	<TD width="80px">
	</TD>
	<TD width="80px">
	</TD>
	<TD>
	多个用半脚逗号隔开
	</TD>
	<TD width="120px">
	0为到点就出价
	</TD>
	<td width="70px" align="center">
	<A HREF="javascript:addauto();">添加</A>
	</td>
</TR>
<?php 
if(!empty($autolist)){
foreach($autolist as $key=>$val){?>
<TR class="tr1" id="auto<?php echo $val['id'];?>">
	<TD width="80px" align="center">
	<?php echo $val['uid'];?>
	
	</TD>
	<TD width="80px" align="center">
	<div id="username-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('usernameinput-<?php echo $val['id'];?>');"><?php echo $val['username'];?>
	</div>
	<input class="hideinput" id="usernameinput-<?php echo $val['id'];?>" ondblclick="confirmValue('autobuy',this.value,'usernameinput-<?php echo $val['id'];?>','id');"/>
	</TD>
	<TD width="80px" align="center">
	<div id="number-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('numberinput-<?php echo $val['id'];?>');"><?php echo $val['number'];?>
	</div>
	<input class="hideinput" id="numberinput-<?php echo $val['id'];?>" ondblclick="confirmValue('autobuy',this.value,'numberinput-<?php echo $val['id'];?>','id');"/>
	</TD>
	<TD align="center">
	<div id="second-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('secondinput-<?php echo $val['id'];?>');"><?php echo $val['second'];?>
	</div>
	<input class="hideinput" id="secondinput-<?php echo $val['id'];?>" ondblclick="confirmValue('autobuy',this.value,'secondinput-<?php echo $val['id'];?>','id');"/>
	</TD>
	<TD width="120px" align="center">
	<div id="maxnumber-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('maxnumberinput-<?php echo $val['id'];?>');"><?php echo $val['maxnumber'];?>
	</div>
	<input class="hideinput" id="maxnumberinput-<?php echo $val['id'];?>" ondblclick="confirmValue('autobuy',this.value,'maxnumberinput-<?php echo $val['id'];?>','id');"/>
	</TD>
	<td width="70px" align="center">
	<A HREF="javascript:deleteVal('autobuy','<?php echo $val['id'];?>','auto<?php echo $val['id'];?>')">删除</A>
	</td>
</TR>
<?php }}?>
</TABLE>

<ul class="page"><?php echo $pageinfo;?></ul>
</div>
<DIV id="auto_area" style="display:none;border:2px solid #ccc;width:300px;position:absolute;left:260px;top:60px;background:#fff;z-index:10px;"></DIV>
</body>
</html>
