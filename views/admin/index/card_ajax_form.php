<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
header("content-type:text/html;charset=utf-8
");
?>
<iframe src="" name="sitemodify" style="display:none"></iframe>
<FORM METHOD="POST" ACTION="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=cardmodify" target="sitemodify">
<INPUT TYPE="hidden" NAME="updateid" value="<?php echo $card['id'];?>">
<INPUT TYPE="hidden" NAME="commit" value="1">
<table width="300px" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
<tr>
<td width="90px" class="left_title_1"><span class="left-title">卡片编号：</span></td>
<td>
<INPUT TYPE="text" NAME="cardnumber" class="normal_txt"  value="<?php echo $card['cardnumber'];?>">
</td>
</tr>
<tr>
<td width="90px" class="left_title_1"><span class="left-title">赠送人：</span></td>
<td>
<INPUT TYPE="text" NAME="username" class="normal_txt"   value="<?php echo $card['username'];?>">
</td>
</tr>
<tr>
<td width="90px" class="left_title_1"><span class="left-title">卡片面值：</span></td>
<td><INPUT TYPE="text" NAME="cardvalue" class="normal_txt"  value="<?php echo $card['cardvalue'];?>" ></td>
</tr>

<tr>
<td width="90px" class="left_title_1">卡片认证码：</td>
<td><INPUT TYPE="text" class="normal_txt" NAME="cardcode" value="<?php echo $card['cardcode']?$card['cardcode']:substr(md52(microtime()),rand(4,10),8);?>"></td>
</tr>
<tr>
<td width="90px" class="left_title_1">生效时间：</td>
<td><INPUT TYPE="text" class="normal_txt" NAME="updatetime" value="<?php echo $card['updatetime']?date('Y-m-d H:i:s',$card['updatetime']):date('Y-m-d H:i:s');?>" onfocus="HS_setDate(this)"></td>
</tr>
<tr>
<td width="90px" class="left_title_1">失效时间：</td>
<td><INPUT TYPE="text" class="normal_txt" NAME="lasttime" value="<?php echo $card['lasttime']?date('Y-m-d H:i:s',$card['lasttime']):date('Y-m-d H:i:s');?>" onfocus="HS_setDate(this)" ></td>
</tr>
<tr>
<td colspan="2" align="center"><INPUT TYPE="submit" value="提交" class="normal_button"> <INPUT TYPE="button" value="关闭" class="normal_button" onclick="$('#card_area').hide();"></td>
</tr>
</table>
</FORM>