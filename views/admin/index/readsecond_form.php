<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>秒杀管理</title>
</head>
<body>


<div id="man_zone">
   <form enctype="multipart/form-data" action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=readsecondmodify" method="post">
	<INPUT TYPE="hidden" NAME="commit" value="1">
  <INPUT TYPE="hidden" NAME="updateid" value="<?php echo $goods['goods_id'];?>">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">商品名称：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt"  NAME="goods_name" id="subject"  size="70" value="<?php echo $goods['goods_name'];?>"><SPAN CLASS="" id="subjecterror"></SPAN></td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">所属分类:</span></td>
      <td width="82%">
	  <SELECT NAME="cateid">
		<?php if($goods['cateid']){?><option value="<?php echo $goods['cateid'];?>"><?php echo $GLOBALS['cate'][$goods['cateid']]['catename'];?></option><?php }?>
		<?php foreach($GLOBALS['cate'] as $k=>$v){?>
		<option value="<?php echo $k;?>"><?php echo $v['catename'];?></option>
		<?php }?>
	  </SELECT>
	  </td>
    </tr>
	 <tr>
      <td class="left_title_2">开始日期：</td>
      <td> <INPUT TYPE="text" class="normal_txt"  NAME="starttime"  value="<?php echo date('Y-m-d H:i:s',$goods['starttime']?$goods['starttime']:time()+300);?>" onfocus="HS_setDate(this);"></td>
    </tr>
	 <tr>
      <td class="left_title_2">结束日期：</td>
      <td> <INPUT TYPE="text" class="normal_txt"  NAME="lasttime"  value="<?php echo date('Y-m-d H:i:s',$goods['lasttime']?$goods['lasttime']:(time()+600));?>" onfocus="HS_setDate(this);"></td>
    </tr>
    <tr>
      <td class="left_title_2">秒杀数量：</td>
      <td> <INPUT TYPE="text" class="normal_txt"  NAME="number"  value="<?php echo $goods['number'];?>"></td>
    </tr>
	
	<tr>
      <td class="left_title_1" valign="top">缩略图：<INPUT TYPE="button" VALUE="+" class="normal_button" ONCLICK="addmthumb();"></td>
      <td>
	  <div id="thumbarea">
	  <div><INPUT TYPE="file" NAME="local_thumb[]" class="normal_button"></div>
	  </div>
	   <?php 
	   if($goods['thumb']){
	   $thumb=explode(',',$goods['thumb']);
	   foreach($thumb as $k=>$v){
	   ?>
	   <div style="float:left;width:100px;height:140px;overflow:hidden;text-align:center;" id="thumb<?php echo $k;?>"><img onclick="window.open(this.src,'','');" src="<?php echo $v;?>" height='100px'/><br/><INPUT TYPE="button" VALUE="删除" ONCLICK="deletethumb('<?php echo $v;?>','thumb<?php echo $k;?>');"></div>
	   <?php }}?>
	  </td>
    </tr>
	<tr>
      <td class="left_title_1">一口价：</td>
      <td>￥<INPUT TYPE="text" class="normal_txt"  NAME="marketprice" value="<?php echo $goods['marketprice'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">秒杀价：</td>
      <td>￥<INPUT TYPE="text" class="normal_txt"  NAME="nowprice" value="<?php echo $goods['nowprice'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">幸运号码：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="goodluck" value="<?php echo $goods['goodluck'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">必须回答问题：</td>
      <td>
	  <SELECT NAME="mustquestion">
		<OPTION VALUE="1" <?php echo $goods['mustquestion']==1?'SELECTED':'';?>>是
		<OPTION VALUE="2" <?php echo $goods['mustquestion']==2?'SELECTED':'';?>>否
	  </SELECT></td>
    </tr>
	<tr>
	<SCRIPT LANGUAGE="JavaScript">
	<!--
		function addquestion()
		{
			$('#questionarea').append('<div>问题：<INPUT TYPE="text" NAME="question[]" class="normal_txt"> 答案：<INPUT TYPE="text" NAME="answer[]" class="normal_txt" size="50"> <INPUT TYPE="button" class="normal_button" VALUE="-"  onclick="$(this).parent().remove();"></div>');
		}
	//-->
	</SCRIPT>
      <td class="left_title_1" valign="top">问题：<INPUT TYPE="button" VALUE="+" class="normal_button" ONCLICK="addquestion();"></td>
      <td>
	  <div id="questionarea">
	  <div>问题：<INPUT TYPE="text" NAME="question[]" class="normal_txt"> 答案：<INPUT TYPE="text" NAME="answer[]" class="normal_txt" size="50"></div>
	  </div>
	   <?php 
	   if($goods['question']){
	   foreach($goods['question'] as $k=>$v){
	   ?>
	    <div id="q<?php echo $k;?>">问题：<INPUT TYPE="text" NAME="question[]" value="<?php echo $v['question'];?>" class="normal_txt"> 答案：<INPUT TYPE="text" NAME="answer[]" class="normal_txt" value="<?php echo $v['answer'];?>" size="50"><INPUT TYPE="button" VALUE="-" ONCLICK="$('#q<?php echo $k;?>').remove();"></div>
	   <?php }}?>
	  </td>
    </tr>
	<tr>
      <td class="left_title_1">奖励积分：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="diffscore" value="<?php echo $goods['diffscore'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">运费：</td>
      <td>￥<INPUT TYPE="text" class="normal_txt"  NAME="yunfei" value="<?php echo $goods['yunfei'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">商家：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="company" value="<?php echo $goods['company'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">品牌：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="brand" value="<?php echo $goods['brand'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1" valign="top">简介：</td>
      <td><textarea NAME="content" rows="8" cols="40"><?php echo $goods['content'];?></textarea></td>
    </tr>
	<tr>
      <td class="left_title_1" valign="top">商家信息：</td>
      <td>
	  <?php echo edit($goods['companyinfo'],'companyinfo','companyinfo');?></td>
    </tr>
	
	<tr>
      <td class="left_title_1" valign="top">详情：</td>
      <td>
	 <DIV ID="" CLASS="">
		 <?php echo edit($goods['details'],'details','details');?>
	  </DIV>
	  </td>
    </tr>
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
