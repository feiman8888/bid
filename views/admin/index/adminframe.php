<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<title>管理区域</title>
</head>

<body>
<div id="man_zone">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">产品总数：</span></td>
      <td width="82%">正在进行：<?php echo intval($count['goods']['load']);?>　已完成：<?php echo intval($count['goods']['complete']);?>　未开始：<?php echo intval($count['goods']['nostart']);?>　<span class="nav_return"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=goods">产品管理</A></span></td>
    </tr>
    <tr>
      <td class="left_title_1">会员总数：</td>
      <td>普通会员：<?php echo intval($count['usercount']['normaluser']);?>　管理员：<?php echo intval($count['usercount']['adminuser']);?> <span class="nav_return"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=normaluser">管理会员</A></span></td>
    </tr>
    <tr>
      <td class="left_title_2">订单总数：</td>
      <td>未付款：<?php echo intval($count['ordercount']['nopay']);?>　已付款：<?php echo intval($count['ordercount']['pay']);?>　已发货：<?php echo intval($count['ordercount']['send']);?>　已确认：<?php echo intval($count['ordercount']['confirm']);?>　已取消：<?php echo intval($count['ordercount']['cancel']);?>　<span class="nav_return"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=order">管理订单</A></span></td>
    </tr>
   <tr>
      <td class="left_title_1" valign="top">技术支持：</td>
      <td>
	  <DIV ID="" CLASS="">
		<A HREF="http://bidcms.com/bbs">bidcms.com/bbs</A>
	  </DIV>
	  </td>
    </tr>
   <tr>
      <td colspan="2">
		<iframe src="http://bidcms.com/version.php" width="100%" height="200px" frameborder="0" scrolling="no"></iframe>
	  </td>
   </tr>
  </table>
</div>
</body>
</html>
