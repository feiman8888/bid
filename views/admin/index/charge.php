<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>充值卡管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>

<body>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function addcharge()
{
	url="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=chargemodify";
	$.get(url, function(data){
		$('#cate_area').html(data);
		$('#cate_area').show();
	}); 
}
function modifycharge(id)
	{
		url="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=chargemodify&updateid="+id;
		$.get(url, function(data){
			$('#cate_area').html(data);
			$('#cate_area').show();
		}); 
	}
function parseMyChargeData(data)
{
	var objdata=data;
	if(objdata.dotype=='update')
	{
		$('#title-'+objdata.id).html(objdata.title);
		$('#oldprice-'+objdata.id).html(objdata.oldprice);
		$('#nowprice-'+objdata.id).html(objdata.nowprice);
		$('#thumb-'+objdata.id).attr('src',objdata.thumb);
		$('#cate_area').hide();
	}
	else
	{
	$('#charge0').after('<TR class="tr1" id="charge'+objdata.id+'"><TD><img src="'+objdata.thumb+'"/></TD><TD>'+objdata.title+'</TD><TD align="center">'+objdata.oldprice+'</TD><TD align="center">'+objdata.nowprice+'</TD><td width="70px" align="center"><A HREF="javascript:deleteVal(\'charge\',\''+objdata.id+'\',\'cate'+objdata.id+'\')">删除</A></td></TR>');
	$('#cate_area').hide();
	}
}
//-->
</SCRIPT>
<div class="list">
<TABLE cellpadding="1" cellspacing="1" style="width:550px;margin:auto;">
<TR>
	<th>缩略图</th><th>充值卡名称</th><th><?php echo !empty($GLOBALS['setting']['site_money_name'])?$GLOBALS['setting']['site_money_name']:'金币';?></th><th>价格</th><th style="width:80px;">操作</th>
 </tr>
 <TR class="tr1" id="charge0">
	<TD>
	</TD>
	<TD>
	</TD>
	<TD>
	</TD>
	<TD>
	</TD>
	<td width="70px" align="center">
	<A HREF="javascript:addcharge();">添加</A>
	</td>
</TR>
<?php   foreach($chargelist AS $key => $val){     ?>
<TR class="tr1" id="charge<?php echo $val['id'];?>">
		<TD width="150px"><img src='<?php echo $val['thumb'];?>' id="thumb--<?php echo $val['id'];?>"/>
		</TD>
		<TD width="150px">
		<div id="title-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('titleinput-<?php echo $val['id'];?>');"><?php echo $val['title'];?>
		</div>
		<textarea class="hideinput" id="titleinput-<?php echo $val['id'];?>" ondblclick="confirmValue('charge',this.value,'titleinput-<?php echo $val['id'];?>','id');"></textarea>
		</TD>
		<TD width="60px" align="center">
		<div id="oldprice-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('oldpriceinput-<?php echo $val['id'];?>');"><?php echo $val['oldprice'];?>
		</div>
		<input class="hideinput" id="oldpriceinput-<?php echo $val['id'];?>" ondblclick="confirmValue('charge',this.value,'oldpriceinput-<?php echo $val['id'];?>','id');" />
		</TD>
		<TD width="60px" align="center">
		<div id="nowprice-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('nowpriceinput-<?php echo $val['id'];?>');"><?php echo $val['nowprice'];?>
		</div>
		<input class="hideinput" id="nowpriceinput-<?php echo $val['id'];?>" ondblclick="confirmValue('charge',this.value,'nowpriceinput-<?php echo $val['id'];?>','id');" />
		</TD>
		<td align="center"> <A HREF="javascript:modifycharge('<?php echo $val['id'];?>');">修改</A> <a href="javascript:deleteVal('charge','<?php echo $val['id'];?>','charge<?php echo $val['id'];?>')">删除</a></td>
	</tr>
<?php  }    ?>
 </table>
 </div>
 <DIV id="cate_area" style="display:none;border:2px solid #ccc;width:300px;position:absolute;left:260px;top:60px;background:#fff;z-index:10px;"></DIV>
</body>
</html>