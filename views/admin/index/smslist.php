<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>短信订阅管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>
<form action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=multismsmodify" method="post">
<input type="hidden" name="commit" value="1"/>
<ul class="submenu" id="submenu">

<li class="<?php echo !isset($_GET['status'])?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=smslist">全部</A>
</li>
<li class="<?php echo $_GET['status']==1?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=smslist&status=1">已发送</A>
</li>
<li class="<?php echo $_GET['status']==2?'focus':'normal';?>"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=smslist&status=2">未发送</A></li>
</ul>
<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
	<TH>商品标题</TH>
	<TH>手机号</TH>
	<TH>订阅时间</TH>
	<TH>发送定制</TH>
	<TH>发送时间</TH>
	<TH>状态</TH>
	<TH>操作</TH>
</TR>
<?php foreach($smslist as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="smslist<?php echo $val['id'];?>">
	<TD><?php echo $val['goods_name'];?></TD>
	<TD width="60px" align="center"><?php echo $val['mobile'];?></TD>
	<TD width="140px" align="center"><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></TD>
	<TD width="60px" align="center"><?php echo $val['smstime'];?>分钟</TD>
	<td width="140px" align="center"><?php echo date('Y-m-d H:i:s',$val['sendtime']);?></td>
	<td width="60px" align="center">
	<?php echo $val['status']?'已发送':'未发送';?>
	</td>
	
	<TD align="center" width="80px"><?php if($val['status']==0){?><A HREF="javascript:setsms('<?php echo $val['id'];?>');">单独发送</A> <?php }?><A HREF="javascript:deleteVal('smslist','<?php echo $val['id'];?>','smslist<?php echo $val['id'];?>','id')">删除</A></TD>
</TR>
<?php }?>
</TABLE>
</div>
<div style="clear:both;"><input type="submit" class="normal_button" value="发送短信" onclick="return confirm('确认发送？');" name="confirmbutton"/>
</div>
<ul class="page"><?php echo $pageinfo;?></ul>
</form>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function setsms(sid)
	{
		$.post("?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=smsmodify",{ id:sid,'rand':Math.random() },
					   function(data){
						eval('var dataobj='+data);
						if(dataobj.datastatus=='success')
						{
							alert('发送成功');
						}
						else if(dataobj.datastatus=='failed')
						{
							alert('发送失败，错误码：'+datastatus.msg);
						}
						else if(dataobj.datastatus=='nodata')
						{
							alert('发送失败,参数错误');
						}
						else if(dataobj.datastatus=='error')
						{
							alert('发送失败,未到发送日期或手机为空');
						}
					   } 
					); 
	}
//-->
</SCRIPT>
</body>
</html>
