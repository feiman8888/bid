<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>团购管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>

<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
	<TH>用户名</TH>
	<TH>用户ID</TH>
	<TH>花费<?php echo $GLOBALS['setting']['site_money_name'];?></TH>
	<TH>出价次数</TH>
	<TH>IP</TH>
	<TH>时间</TH>
	<TH>商品</TH>
	<TH>操作</TH>
</TR>

<?php foreach($buyinfo as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="buylog<?php echo $val['id'];?>">
	<TD width="60px" align="center">
	<?php echo $val['username'];?>
	</TD>

	<TD width="60px" align="center">
	<?php echo $val['uid'];?>
	</TD>

	<TD width="70px" align="center">
	<?php echo $val['money'];?>
	</TD>
	<TD align="center" width="60px" align="center"><?php echo $val['count'];?></TD>
	
	<TD  align="center" width="70px"><?php echo $val['ip'];?></TD>

	

	<TD  align="center" width="160px" align="center"><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></TD>

	<td align="center" align="center"><A HREF="<?php echo url('index',$goosinfo[$val['gid']]['type'],array('id'=>$val['gid']));?>" target="_blank"><?php echo $goosinfo[$val['gid']]['name'];?></A>
	</td>
	<TD align="center" width="40px"><A HREF="javascript:wran('<?php echo $val['id'];?>');">删除</A></TD>
</TR>
<?php }?>
</TABLE>
</div>
<ul class="page"><?php echo $pageinfo;?></ul>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function wran(id)
	{
		if(confirm('删除后影响用户补差兑换,确认删除?'))
		{
			deleteVal('bidlog',id,'buylog'+id,'id');
		}
	}
//-->
</SCRIPT>
</body>
</html>
