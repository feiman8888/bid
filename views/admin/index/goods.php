<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>项目管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>
<form act="" method="get">
<input type="hidden" value="goods" name="act"/>
<input type="hidden" value="admin" name="con"/>
关键字：<input type="text" name="keyword" value=""/>
<input type="submit" value="搜索"/>
</form>
<form action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=multigoods" method="post">
<input type="hidden" name="commit" value="1"/>
<ul class="submenu" id="submenu">

<li class="<?php echo !isset($_GET['start'])?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=goods">全部</A>
</li>
<li class="<?php echo $_GET['start']==1?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=goods&start=1">正在进行中</A>
</li>
<li class="<?php echo $_GET['start']==2?'focus':'normal';?>"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=goods&start=2">未开始</A></li>
</ul>
<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
<TH>项目标题</TH>
	<TH>现价</TH>
	<TH>当前用户</TH>
	<TH>开始时间</TH>
	<TH>热门</TH>
	<TH>审核</TH>
	<TH>体验</TH>
	<TH>发布人</TH>	
	<TH>操作</TH>
</TR>
<?php foreach($goodslist as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="goods<?php echo $val['goods_id'];?>">
	
	<TD>
	<div id="goods_name-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('goods_nameinput-<?php echo $val['goods_id'];?>');"><?php echo $val['goods_name'];?>
	</div>
	<textarea class="hideinput" id="goods_nameinput-<?php echo $val['goods_id'];?>" ondblclick="confirmValue('goods_base',this.value,'goods_nameinput-<?php echo $val['goods_id'];?>','goods_id');"></textarea>
	</TD>
	
	<TD width="60px" align="center"><?php echo $val['nowprice'];?></TD>
	<TD width="60px" align="center"><?php echo $val['currentuser'];?>(<?php echo $val['currentuid'];?>)</TD>
	<TD width="140px" align="center"><font color="<?php echo $val['starttime']>time()?'red':'';?>"><?php echo date('Y-m-d H:i:s',$val['starttime']);?></font></TD>
	<td width="60px" align="center">
	<div id="ishot-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff0000';" onmouseout="this.style.backgroundColor='';" onclick="updateVal('goods_base','ishotinput-<?php echo $val['goods_id'];?>','goods_id','',['热门','不热门']);">
	<?php echo $val['ishot']?'热门':'不热门';?>
	</div>
	<INPUT TYPE="text" class="hideinput" id="ishotinput-<?php echo $val['goods_id'];?>" value="<?php echo intval(!$val['ishot']);?>" title='nochange'>
	</td>
	<td width="60px" align="center">
	<div id="ispassed-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff0000';" onmouseout="this.style.backgroundColor='';" onclick="updateVal('goods_base','ispassedinput-<?php echo $val['goods_id'];?>','goods_id','',['审核','未审核']);">
	<?php echo $val['ispassed']?'审核':'未审核';?>
	</div>
	<INPUT TYPE="text" class="hideinput" id="ispassedinput-<?php echo $val['goods_id'];?>" value="<?php echo intval(!$val['ispassed']);?>" title='nochange'>
	</td>
	<td width="60px" align="center">
	<div id="isfree-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff0000';" onmouseout="this.style.backgroundColor='';" onclick="updateVal('goods_base','isfreeinput-<?php echo $val['goods_id'];?>','goods_id','',['是','否']);">
	<?php echo $val['isfree']?'是':'否';?>
	</div>
	<INPUT TYPE="text" class="hideinput" id="isfreeinput-<?php echo $val['goods_id'];?>" value="<?php echo intval(!$val['isfree']);?>" title='nochange'>
	</td>
	<TD width="60px" align="center"><?php echo $val['hostuser'];?></TD>
	<TD align="center" width="150px"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=goodsmodify&updateid=<?php echo $val['goods_id'];?>">修改</A> <A HREF="javascript:deleteVal('goods_base','<?php echo $val['goods_id'];?>','goods<?php echo $val['goods_id'];?>','goods_id')">删除</A>  <A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=auto&gid=<?php echo $val['goods_id'];?>">设置自动出价</A></TD>
</TR>
<?php }?>
</TABLE>
</div>

<ul class="page"><?php echo $pageinfo;?></ul>
</form>

</body>
</html>
