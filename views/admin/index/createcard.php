<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>批量生成代金券管理</title>
</head>
<body>

<div id="man_zone">
   <form enctype="multipart/form-data" action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=createcard" method="post">
	<INPUT TYPE="hidden" NAME="commit" value="1">
  <INPUT TYPE="hidden" NAME="updateid" value="<?php echo $goods['goods_id'];?>">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">卡片前缀：</span></td>
      <td width="82%" colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="cardpre" value=""></td>
    </tr>
	<tr>
      <td class="left_title_1">开始数字：</td>
      <td colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="startnum"> 系统为10位卡号，如XXX0000001</td>
    </tr>
	 <tr>
      <td class="left_title_1">结束数字：</td>
      <td colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="endnum"> 系统为10位卡号，如XXX0000001</td>
    </tr>
	 <tr>
      <td class="left_title_1">生效日期：</td>
      <td style="width:210px;"> <INPUT TYPE="text" class="normal_txt" id="starttime" NAME="starttime"  value=""><INPUT TYPE="button" VALUE="选择" ONCLICK="HS_setDate(document.getElementById('starttime'))" class="normal_button"></td>
    </tr>
	<tr>
      <td class="left_title_1">失效日期：</td>
      <td> <INPUT TYPE="text" class="normal_txt"  NAME="endtime" id="lasttime" value=""><INPUT TYPE="button" VALUE="选择" ONCLICK="HS_setDate(document.getElementById('lasttime'))" class="normal_button"></td>
    </tr>
	
	
	<tr>
      <td class="left_title_1">面值：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="cardval" value=""><?php echo $GLOBALS['setting']['site_money_name'];?></td>
    </tr>
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
