<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="<?php echo STATIC_ROOT;?>jquery/jquery.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?>jquery/jquery-ui.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>

    <ul class="bidinfo_nav mar10">
    <li class="other"><a href="<?php echo url('user');?>" ><span>我参与的竞拍</span></a></li>
    <li class="other"><a href="<?php echo url('user','gain');?>"><span>成功的竞拍</span></a></li>
    <li class="<?php echo $type==1?'other':'thisclass';?>"><a href="<?php echo url('order','list');?>"><span>我的订单</span></a></li>
	<li class="<?php echo $type==1?'thisclass':'other';?>"><a href="<?php echo url('order','list',array('type'=>1));?>"><span>积分换购</span></a></li>
	<li class="other"><a href="<?php echo url('order','show');?>"><span>我的晒单</span></a></li>
  </ul>
  <div class="mem_box bpad10">
	<div class="mem_boxx">
  <div class="bidinfo_nav_txt01">
		<span class="s_w2 txtcenter" style="width:70px;">类型</span>
		<span class="s_w2 txtcenter">订单编号</span>
		<span class="s_w1 txtcenter">产品名称</span>
		<span class="s_w2 txtcenter">下单时间</span>
		<span class="s_w2 txtcenter" style="width:80px;">应付总额</span>
		<span class="s_w2 txtcenter" style="width:80px;">状态</span>
		<span class="s_w2 txtcenter">操作</span>
	</div>
	<!--订单开始-->
	<?php if($orderlist){foreach($orderlist as $key=>$val){?>
	<div class="mag_main"> 
	<span class="s_w2 txtcenter" style="width:70px;"><strong class="red"><?php echo $GLOBALS['order_type'][$val['order_type']];?></strong></span> 
	<span class="s_w2 txtcenter"><?php echo $val['order_no'];?></span> 
	<span class="s_w1 txtcenter"><A HREF="<?php echo url('index',$val['order_type']==1?'gdetails':'complete',array('id'=>$val['goods_id']));?>" target="_blank"><span class="blue"><?php echo $val['goods_name'];?></span></A></span> 
	<span class="s_w2 txtcenter"><?php echo date('y-m-d H:i',$val['updatetime']);?></span> 
	<span class="s_w2 txtcenter" style="width:80px;"> <strong class="yellow66 imgie6"><?php echo $val['total_fee'];?></strong></span>
	<span class="s_w2 txtcenter" style="width:80px;"><?php echo $GLOBALS['order_status'][$val['order_status']];?></span>
	<span class="s_w2 txtcenter blue"><?php if($val['order_status']<1){?>
	<A HREF="javascript:OrderPay_Dialog('<?php echo $val['id'];?>');">付款</A><?php }?>
	<?php if($val['order_status']<1){?>
	<A HREF="<?php echo url('order','manage',array('orderid'=>$val['id'],'dotype'=>'cancel'));?>" onclick="return confirm('确认取消此定单');">取消</A><?php }?>
	<?php if($val['order_status']>0 && $val['order_status']<3){?><A HREF="<?php echo url('order','manage',array('orderid'=>$val['id'],'dotype'=>'confirm'));?>" onclick="return confirm('确认收到货了？');">确认收货</A><?php }?>
	<?php if($val['order_status']==3){?><A HREF="<?php echo url('order','showmodify',array('orderid'=>$val['id']));?>">晒单</A><?php }?> 
	<A HREF="javascript:OrderView_Dialog('<?php echo $val['id'];?>');" >查看</A>
	</span>
	</div>
	<?php }}?>
	<!--订单结束-->
        <div class="pages" >
      <table align="center" class="mar10"  >
        <tbody>
          <tr>

            <td>
			<div class="list_page"> <?php echo $pageinfo;?></div>
              </td>
          </tr>
        </tbody>

      </table>
    </div>
  </div>
  <div class="public_corner public_bottomleft"></div>
	<div class="public_corner public_bottomright"></div>
	</div>
	
	</div>
  <div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->