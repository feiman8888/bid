<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>
    <ul class="bidinfo_nav mar10">
    <li class="other"><a href="<?php echo url('user');?>" ><span>我参与的竞拍</span></a></li>
    <li class="other"><a href="<?php echo url('user','gain');?>"><span>成功的竞拍</span></a></li>
    <li class="other"><a href="<?php echo url('order','list');?>"><span>我的订单</span></a></li>
	<li class="thisclass"><a href="<?php echo url('order','show');?>"><span>我的晒单</span></a></li>
  </ul>
<form name="account" method="post" action="<?php echo SITE_ROOT;?>/index.php?con=order&act=showmodify">
<INPUT TYPE="hidden" NAME="commit" value="1">
<INPUT TYPE="hidden" NAME="orderid" value="<?php echo $oid;?>">
<INPUT TYPE="hidden" NAME="updateid" value="<?php echo $showlist['id'];?>">
<p class="p">
<label class="left">标题：</label><input type="text" size="50" name="title" maxlength="100" value="<?php echo $showlist['title'];?>" class="input">
</p>
<div class="p" style="height:auto;"><label class="left">内容：</label>
<div class="left">
<?php echo $this->edit($showlist['content'],'content','content');?>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<div><input type="submit" value="确定"  onmouseout="this.className='layer_button'" onmousemove="this.className='layer_button_over'" class="layer_button">
<input type="button" value="取消"  onmouseout="this.className='layer_button'" onmousemove="this.className='layer_button_over'" class="layer_button" onclick="window.open('<?php echo url('order','show');?>','_self','');"/>
</div>
</form>

  </div>
  <div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->