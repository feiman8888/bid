<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
header("content-type:text/html;charset=utf-8");
?>
<div class="dialogdiv">
<div class="charge_after">
<div class="ca">
<span class="tsimg"></span>
<span class="txt">请您在新打开的页面完成购买！</span>
</div>
<div class="ca mar20">
完成购买前请不要关闭此窗口。<br>
完成购买后请根据您的情况点击下面的按钮：
</div>
<div class="ca mar20">
<input type="button" value="已完成付款" onmouseout="this.className='layer_button01'" onmousemove="this.className='layer_button_over01'" class="layer_button01" onclick="javascript:Dialog_close();window.location.reload();">
<input type="button" value="支付遇到问题" onmouseout="this.className='layer_button01'" onmousemove="this.className='layer_button_over01'" class="layer_button01" onclick="javascript:ChargeHelp_open();">
</div>
</div>
<div class="layer_msg layer_w22 mar20">
 	<span class="grayB4">
 	亲爱的用户，购买<?php echo $GLOBALS['setting']['site_money_name'];?>过程中如有任何无法解决的问题。请拨打免费电话：</span>400-666-3036<span class="grayB4">。我们真诚为您服务！
 	</span>
</div>
</div>