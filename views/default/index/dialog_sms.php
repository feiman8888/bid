<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
header("content-type:text/html;charset=utf-8");
?>
<div id="dialog_success" style="display:none">
<div class="sms_main04">
  <div class="fail_icons"></div>
  <div class="sms_succ font14"><strong class="font16">订阅成功！</strong><br>
   <?php echo $GLOBALS['setting']['site_title'];?>会在<strong class="red" id="sendshow"></strong>短信通知您。</div>
  <div class="sms_succ_button mar10">
    <input type="button" value="确 定" onclick="Dialog_close();" onmouseout="this.className='layer_button left dialog_close'" onmousemove="this.className='layer_button_over left dialog_close'" id="" class="layer_button left dialog_close">
  </div>
  <div class="clear"></div>
</div>

</div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function parseSmsError(dataobj)
	{
		if(dataobj.datastatus=='success')
		{
			$('#dialog').html($('#dialog_success').html());
			$("#sendshow").html(dataobj.sendtime);
		}
		else if(dataobj.datastatus=='nogoods')
		{
			alert('不存在此商品');
		}
		else if(dataobj.datastatus=='nomoney')
		{
			alert('金币不足,当前您的金币为'+dataobj.money);
		}
		else if(dataobj.datastatus=='nologin')
		{
			Login_Dialog();
		}
		else if(dataobj.datastatus=='nomobile')
		{
			alert('手机号不存在');
		}
		else if(dataobj.datastatus=='notime')
		{
			alert('请选择一个时间');
		}

	}
//-->
</SCRIPT>
<div class="sms_main_div">
<iframe src="" style="display:none;" name="smstime"></iframe>
<FORM METHOD="POST" ACTION="<?php echo SITE_ROOT;?>/index.php" target="smstime">
<INPUT TYPE="hidden" NAME="commit" value="1">
<INPUT TYPE="hidden" NAME="act" value="smslist">
<INPUT TYPE="hidden" NAME="con" value="index">

  <div class="sms_main">
    <div class="sms_main_name blue"> <a target="_blank" href="<?php echo url('index','goods',array('id'=>$goodsinfo['goods_id']));?>">[第<?php echo $goodsinfo['goods_id'];?>期] <?php echo $goodsinfo['goods_name'];?></a></div>
    <div class="sms_main_time"> <span class="smsimg"></span> <span class="smstxt">在竞拍结束前</span> <span class="smstxt">
      <select class="sms_select" id="sel_ftime" name="time">
        <option value="15" selected="">15分钟</option>
        <option value="20">20分钟</option>
        <option value="30">30分钟</option>
        <option value="40">40分钟</option>
        <option value="60">60分钟</option>      
		</select>
      </span> <span class="smstxt">通知我</span> </div>
    <div class="clear"></div>
  </div>
  <div class="sms_tips grayB4">每次订阅需要<?php echo $GLOBALS['setting']['smslist_money'];?><?php echo $GLOBALS['setting']['site_money_name'];?>。</div>
  <div class="sms_button">
	<input type="hidden" value="<?php echo $goodsinfo['goods_id'];?>" name="goodsid">
    <input type="submit" value="确 定" onmouseout="this.className='layer_button left'" onmousemove="this.className='layer_button_over left'" id="smsorder_submit" class="layer_button left">
    <span class="yellow14 cancel_txt dialog_close cursor" onclick="Dialog_close();">取 消</span></div>
  <div class="clear"></div>

</FORM>
</div>