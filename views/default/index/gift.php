<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/cate.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/global.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">
  <!--左侧部分开始-->
  <div class="class_left">	    
    <img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/category/scoreline.gif" alt="温馨提示" />
        <!--积分商品开始-->

    <div class="left_box01 borD6 mar10">
      <dl>
        <dt class="black"><strong>积分商品</strong></dt>
        <dd>
		<?php if($gift){foreach($gift as $key=>$val){
			$thumb=thumb($val['thumb']);
			$url=url('index','gdetails',array('id'=>$val['id']));
			?>
		 <!--单个开始-->
		  <div class="rightimg_txt">
			<div class="leftimg borEB">
			<a href="<?php echo $url;?>" title="<?php echo $val['subject'];?>">
			<img  width="90" height="76" src="<?php echo $thumb[0];?>" alt="<?php echo $val['subject'];?>">
			</a>
			</div>

			<table>
			  <tr>
				<td><div class="leftname">  <a href="<?php echo $url;?>" title="<?php echo $val['subject'];?>"><strong><?php echo $val['subject'];?></strong></a> </div>
				  <div class="leftprice"> <span class="gray62"> 市场价：<span class="line-through">￥<?php echo $val['marketprice'];?></span> </span> </div>

				  <div class="left"></div>
				  <div class="index_username"> 供应商：<span class="blue"><?php echo $val['company'];?></span> 所需积分：<strong class="yellow66"><?php echo $val['needcount'];?></strong> 库存：<strong class="yellow66"><?php echo $val['giftcount'];?></strong ></div></td>
			  </tr>

			</table>
			<div class="button"> 
					<a href="<?php echo $url;?>" title="<?php echo $val['subject'];?>" target="_blank">
					<img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/category/score.gif" onmouseout="this.src='<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/category/score.gif';" onmouseover="this.src='<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/category/score_over.gif';"  alt="立即换购" style="margin-top:25px;"/>
				</a>
							</div>
		  </div>
		  <!--单个结束-->
		  <?php }}?>
		  </dd>
      </dl>
      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <!--积分商品结束-->
        
    <div class="pages">

      <table align="center" style="margin: 10px auto;">
        <tbody>
          <tr>
            <td>
				<div class="list_page"><ul><?php echo $pageinfo;?></ul></div>

			</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <!--左侧部分结束-->
  <!--右侧部分开始-->
  <div class="class_right">

  <div class="class_right">
			<a href="<?php echo url('user','register');?>"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/head/register.gif" alt="注册即送500<?php echo $GLOBALS['setting']['site_money_name'];?>" /></a>
	</div>
        
        <!--拍友晒单开始-->
    <div class="right_box borD6 mar10">
      <dl>
        <dt class="black">
        	<strong class="left">拍友晒单</strong>
        </dt>
        <dd>
		<?php if($GLOBALS['show']){foreach($GLOBALS['show'] as $k=>$v){?>
   		  
          <div class="leftimg_txt">
            <div class="leftimg borEB"><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>" target="_blank"><img src="<?php echo $v['thumb'];?>" alt="<?php echo $v['goods_name'];?>" width="90" height="76" /></a></div>
            <table class="txt">
              <tr>
                <td><span class="leftname"><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>" target="_blank">
			<?php echo $v['title'];?></a></span> <span class="leftprice gray62">成交价：￥<?php echo $v['orderinfo']['goods_info']['nowprice'];?></span> <img src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=middle&type=virtual";?>" width="13" height="13" class="index_avatar" />
                  <div class="index_username blue"><?php echo $v['username'];?></div></td>

              </tr>
            </table>
          </div>
 
         <?php }}?>         
        </dd>
      </dl>
      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>

      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <!--拍友晒单结束-->
        
	<div class="right_box borD6 mar10">
	<dl>
			<dt class="black"><strong>常见问题</strong></dt>
			<dd>

			<ul class="problem">
				<li><a href="<?php echo url('article','help',array('id'=>5));?>" target="_blank">如何注册会员？</a></li>
				<li><a href="<?php echo url('article','help',array('id'=>6));?>" target="_blank">如何购买<?php echo $GLOBALS['setting']['site_money_name'];?>参与竞拍？</a></li>
				<li><a href="<?php echo url('article','help',array('id'=>7));?>" target="_blank">如何查看我订单状态？</a></li>
				<li><a href="<?php echo url('article','help',array('id'=>8));?>" target="_blank">如何获得免费<?php echo $GLOBALS['setting']['site_money_name'];?>？</a></li>
			</ul>
			</dd>

	  </dl>
		<div class="public_corner public_topleft2"></div>
		<div class="public_corner public_topright2"></div>
		<div class="public_corner public_bottomleft"></div>
		<div class="public_corner public_bottomright"></div>
	</div>
  </div>
  <!--右侧部分结束-->
  <div class="clear"></div>

</div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	show_alert();
//-->
</SCRIPT>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->