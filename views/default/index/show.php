<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/show.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">
	<!--左侧部分开始-->
	<div class="experience_left">
	 <?php if($showlist){foreach($showlist as $k=>$v){?>  
						<!--单个开始-->
		<div class="left_box01 borFC">

		<div class="ty">
			<div class="ty_left">
				<div class="experience_avatar"><img src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=middle&type=virtual";?>" width="66" height="66" /></div>
				<div class="experience_ueername blue"><?php echo $v['username'];?></div>
			</div>
		  <div class="ty_center borD6">
			  <div class="div_showsheet"></div>
		  <div class="tyc">

				<div class="name">
					<a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" target="_blank">
						<?php echo $v['title'];?>					</a>
					
				</div>
				<div class="time grayB4"><?php echo date('Y-m-d H:i:s',$v['updatetime']);?></div>
				<ul class="img">
				<?php $pic=getUploadPic($v['content'],3);
				foreach($pic as $key=>$val){
				?>
				<li><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" target="_blank"><img src="<?php echo $val;?>" width="93" height="76" /></a></li>
				<?php }?>
										
				</ul>
				<div class="txt">
				<span class="left">
					奖励<span class="red"><?php echo $v['givemoney'];?></span><?php echo $GLOBALS['setting']['site_money_name'];?>
								</span>
				<span class="right">

					回复<span class="red"><?php echo $v['comments'];?></span>&nbsp;
					浏览<span class="red"><?php echo $v['hits'];?></span>&nbsp;
					<span class="blue">
						<a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" target="_blank">
							更多详细						</a>					</span>				</span>				</div> 
			</div>

				<div class="ty_corner ty_topleft"></div>
				<div class="ty_corner ty_topright"></div>
				<div class="ty_corner ty_bottomleft"></div>
				<div class="ty_corner ty_bottomright"></div>
		  </div> 
			<div class="ty_right borD6">
				<div class="txt">
				<div class="tyr">
				<span class="a">

					
						<strong class="black"><A HREF="<?php echo $v['url'];?>" target="_blank"><?php echo $v['order_info']['goods_name'];?></A></strong>
					</a>
					<span style="color:gray;">[第<?php echo $v['order_info']['goods_id'];?>期]</span>
				</span>
				<span class="b">
				成交价：<span class="yellow66 font14"><strong>￥<?php echo $v['order_info']['goods_info']['nowprice'];?></strong></span><br />

				市场价：￥<?php echo $v['order_info']['goods_info']['marketprice'];?><br />
				共节省：<?php echo $v['order_info']['goods_info']['marketprice']-$v['order_info']['goods_info']['nowprice'];?><br />
				结束时间：<?php echo date('Y-m-d H:i:s',$v['order_info']['goods_info']['lasttime']);?>				</span>
				<span class="c blue">
					<a href="<?php echo $v['url'];?>" target="_blank">
					查看详细
					</a>
				</span>

				</div>
				<div class="div_qingdan">清单</div>
				<div class="ty_corner ty_topleft"></div>
				<div class="ty_corner ty_topright"></div>
				<div class="ty_corner ty_bottomleft"></div>
				<div class="ty_corner ty_bottomright"></div>
			</div></div>
		</div>

			<div class="public_corner public_topleft6"></div>
			<div class="public_corner public_topright6"></div>
			<div class="public_corner public_bottomleft6"></div>
			<div class="public_corner public_bottomright6"></div>
	  </div>
	  <!--单个结束-->
	  <?php }}?>
		<!--分页开始-->
		<div class="pages" >
		<table align="center" style="margin: 10px auto;">
		<tbody><tr>
		<td>
		<div class="list_page"><?php echo $pageinfo;?></div></td>
		</tr>
		</tbody></table>
		</div>
		<!--分页结束-->
	
	</div>
	<!--左侧部分结束-->
	<!--右侧部分开始-->
	<div class="experience_right">

	<!--拍友晒单开始-->

	<div class="right_box borD6">
		<dl>
			<dt class="black">
			<strong class="left">拍友晒单</strong>
			</dt>
			<dd>
			<?php if($GLOBALS['show']){foreach($GLOBALS['show'] as $k=>$v){?>
   		  
          <div class="leftimg_txt">
            <div class="leftimg borEB"><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>" target="_blank"><img src="<?php echo $v['thumb'];?>" alt="<?php echo $v['goods_name'];?>" width="90" height="76" /></a></div>
            <table class="txt">
              <tr>
                <td><span class="leftname"><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>" target="_blank">
				  <?php echo $v['title'];?></a> </span> <span class="leftprice gray62">成交价：￥<?php echo $v['orderinfo']['goods_info']['nowprice'];?></span> <img src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=middle&type=virtual";?>" width="13" height="13" class="index_avatar" />
                  <div class="index_username blue"><?php echo $v['username'];?></div></td>

              </tr>
            </table>
          </div>
 
         <?php }}?> 
        </dd>
		</dl>

		<div class="public_corner public_topleft2"></div>
		<div class="public_corner public_topright2"></div>
		<div class="public_corner public_bottomleft"></div>
		<div class="public_corner public_bottomright"></div>
	</div>
	<!--拍友晒单结束-->
	
	<div class="left_box borD6 mar10">

	<dl>
			<dt class="black"><strong>常见问题</strong></dt>
			<dd>
			<ul class="problem">
				<li><a href="<?php echo url('article','help',array('id'=>5));?>" target="_blank">如何注册会员？</a></li>
				<li><a href="<?php echo url('article','help',array('id'=>12));?>" target="_blank">如何购买<?php echo $GLOBALS['setting']['site_money_name'];?>参与竞拍？</a></li>
				<li><a href="<?php echo url('article','help',array('id'=>13));?>" target="_blank">如何查看我订单状态？</a></li>

				<li><a href="<?php echo url('article','help',array('id'=>21));?>" target="_blank">如何获得免费<?php echo $GLOBALS['setting']['site_money_name'];?>？</a></li>
			</ul>
			</dd>
	  </dl>
		<div class="public_corner public_topleft2"></div>
		<div class="public_corner public_topright2"></div>
		<div class="public_corner public_bottomleft"></div>
		<div class="public_corner public_bottomright"></div>

	</div>
	
	</div>
	
<div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->