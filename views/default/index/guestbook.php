<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/feedback.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="<?php echo STATIC_ROOT;?>jquery/jquery.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?>jquery/jquery-ui.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?>jquery/jquery.form.min.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function checksubmit(theform)
	{
		if(theform.title.value=='')
		{
			alert('标题不能为空');
			return false;
		}
		if(theform.content.value=='')
		{
			alert('内容不能为空');
			return false;
		}
		if(theform.txt_checkcode.value=='')
		{
			alert('验证码不能为空');
			return false;
		}
		return true;
	}
//-->
</SCRIPT>

<div class="container980">
  <!--左侧部分开始-->
  <div class="experience_left">

    <!--反馈列表开始-->
        <div class="experience_box borFC mar10">
      <dl>
        <dt class="black"> <strong class="left">意见反馈</strong>
          <div class="more grayB4"> 采纳后获得提问奖励 </div>
        </dt>

        <dd>
          <!--单个开始-->
		  <?php if($guestbook){foreach($guestbook as $v){?>
            <div class="feedback_box bg2"> <span class="name" >
           <?php if($v['isgood']){?> <div class="green">已采纳</div><?php }?>
            <strong class="black left" style="line-height:18px;"><?php echo $v['title'];?></strong>
            <?php if($v['givemoney']>0){?><span class="addxibi lmar10"></span> <strong class="yellow66 left" style="line-height:18px;">奖励<?php echo $v['givemoney'];?><?php echo $GLOBALS['setting']['site_money_name'];?></strong><?php }?>
			</span>
            <div class="user">
              <div class="index_username grayB4">提问人：</div>
              <div class="index_username"><span class="blue">
              <?php if($v['uid']){?><img src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=middle&type=virtual";?>" width="13" height="13" class="index_avatar" /><?php }?><?php echo $v['username'];?></span><span class="grayB4"><?php echo date('Y-m-d H:i:s',$v['updatetime']);?></span> </div>
              <div class="txt"><?php echo $v['content'];?></div>
			  <?php if($v['reply']){?>
				<div class="user mar10 yellow66"><span class="div_admin"></span><strong><?php echo $GLOBALS['setting']['site_title'];?>回复</strong></div>
              <div class="txt01 yellow66"><?php echo $v['reply'];?></div>
			  <?php }?>
               </div>
          </div>
		  <?php }}?>
		  <!--单个结束-->
        </dd>
		<STYLE TYPE="text/css">
			.txtcenter li{float:left;padding-left:5px;padding-right:5px;}
		</STYLE>
        <dd class="txtcenter"><ul><?php echo $pageinfo;?></ul></dd>

      </dl>
      <div class="public_corner public_topleft6"></div>
      <div class="public_corner public_topright6"></div>
      <div class="public_corner public_bottomleft6"></div>
      <div class="public_corner public_bottomright6"></div>
    </div>
        <!--反馈列表结束-->
    <!--反馈列表开始-->
    <div class="experience_box borFC mar10">

      <dl>
        <dt class="black"> <a name="jy" id="jy"></a> <strong class="left">我的建议</strong>
          <div class="more grayB4"> 您不登录也可以提建议 </div>
        </dt>
        <dd>
          <form method="post" action="<?php echo SITE_ROOT;?>/index.php" id="form1" data-type="ajax" onsubmit="return checksubmit(this)">
			<INPUT TYPE="hidden" NAME="commit" value="1"><INPUT TYPE="hidden" NAME="con" value="index"><INPUT TYPE="hidden" NAME="act" value="guestbook">
            <div class="feedback_box01">
            <span class="newmsg red left mar10"> <span class="div_msg"></span>提示：登录后提建议，一旦被采纳将可以获得一定数量的<?php echo $GLOBALS['setting']['site_money_name'];?>。<?php if(!$GLOBALS['session']->get('uid')){?> <span class="blue"> <a href="javascript:Login_Dialog();">登录</a> </span> | <span class="blue"><a href="<?php echo url('user','register');?>">注册</a></span><?php }?> </span>
            <span class="newmsg red left"> <span  id="msg"></span></span> </div>

            <div class="feedback_box02"> <span class="txt">标题：</span> <span class="input">
              <input id='title' type="text" name="title" size="40" class="wenbens"/>
              <span class="wb01 grayB4">请至少输入4个汉字</span></span>
                            <span class="txt mar10">E-mail：</span> <span class="input mar10">
              <input type="text" id='email' name="email" class="wenbens"/>
              <span class="wb01 grayB4">希望您能留下联系方式，以便我们对您的反馈进行回复。</span></span>

                            <span class="txt mar10">内容： </span> 
			  <span class="input mar10">
              <textarea id='content' name="content" cols="60" rows="5" class="textarea"></textarea>
              </span> 
              <span class="txt mar10">验证码：</span>
			   <span class="input mar10">
			  <input type="text" name="txt_checkcode"  class="hengma"/><img style="height:30px;" src="" onclick="getCode();" id="txt_code"/></span>

              <span class="submit">
              <input type="button" id="btn_submit" name="submit" value="提交" class="contrbutton" />
              </span>
               
              </div>
          </form>
        </dd>
      </dl>
      <div class="public_corner public_topleft6"></div>
      <div class="public_corner public_topright6"></div>

      <div class="public_corner public_bottomleft6"></div>
      <div class="public_corner public_bottomright6"></div>
    </div>
    <!--反馈列表结束-->
  </div>
  <!--左侧部分结束-->
  <!--右侧部分开始-->
  <div class="experience_right">
  <!--拍友晒单开始-->

  	<div class="left_box borD6 mar10">
		<dl>
			<dt class="black">
			<strong class="left">拍友晒单</strong>
			</dt>
			<dd class="bpad10">
					
			<?php if($GLOBALS['show']){foreach($GLOBALS['show'] as $k=>$v){?>
   		  
          <div class="leftimg_txt">
            <div class="leftimg borEB"><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>" target="_blank"><img src="<?php echo $v['thumb'];?>" alt="<?php echo $v['goods_name'];?>" width="90" height="76" /></a></div>
            <table class="txt">
              <tr>
                <td>
                  <span class="leftname"><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>" target="_blank"><?php echo $v['title'];?></a></span> 
                  <span class="leftprice gray62">成交价：￥<?php echo $v['orderinfo']['goods_info']['nowprice'];?></span> 
                  <img src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=middle&type=virtual";?>" width="13" height="13" class="index_avatar" />
                  <div class="index_username blue"><?php echo $v['username'];?></div></td>

              </tr>
            </table>
          </div>
 
         <?php }}?> 
		 </dd>
		</dl>

		<div class="public_corner public_topleft2"></div>
		<div class="public_corner public_topright2"></div>
		<div class="public_corner public_bottomleft"></div>
		<div class="public_corner public_bottomright"></div>
	</div>
		<!--拍友晒单结束-->
    <!--常见问题开始-->
    <div class="right_box borD6 mar10">
      <dl>

        <dt class="black"><strong>常见问题</strong></dt>
        <dd>
          <ul class="problem">
            <li><a href="/help/register.html" target="_blank">如何注册会员？</a></li>
              <li><a href="/help/dzpayment.html" target="_blank">如何购买<?php echo $GLOBALS['setting']['site_money_name'];?>参与竞拍？</a></li>
              <li><a href="/help/my-account.html" target="_blank">如何查看我订单状态？</a></li>
              <li><a href="/help/faq.html" target="_blank">如何获得免费<?php echo $GLOBALS['setting']['site_money_name'];?>？</a></li>

          </ul>
        </dd>
      </dl>
      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <!--常见问题结束-->

  </div>
  <!--右侧部分结束-->
  <div class="clear"></div>
</div>
<!--底部-->
<script type="text/javascript">
	$('#btn_submit').click(function(){
		$('form[data-type="ajax"]').ajaxSubmit({
			success:function(res){
                var data = JSON.parse(res);
				if(data.code == 0){
                    alert(data.msg);
					if(data.url!=''){
                      window.location.replace(data.url);
                    } else {
                      window.location.reload();
                    }
				} else {
                  alert(data.msg);
                }
				
			}
		}); 
	});
  getCode();
</script>
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->