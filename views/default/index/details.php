<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/bid.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/cookie.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/global.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">
  <div class="left t8"> <span class="left name"><?php echo $info['goods_name'];?> <span class="grayB4">[第<?php echo $info['goods_id'];?>期]</span></span>
        <?php if($info['backmoney']>0){?><div class="figure left"><span class="font14b white" title="出价未获胜者可返还<?php echo $info['backmoney'];?>%<?php echo $GLOBALS['setting']['site_money_name'];?>">返币<?php echo $info['backmoney'];?>%</span></div><?php }?>

       	 
	<div class="bidsms"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/bid/bidsms.gif" style='cursor:pointer;' onclick="javascript:SmsBidNotifyDialog('<?php echo $info['goods_id'];?>');" alt="在竞拍结束前短信通知我"  /></div>
            <div class="right bidt3"> <span class="weiguan">当前围观：<span id="ccuser_<?php echo $info['goods_id'];?>"><?php echo count($ips);?></span>人 </span><span id="imgSpeed" class="speed00 " alt="实时网速"></span> 
			<SCRIPT LANGUAGE="JavaScript">
			<!--
				testspeed();
				cuser("<?php echo $info['goods_id'];?>");
			//-->
			</SCRIPT>
			</div>
        <div class="clear"></div>
    <div class="nbid_bg"></div>
    <div class="clear"></div>
  </div>

 
</div>
<div class="clear"></div>
<div class="container980" style="position:relative; z-index:6">
  <div class="nleft">
	<SCRIPT LANGUAGE="JavaScript">
	<!--
		function showbigimg(obj)
		{
			$('#small_'+obj).addClass('imghover');
			$('#big_'+obj).show();
		}
		function hidebigimg(obj)
		{
			$('#small_'+obj).addClass('img');
			$('#big_'+obj).hide('');
		}
	//-->
	</SCRIPT>
    <!--商品展示图片开始-->
    <div class="nleft_goods" >
      <div class="goods_pic" style="position:absolute;z-index:10;">
			<?php foreach($thumb as $k=>$img){?>
			<div class="goods_sigle" style="cursor:pointer;"> 
			<img src="<?php echo $img;?>" id="small_<?php echo $k;?>"  class="img" onmousemove="showbigimg(<?php echo $k;?>);" onmouseout="hidebigimg(<?php echo $k;?>);" width="63"  height="53"/>
			<div class="goods_magni bottom_right"></div>
			 <div class="goods_bigpica" id="big_<?php echo $k;?>" style="display:none;"> <img src="<?php echo $img;?>" width="289" height="241"/> </div>
			</div>
			<?php }?>       
      </div>
    </div>
    <!--商品展示图片结束-->
    <!--状态1:用户已经登录-->
    <div class="nleft_game borD6">
      <dl>

        <dt><span class="font16 lmar10"><strong>猜猜成交价</strong></span><span class="grayB4"> 玩游戏，赢<?php echo $GLOBALS['setting']['site_money_name'];?></span>
                    <div class="game_note" onmousemove="this.className='game_note_over'" onmouseout="this.className='game_note';$('.gamerule').hide();" id="gamerule" onmouseover="$('.gamerule').show();"><span>游戏规则</span></div>
	      <span class="gamerule" style="display:none;" onmouseout="$(this).hide();"  onmouseover="$(this).show();"><p>竞猜单双玩法：在竞拍结束之前预测<br />成交价小数点最后一位不为0的单双。</p>
<p class="tmar6">如竞猜成功，您将在本次竞拍中赢得<br /><span class="red">[投入竞猜<?php echo $GLOBALS['setting']['site_money_name'];?>数额 * 赢率]</span>的<?php echo $GLOBALS['setting']['site_money_name'];?>！</p>
<span class="tmar6 blue rpad10 right"><a href="/help/jingcaifuwu.html" target="_blank" title="查看详细规则">查看详细规则</a></span></span>

                  </dt>
        <dd>
          <?php include(VIEWS_PATH."public/guess.php");?>
        </dd>
      </dl>
      <div class="public_corner public_topleft"></div>
      <div class="public_corner public_topright"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <!--状态1结束-->

    <!--状态2-->
    <!--状态2结束-->
  </div>
        <!--竞拍中开始-->
  <div class="ncenter" id="divID_8289">
   <div class="ngoods_more">
      <p class="a grayB4">市场价<br /><span class="yellow66">￥<?php echo $info['marketprice'];?></span></p>
   <p class="b"><a href="<?php echo url('index','descgoods',array('id'=>$info['goods_id']));?>" target="_blank">商品详情</a></p>

      </div>
    <!--倒计时开始-->
    <div class="countdown" ><span name="lefttime" id="lasttime_<?php echo $info['goods_id'];?>">--:--:--</span></div>
    <!--倒计时结束-->
    <!--当前价开始-->

    <div class="txtcenter"><div class="end_txt033"><span class="bid_pricetxt" >当前价：</span><span class="bid_priceimg"></span><span id="nowprice_<?php echo $info['goods_id'];?>" name="price" class="bid_pricefont">-.--</span></div></div>
    <!--当前价结束-->
    <!--出价人开始-->
    <div class="nbid_user">
            <img name="uhead" id="avatar_<?php echo $info['goods_id'];?>" data-uid="<?php echo $info['currentuid'];?>" src="<?php echo UC_API.'/avatar.php?uid='.$info['currentuid'].'&size=small';?>" width="46" height="46" />
          </div>
    <div class="blue bid_username"><span name="username" id="currentuser_<?php echo $info['goods_id'];?>" >---</span></div>

    <!--出价人结束-->
		<!--按钮开始-->
		<div class="bid_button">
		<?php if($info['button_status'] == 'ok'){?>
		<!--1.可以竞拍开始-->
		<input type="button" id="bid_<?php echo $info['goods_id'];?>" class="bidbutton" onmouseup="this.className='bidbuttonover'"  onmousemove="this.className='bidbuttonover'" onmouseout="this.className='bidbutton'" onmousedown="this.className='bidbuttondown';setInfo('<?php echo $info['goods_id'];?>');" name="bid_btn" />
		<div class="bid_num" id="mybidcount" title="您当前出价<?php echo $buycount;?>次" >
		<?php echo $buycount>0?$buycount:0;?>
		</div>
		<!--可以结束-->
		<?php } elseif($info['button_status'] == 'nologin'){?>
		<!--2.未登录开始-->
		<div id="bid_<?php echo $info['goods_id'];?>" class="bidlogin bid">
		<a href="javascript:Login_Dialog();" style="cursor:pointer;"></a>
		</div>
		<!--未登录结束-->
		<?php } elseif($info['button_status'] == 'needsign'){?>
		<!--3.需要报名开始-->
		<div id="bid_<?php echo $info['goods_id'];?>" class="bidapply bid">
		<a href="" style="cursor:pointer;"></a>
		</div>
		<!--需要报名结束-->	
		<?php } elseif($info['button_status'] == 'moneyerror'){?>
		<!--4.不足开始-->
		<div id="bid_<?php echo $info['goods_id'];?>" class="bidcharge bid">
		<a href="javascript:Charge_Dialog();" style="cursor:pointer;"></a>
		</div>
		<!--不足结束-->
		<?php } elseif($info['button_status'] == 'needconfirm'){?>
		<!--5.手机未验证开始-->
		<div id="bid_<?php echo $info['goods_id'];?>" class="bidphone bid">
		<a href="<?php echo url('user','mobile');?>" style="cursor:pointer;"></a>
		</div>
		<!--手机未验证结束-->
		<?php }?>
		</div>
		<!--按钮结束-->

    <!--自动出价开始-->
    <div class="auto_price">
            <span class="left lmar10">自动出价：</span><span class="radio"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/<?php echo empty($autoinfo)?'autoover':'autodown';?>.gif" id="to_start"  onclick="AutoBuy_Dialog('<?php echo $info['goods_id'];?>');"/></span><span class="left"  style="cursor:pointer;">开启&nbsp;</span><span class="radio_checked have_select"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/<?php echo empty($autoinfo)?'autodown':'autoover';?>.gif" id="to_cancel"  onclick="AutoBuy_Close('<?php echo $info['goods_id'];?>');"/></span><span class="left"  style="cursor:pointer;">关闭</span><a href="<?php echo url('article','help',array('id'=>24));?>" class="img" target="_blank"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/bid/nbid_44.gif" alt="如何使用自动出价"   /></a></span>
     </div>
    <!--自动出价结束-->
    <!--出价信息开始-->
    <div class="bid_msg">

            <?php if($GLOBALS['session']->get('uid')){?>剩余<?php echo $GLOBALS['setting']['site_money_name'];?>：<span name="bidcount" id="myremainb"><?php echo $GLOBALS['userinfo']['money'];?></span> <span class="blue"><a href="javascript:Charge_Dialog();" style="cursor:pointer;">快速购买</a></span><?php } else{?>您当前未登录,<span class="blue"><a href="javascript:Login_Dialog();" style="cursor:pointer;">快速登录</a></span><?php }?><br />
           	<span class="grayB4">
     	     	该商品快递费：￥<?php echo $info['yfeemoney'];?></span> 
      	</div>
    <!--出价信息结束-->
    <!--竞拍规则开始-->
    <div class="bid_ruleimg"></div>

    <div class="bid_ruletxt">
	      在倒计时<span class="red_b"><?php echo $info['difftime'];?></span>秒内，每次出价剩余时间将增长<span class="red_b"><?php echo $info['addtime'];?></span>秒。<br />
		   每次<img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/common/bid.gif" style="cursor:pointer;" class="img2bottom" /><span class="rolered" title="不同的<?php echo $GLOBALS['setting']['site_title'];?>注册会员 &#13;若上一个出价人是你则不扣除<?php echo $GLOBALS['setting']['site_money_name'];?>">出价</span>需要<span class="red_b" id="diffmoney"><?php echo $info['isfree']?1:$info['diffmoney'];?></span><?php echo $GLOBALS['setting']['site_money_name'];?>，成交价将<?php echo $info['diffprice']>0?'提高':'降低';?><span class="red_b"><?php echo abs($info['diffprice']);?></span>元。

</div>
    <!--竞拍规则结束-->
  
  </div>
  <!--竞拍中结束-->
      <!--正在竞拍中的结束-->

<?php if($goods_list){?>
    <INPUT TYPE="hidden" id="ids" value="<?php echo $info['goods_id'];?>">
    <INPUT TYPE="hidden" id="oids" value="<?php echo $info['goods_id'];?>">
    <textarea style="display:none;" id="goods_list"><?php echo json_encode($goods_list);?></textarea>
    <SCRIPT LANGUAGE="JavaScript">
          <!--
          getInfo();
          //-->
     </SCRIPT>
<?php }?>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function changtag(id)
	{
		ida=['chat','log','rank'];
		for(i in ida)
		{
			$('#'+ida[i]).removeClass('tab_current');
			$('#'+ida[i]).addClass('tab_other');
			$('#bid_'+ida[i]).hide();
		}
		$('#'+id).removeClass('tab_other');
		$('#'+id).addClass('tab_current');
		$('#bid_'+id).show();
	}
//-->
</SCRIPT>
  <!--右侧切换部分开始-->
  <div class="nright borD6">
    <dl>
      <dt id="nv">

        <div class="tab_current black" id="chat" onclick="changtag('chat');" title="聊天室">聊天室</div>
        <div class="tab_other black" id="log" onclick="changtag('log');" title="历史出价">出价记录</div>
        <div class="tab_other black" id="rank" onclick="changtag('rank');" title="出价排行">出价排行</div>
      </dt>
		<dd id="bid_log" style="display:none;">
		<ul class="bid_history" id="saleLog_<?php echo $info['goods_id'];?>">


		</ul>
		<SCRIPT LANGUAGE="JavaScript">
		<!--
			getBuyLog('<?php echo $info['goods_id'];?>','ajax',false,'price','saleLog');
		//-->
		</SCRIPT>
		</dd>
		<dd id="bid_rank" style="display:none;">
		<ul class="bid_top" id="countLog_<?php echo $info['goods_id'];?>">
         
        </ul>
		<SCRIPT LANGUAGE="JavaScript">
		<!--
			getBuyLog('<?php echo $info['goods_id'];?>','ajax',false,'count','countLog');
		//-->
		</SCRIPT>
		</dd>
        <dd id="bid_chat" style="display:block;">
        <ul class="bid_chat" id="ifrm_chat">
          <div id="divChatList" name="chat_list">
		  </div>
		   <SCRIPT LANGUAGE="JavaScript">
		   <!--
				getChat("<?php echo $info['goods_id'];?>",'ajax',false);
				function doface(obj)
				{
					setChat('<?php echo $info['goods_id'];?>',$(obj).attr('title'),'<?php echo $info['goods_name'];?>',$(obj).attr('data'));
					$('#face').hide();
				}
				function checklen(obj)
				{
					var len=$(obj).val().length;
					if(len<30)
					{
						$('#msgerror').html('目前还可以输入'+(30-len-1)+'个字');
					}
				}
		   //-->
		   </SCRIPT>
		   <div id="bid_message" class="bid_message">
            <div class="wide">
              <div class="l">发表留言<span name="spMsg" id="msgerror" class="red lmar6"></span></div>
              <span class="left greentime" id="spChatLeftTime"></span>
            </div>
                <span class="left">
            	<input type="text" name="txt_content" maxlength="30" <?php if(!$GLOBALS['session']->get('uid')){?>class="notxtContent txtContent" onclick="Login_Dialog();" value="您还未登陆，点此登陆"<?php } else{?>onkeydown="return checklen(this);" class="txtContent"<?php }?> id="txtContent">
				<div title="表情" style="cursor: pointer;" class="emotionsbutton" onclick="$('#face').toggle();"></div>
           		<input class="sendbutton" type="button" id="btnSubmit" value=""  name="chat_submit"  onmousemove="this.className='sendbutton_over'" data="0" onmouseout="this.className='sendbutton'" onclick="<?php if(isset($GLOBALS['setting']['site_chatmoney']) && $GLOBALS['setting']['site_chatmoney']>0){?>Warn_Dialog(this);<?php }?>setChat('<?php echo $info['goods_id'];?>',$('#txtContent').val(),'<?php echo $info['goods_name'];?>');$('#txtContent').val('');$(this).attr('data','1');"/>
            </span>
          </div>
		  <div class="clear"></div>
		  <div style="display:none ;" class="kb_tips" id="kb_tips"><span class="txtxibi"><?php echo $GLOBALS['setting']['site_money_name'];?></span><span class="xibijian" id="xibijian">-<?php echo $havestatus?$GLOBALS['setting']['site_chatmoney']:$GLOBALS['setting']['site_chatmoney2'];?></span><span class="imgxibi"></span></div>
          <div style="display:none" id="face" onmouseout="$(this).hide();" onmouseover="$(this).show();" class="emotions">
	<dl>
		<dt><span class="emo_bg">动一下</span></dt>
		<dd>
			<ul>
<li><img  onclick="doface(this);" data="002" title="不好意思，这个我要了！" src="<?php echo SITE_ROOT;?>/data/face/002_.gif"></li>
<li><img  onclick="doface(this);" data="003" title="不要这样说嘛，人家很害羞！" src="<?php echo SITE_ROOT;?>/data/face/003_.gif"></li>
<li><img  onclick="doface(this);" data="005" title="知道你过的不好，我就安心了！" src="<?php echo SITE_ROOT;?>/data/face/005_.gif"></li>
<li><img  onclick="doface(this);" data="009" title="淫荡的一天开始了！" src="<?php echo SITE_ROOT;?>/data/face/009_.gif"></li>
<li><img  onclick="doface(this);" data="010" title="送你一个吻！" src="<?php echo SITE_ROOT;?>/data/face/010_.gif"></li>
<li><img  onclick="doface(this);" data="012" title="小时候缺钙，长大缺爱！" src="<?php echo SITE_ROOT;?>/data/face/012_.gif"></li>
<li><img  onclick="doface(this);" data="013" title="世上最悲惨的是我！" src="<?php echo SITE_ROOT;?>/data/face/013_.gif"></li>
<li><img  onclick="doface(this);" data="015" title="菊花残，满身皆伤！" src="<?php echo SITE_ROOT;?>/data/face/015_.gif"></li>
<li><img  onclick="doface(this);" data="016" title="我是可怜人！" src="<?php echo SITE_ROOT;?>/data/face/016_.gif"></li>
<li><img  onclick="doface(this);" data="017" title="广告可以这样做，钱可以这样花！" src="<?php echo SITE_ROOT;?>/data/face/017_.gif"></li>
<li><img  onclick="doface(this);" data="018" title="不是吧？这样也行！" src="<?php echo SITE_ROOT;?>/data/face/018_.gif"></li>
<li><img  onclick="doface(this);" data="019" title="哇！这是我心仪的商品哦！" src="<?php echo SITE_ROOT;?>/data/face/019_.gif"></li>
<li><img  onclick="doface(this);" data="020" title="我是财迷，我怕谁！" src="<?php echo SITE_ROOT;?>/data/face/020_.gif"></li>
<li><img  onclick="doface(this);" data="021" title="恭喜你呀，恭喜你！" src="<?php echo SITE_ROOT;?>/data/face/021_.gif"></li>
<li><img  onclick="doface(this);" data="023" title="为什么！" src="<?php echo SITE_ROOT;?>/data/face/023_.gif"></li>
<li><img  onclick="doface(this);" data="024" title="是吗？我表示怀疑！" src="<?php echo SITE_ROOT;?>/data/face/024_.gif"></li>
<li><img  onclick="doface(this);" data="027" title="种草不让人躺，不如改种仙人掌！" src="<?php echo SITE_ROOT;?>/data/face/027_.gif"></li>
<li><img  onclick="doface(this);" data="028" title="你以为我会眼睁睁看你死吗？我会闭上眼的！" src="<?php echo SITE_ROOT;?>/data/face/028_.gif"></li>
<li><img  onclick="doface(this);" data="029" title="天干物燥，小心感冒！" src="<?php echo SITE_ROOT;?>/data/face/029_.gif"></li>
<li><img  onclick="doface(this);" data="031" title="好恶心，很想吐！" src="<?php echo SITE_ROOT;?>/data/face/031_.gif"></li>
<li><img  onclick="doface(this);" data="033" title="睡眠是艺术，谁也无法阻挡我追求艺术！" src="<?php echo SITE_ROOT;?>/data/face/033_.gif"></li>
<li><img  onclick="doface(this);" data="038" title="你太强了，佩服ing！" src="<?php echo SITE_ROOT;?>/data/face/038_.gif"></li>
<li><img  onclick="doface(this);" data="037" title="走自己的路让别人打车去吧！" src="<?php echo SITE_ROOT;?>/data/face/037_.gif"></li>
<li><img  onclick="doface(this);" data="039" title="哥抽的不是烟是寂寞！" src="<?php echo SITE_ROOT;?>/data/face/039_.gif"></li>

<li><img  onclick="doface(this);" data="036" title="很生气，别理我都！" src="<?php echo SITE_ROOT;?>/data/face/036_.gif"></li>
<li><img  onclick="doface(this);" data="040" title="有本事单挑，看看谁拳头硬！" src="<?php echo SITE_ROOT;?>/data/face/040_.gif"></li>
<li><img  onclick="doface(this);" data="042" title="………！" src="<?php echo SITE_ROOT;?>/data/face/042_.gif"></li>
<li><img  onclick="doface(this);" data="043" title="说话小心哦！小心我扁你！" src="<?php echo SITE_ROOT;?>/data/face/043_.gif"></li>
<li><img  onclick="doface(this);" data="044" title="I服了U！" src="<?php echo SITE_ROOT;?>/data/face/044_.gif"></li>
<li><img  onclick="doface(this);" data="046" title="命中有时终须有,命中无时莫强求！" src="<?php echo SITE_ROOT;?>/data/face/046_.gif"></li>
<li><img  onclick="doface(this);" data="049" title="掌声响起，我将离去！" src="<?php echo SITE_ROOT;?>/data/face/049_.gif"></li>

<li><img  onclick="doface(this);" data="066" title="同志们，我来了！" src="<?php echo SITE_ROOT;?>/data/face/066_.gif"></li>
<li><img  onclick="doface(this);" data="067" title="闭嘴行不！" src="<?php echo SITE_ROOT;?>/data/face/067_.gif"></li>
<li><img  onclick="doface(this);" data="068" title="竞拍无难事，只怕有心人！" src="<?php echo SITE_ROOT;?>/data/face/068_.gif"></li>
<li><img  onclick="doface(this);" data="069" title="上帝欲使其灭亡 必先使其疯狂！" src="<?php echo SITE_ROOT;?>/data/face/069_.gif"></li>
<li><img  onclick="doface(this);" data="070" title="再见！" src="<?php echo SITE_ROOT;?>/data/face/070_.gif"></li>
<li><img  onclick="doface(this);" data="071" title="好主意！" src="<?php echo SITE_ROOT;?>/data/face/071_.gif"></li>
<li><img  onclick="doface(this);" data="076" title="手气决定命运，难怪你命运如此坎坷！" src="<?php echo SITE_ROOT;?>/data/face/076_.gif"></li>
<li><img  onclick="doface(this);" data="077" title="哎。。没喜币了！" src="<?php echo SITE_ROOT;?>/data/face/077_.gif"></li>
<li><img  onclick="doface(this);" data="078" title="我算算划算不！" src="<?php echo SITE_ROOT;?>/data/face/078_.gif"></li>
<li><img  onclick="doface(this);" data="079" title="I don’t know 不要问我哦！" src="<?php echo SITE_ROOT;?>/data/face/079_.gif"></li>
<li><img  onclick="doface(this);" data="080" title="仔细观察，小心求证！" src="<?php echo SITE_ROOT;?>/data/face/080_.gif"></li>
<li><img  onclick="doface(this);" data="081" title="大家好，我想死你们了！" src="<?php echo SITE_ROOT;?>/data/face/081_.gif"></li>
<li><img  onclick="doface(this);" data="082" title="我在沉思，不要打扰我！" src="<?php echo SITE_ROOT;?>/data/face/082_.gif"></li>
<li><img  onclick="doface(this);" data="083" title="无聊的时候最喜欢做的事就是发呆！" src="<?php echo SITE_ROOT;?>/data/face/083_.gif"></li>
<li><img  onclick="doface(this);" data="085" title="真舍不得你走！" src="<?php echo SITE_ROOT;?>/data/face/085_.gif"></li>
<li><img  onclick="doface(this);" data="088" title="我悄悄的来，挥一挥匕首，不留一个活口！" src="<?php echo SITE_ROOT;?>/data/face/088_.gif"></li>
<li><img  onclick="doface(this);" data="089" title="I’m sorry！" src="<?php echo SITE_ROOT;?>/data/face/089_.gif"></li>
<li><img  onclick="doface(this);" data="090" title="好累啊！要休息一下！" src="<?php echo SITE_ROOT;?>/data/face/090_.gif"></li>
<li><img  onclick="doface(this);" data="091" title="人生是一个茶几，上面放满了杯具！" src="<?php echo SITE_ROOT;?>/data/face/091_.gif"></li>
			</ul>
		</dd>
	</dl>
	<div class="emo_corner emo_topleft"></div>
	<div class="emo_corner emo_topright"></div>
    <div class="emo_corner emo_bottomleft"></div>
    <div class="emo_corner emo_bottomright"></div>
</div>
        </ul>	<div class="clear"></div>
      </dd>	<div class="clear"></div>
    </dl>	<div class="clear"></div>
    <div class="public_corner public_topleft"></div>

    <div class="public_corner public_topright"></div>
    <div class="public_corner public_bottomleft"></div>
    <div class="public_corner public_bottomright"></div>
  </div>
  <div class="clear"></div>
</div>
<!--右侧切换部分结束-->
<div class="clear"></div>

<?php if($showlist){?>
<!--拍友晒单开始-->
<div class="container980">
  <div class="mainbox borD6 mar10">
    <dl>
      <dt> <span class="box_nav">拍友晒单</span> </dt>
      <dd>
        <!--标题开始-->

        <div class="showsheet_title"> <span class="title black_b">标题</span> <span class="view black_b">回复/浏览</span> <span class="username black_b">拍友</span> <span class="time black_b">时间</span> </div>
        <!--标题结束-->
		<?php foreach($showlist as $k=>$info){
		$goodsinfo=unserialize($info['order_info']);
		$pic=getUploadPic($info['content']);
		?>
		<div class="showsheet"> <span class="title">
		<div class="ss_height"> <span class="aa"></span> <span class="blue"> <a href="<?php echo url('index','showinfo',array('id'=>$info['id']));?>" target="_blank" >

		<?php echo $info['title'];?>【第<?php echo $goodsinfo['goods_id'];?>期，以<?php echo $goodsinfo['nowprice'];?>元赢得此竞拍】 </a><span class="tmar12 lmar2"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/bid2/imgs.gif"  class="img2bottom" alt="" /></span> </span> </div>
		</span> <span class="view"><?php echo $info['comments'];?>/<?php echo $info['hits'];?></span> <span class="username blue"><?php echo $info['username'];?></span> <span class="time"><?php echo date('Y-m-d',$info['updatetime']);?></span>
		<ul class="showsheetimg">
		<?php foreach($pic as $infoal){?>
		<li> <a href="<?php echo url('index','showinfo',array('id'=>$info['id']));?>" target="_blank"><img src="<?php echo $infoal;?>" width="93" height="76"  alt="<?php echo $info['title'];?>"/></a> </li>
		<?php }?>
		</ul>

		</div>
		<?php }?>
		</dd>
    </dl>
    <span class="right rpad10 bpad8"> <a href="<?php echo url('index','show',array('id'=>$info['goods_id']));?>" target="_blank"> <img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/bid2/showsheet_more.gif" alt="查看更多" /> </a> </span>
    <div class="corner topleft3"></div>
    <div class="public_corner public_topright1"></div>
    <div class="public_corner public_bottomleft"></div>

    <div class="public_corner public_bottomright"></div>
  </div>
  <div class="clear"></div>
</div>
<!--拍友晒单结束-->
<?php }?>
<!--商品详细介绍开始-->
<div class="container980">
  <div class="mainbox borD6 mar10">
    <dl>
      <dt> <span class="box_nav">商品详细介绍</span> </dt>
      <dd><?php echo htmlspecialchars_decode($details['content']);?></dd>
    </dl>
    <span class="right rpad10" id="pd_top"><img id="pd_top" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/bid2/bid_top.gif" alt="回到顶部" width="59" height="20" border="0" class="cursor"/></span>
    <div class="corner topleft3"></div>
    <div class="corner topright"></div>

    <div class="corner bottomleft"></div>
    <div class="corner bottomright"></div>
  </div>
  <div class="clear"></div>
</div>
<!--商品详细介绍结束-->
<!--常见问题开始-->
<div class="container980">
  <div class="mainbox borD6 mar10">
    <dl>
      <dt> <span class="box_nav">常见问题</span> <span class="morefaq blue"><a href="/help/" target="_blank">其它常见问题</a></span> </dt>

      <dd>
        <div class="faq">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>竞拍成功后怎么做？</strong></span> </div>
          <div class="txt"> 首先恭喜您竞拍成功，进入“我的账户中心”您会看到刚刚竞拍成功的商品，在线支付成功后，必得喜客服人员会在两个工作日内与您联系确认订单和发货。 </div>
        </div>
        <div class="faq lmar10">

          <div class="title"> <span class="a"></span> <span class="left blue"><strong>没有竞拍成功怎么办？</strong></span> </div>
          <div class="txt"> 没关系，您可以在商品竞拍结束后的7天内使用“补差兑换”功能补上差价，即可不浪费您使用过的<?php echo $GLOBALS['setting']['site_money_name'];?>，购得该商品。 </div>
        </div>
        <div class="faq mar10">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>付款后多长时间可以收到货？</strong></span> </div>

          <div class="txt l96"> 必得喜是国内最专业的商品竞拍平台，为您提供公开、公平、公正的娱乐竞拍服务。我们的货源是与国内知名商城合作，包括京东、新蛋、卓越、当当和淘宝商城，所有商品均正品行货。一般情况在您支付成功后的第二个工作日内商品发出，经过3－5天到达。</div>
        </div>
        <div class="faq lmar10 mar10">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>收到货后晒单如何奖励？</strong></span> </div>
          <div class="txt"> 凡是竞拍成功的实物商品，都可以参与晒单，向其他拍友展示您的成果。晒单需要至少两张商品图片，一段自己的竞拍感言或心得，必得喜管理员会根据您的晒单内容质量给出1000－3000不等<?php echo $GLOBALS['setting']['site_money_name'];?>奖励。图片中若是有您的靓照，奖励翻倍。</div>

        </div>
      </dd>
    </dl>
    <div class="faqimg"> <img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/bid2/newbid_114.gif" alt="" />
      <div class="clear"></div>
    </div>
    <span class="right rpad10"><img id="faq_top" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/bid2/bid_top.gif" alt="回到顶部" width="59" height="20" border="0" class="cursor" /></span>
    <div class="corner topleft3"></div>

    <div class="public_corner public_topright1"></div>
    <div class="public_corner public_bottomleft"></div>
    <div class="public_corner public_bottomright"></div>
  </div>
  <div class="clear"></div>
</div>
<!--常见问题结束-->
<SCRIPT LANGUAGE="JavaScript">
<!--
	show_alert();
//-->
</SCRIPT>
<?php if(isset($GLOBALS['setting']['site_chatmoney'])){?>
<!--聊天扣除-->
<div id="warnchat" style="display:none;"><div class="autobid_div">
  <div class="autobid_main">
    <div class="autobid_time"><span class="smstxt">您每次发言仅需消耗：<span class="yellow66"><?php echo $havestatus?$GLOBALS['setting']['site_chatmoney']:$GLOBALS['setting']['site_chatmoney2'];?></span><?php echo $GLOBALS['setting']['site_money_name'];?>
      </span></div>
    <div class="clear"></div>
  </div>
  <div class="autobid_tips01 grayB4">购买过<?php echo $GLOBALS['setting']['site_money_name'];?>的用户每次发言需要消耗:<?php echo $GLOBALS['setting']['site_chatmoney'];?><?php echo $GLOBALS['setting']['site_money_name'];?><br/>未购买过<?php echo $GLOBALS['setting']['site_money_name'];?>的用户每次发言需要消耗:<?php echo $GLOBALS['setting']['site_chatmoney2'];?><?php echo $GLOBALS['setting']['site_money_name'];?></div>
  <div class="sms_button01">
    <input type="button" value="确定" onclick="Dialog_close();" onmouseout="this.className='layer_button left'" onmousemove="this.className='layer_button_over left'" id="autopay_submit" class="layer_button left">
    <span class="yellow14 cancel_txt dialog_close cursor" onclick="Dialog_close();" >取 消</span></div>
  <div class="clear"></div>
</div></div>
<!--聊天扣除-->
<?php }?>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->