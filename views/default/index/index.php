<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms
	$Id: order.class.php 2010-08-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?>
<?php echo $GLOBALS['setting']['site_title'];?>
</title>
<meta name="Keywords" content="<?php echo $GLOBALS['setting']['seo_keyword'];?>
">
<meta name="Description" content="<?php echo $GLOBALS['setting']['seo_description'];?>
">
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="//apps.bdimg.com/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>
<?php echo TPL_DIR;?>/css/index4.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></script>
<script language="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/imgscroll.js"></script>
<script language="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/global.js"></script>
</head>
<body>
<?php include(VIEWS_PATH."public/header.php");?>
<!--滚动banner开始-->
<?php if(is_file('data/adjs/headbanner.js')){?>
<script language='javascript' src='data/adjs/headbanner.js'></script>
<div id="container">
	<div id="content">
		<div id="slider" style="overflow:hidden;width:980px;height:170px;position:relative">
			<ul id="item">
				<script language="JavaScript">
			<!--
			if(headbanner_data)
			{
				for(i in headbanner_data){
					document.write('<li style="display: block;"><a target="_blank" href="'+headbanner_data[i].url+'"><img height="170" width="980" data="0" alt="" src="'+site_root+'/'+headbanner_data[i].img+'"></a></li>');
				}
			}
			//-->
			</script>
			</ul>
			<ul id="nav" style="position:absolute;bottom:5px;right:5px;">
				<script language="JavaScript">
			<!--
			if(headbanner_data)
			{
				for(j in headbanner_data){
					document.write('<li onmouseover="setTimeout(function(){Hongru.slider.pos('+j+')},300)">'+(j*1+1)+'</li>');
				}
			}
			//-->
			</script>
			</ul>
		</div>
	</div>
</div>
<script language="JavaScript">
<!--
if(headbanner_data)
{
	var objs = document.getElementById("nav").getElementsByTagName("li");
		var tv = new TransformView("slider", "item", 170, headbanner_data.length, {onStart : function(){ Each(objs, function(o, i){ o.className = tv.Index == i ? "on" : ""; }) },Pause:3000});
		tv.Start();
		Each(objs, function(o, i){
			o.onmouseover = function(){o.className = "on";tv.Auto = false;tv.Index = i;tv.Start();}
			o.onmouseout = function(){o.className = "";tv.Auto = true;tv.Start();}
		})
}
//-->
</script>
<?php }?>
<!--滚动banner结束-->
<div class="container980">
	<div class="index_left">
		<!--公告部分,我们一直在努力切换开始-->
		<script language="JavaScript">
        <!--
            function changetab(obj)
            {
                $('#bulletin').removeClass('tab_current');
                $('#bulletin').addClass('tab_other');
                $('#bulletinlist').hide();
                $('#updatelog').removeClass('tab_current');
                $('#updatelog').addClass('tab_other');
                $('#updateloglist').hide();
                $('#'+obj).addClass('tab_current');
                $('#'+obj+'list').show();
            }
        //-->
        </script>
		<div class="bulletin_box borD6 mar10">
			<dl>
				<dt>
				<div id="bulletin" class="tab_current black w1" onclick="changetab('bulletin');">
					网站公告
				</div>
				<div id="updatelog" class="tab_other black w2" onclick="changetab('updatelog');">
					我们一直在努力
				</div>
				<div class="more grayB4">
					<a href="<?php echo url('article');?>" target="_blank">更多</a>
				</div>
				</dt>
				<!--网站公告开始-->
				<dd id="bulletinlist">
				<ul class="bulletin">
					<?php if(isset($article[1])){foreach($article[1] as $t){?>
					<li>
					<span class="g_01">
					<a href="<?php echo url('article','view',array('id'=>$t['id']));?>" target="_blank" title="<?php echo $t['title'];?>">
					<?php echo $t['title'];?>
					</a>
					</span>
					<span class="g_02 grayB4" title="<?php echo date('Y-m-d',$t['updatetime']);?>"><?php echo date('m-d',$t['updatetime']);?>
					</span>
					</li>
					<?php }}?>
				</ul>
				</dd>
				<!--公告部分结束-->
				<!--网站更新日志开始-->
				<dd id="updateloglist" style="display:none;">
				<ul class="bulletin" style="height:160px; overflow:hidden;">
					<?php if(isset($article[4])){foreach($article[4] as $t){?>
					<li>
					<span class="blue" title="<?php echo date('Y-m-d',$t['updatetime']);?>">
					<?php echo date('m月d日',$t['updatetime']);?>
					</span>
					<span title="<?php echo $t['title'];?>"><?php echo $t['title'];?>
					</span>
					</li>
					<?php }}?>
				</ul>
				</dd>
				<!--网站更新日志结束-->
			</dl>
			<div class="public_corner public_topleft2">
			</div>
			<div class="public_corner public_topright2">
			</div>
			<div class="public_corner public_bottomleft">
			</div>
			<div class="public_corner public_bottomright">
			</div>
		</div>
		<!--公告部分,我们一直在努力切换结束-->
		<!--推荐商品切换开始-->
		<?php if(is_file('data/adjs/recommend.js')){?>
		<div class="recommend_good borD6 mar10">
			<script language='javascript' src='data/adjs/recommend.js'></script>
			<div id="slider2">
				<ul id="item2">
					<script language="JavaScript">
			<!--
			if(recommend_data)
			{
				for(i in recommend_data){
					document.write('<li><a href="'+recommend_data[i].url+'" target="_blank"><img src="'+site_root+'/'+recommend_data[i].img+'" alt="" data="0" height="166" width="286"/></a></li>');
				}
			}
			//-->
			</script>
				</ul>
				<ul id="nav2" style="position:absolute;bottom:5px;right:5px;">
					<script language="JavaScript">
				<!--
				if(recommend_data)
				{
					for(j in recommend_data){
						document.write('<li onmouseover="setTimeout(function(){Hongru.slider.pos('+j+')},300)">'+(j*1+1)+'</li>');
					}
				}
				//-->
				</script>
				</ul>
			</div>
			<script type="text/javascript">
		if(recommend_data)
		{
			var objs2 = document.getElementById("nav2").getElementsByTagName("li");
			var tv2 = new TransformView("slider2", "item2", 286, recommend_data.length, {
				onStart: function(){ Each(objs2, function(o, i){ o.className = tv2.Index == i ? "on" : ""; }) },Up: false,Pause:6000
			});
			tv2.Start();
			Each(objs2, function(o, i){
				o.onmouseover = function(){o.className = "on";tv2.Auto = false;tv2.Index = i;tv2.Start();}
				o.onmouseout = function(){o.className = "";tv2.Auto = true;tv2.Start();}
			})
		}
		</script>
			<div class="public_corner public_topleft">
			</div>
			<div class="public_corner public_topright">
			</div>
			<div class="public_corner public_bottomleft">
			</div>
			<div class="public_corner public_bottomright">
			</div>
		</div>
		<?php }?>
		<!--推荐商品切换结束-->
		<?php if($status){?>
		<!--看看拍友在忙什么开始-->
		<div class="left_box borD6 mar10">
			<dl>
				<dt class="black"><strong>看看拍友在忙什么</strong></dt>
				<dd class="tbpad6">
				<ul id="fresh" class="xiyou_do">
					<?php foreach($status as $v){?>
					<li id="fs_<?php echo $v['uid'];?>">
					<img width="13" height="13" class="index_avatar" src="<?php echo "/tools/avatar.php?uid=".$v['uid']."&size=small&type=virtual";?>">
					<div class="index_username">
						<span class="blue"><?php echo $v['username'];?>
						：</span><span class="grayB4"><?php echo $v['title'];?>
						</span>
					</div>
					</li>
					<?php }?>
				</ul>
				</dd>
			</dl>
			<div class="public_corner public_topleft2">
			</div>
			<div class="public_corner public_topright2">
			</div>
			<div class="public_corner public_bottomleft">
			</div>
			<div class="public_corner public_bottomright">
			</div>
		</div>
		<!--看看拍友在忙什么结束-->
		<?php }?>
		<!--最近成交开始-->
		<div class="left_box01 borFC mar10">
			<dl>
				<dt class="black">
				<strong class="left">最新成交</strong>
				<div class="publicmore grayB4">
					<a href="<?php echo url('index','history');?>" target="_blank">更多</a>
				</div>
				</dt>
				<dd class="bpad10">
				<?php foreach($goods_success as $val){$thumb=thumb($val['thumb']);?>
				<!--单个开始-->
				<div class="leftimg_txt">
					<div class="leftimg borEB">
						<a href="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>" title="<?php echo $val['goods_name'];?>
						"  target="_blank"><img src="<?php echo $thumb[0];?>" alt="<?php echo $val['goods_name'];?>
						" width="90" height="76" /></a>
					</div>
					<table>
					<tr>
						<td>
							<span class="leftname">
							<a href="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>" title="<?php echo $val['goods_name'];?>
							" target="_blank">
							<?php echo $val['goods_name'];?>
							</a>
							</span>
							<span class="leftprice">
							<span class="gray62">获胜者：</span><span class="blue"><?php echo $val['currentuser'];?>
							</span>
							</span>
							<span class="leftprice">
							<span class="gray62">成交价：</span><span class="yellow66 font14"><strong>￥<?php echo $val['nowprice'];?>
							</strong></span>
							</span>
						</td>
					</tr>
					</table>
				</div>
				<div class="clear">
				</div>
				<!--单个结束-->
				<?php }?>
				</dd>
			</dl>
			<div class="public_corner public_topleft6">
			</div>
			<div class="public_corner public_topright6">
			</div>
			<div class="public_corner public_bottomleft6">
			</div>
			<div class="public_corner public_bottomright6">
			</div>
		</div>
		<!--拍友晒单开始-->
		<div class="left_box borD6 mar10">
			<dl>
				<dt class="black">
				<strong class="left">拍友晒单</strong>
				<div class="publicmore grayB4">
					<a href="<?php echo url('index','show');?>" target="_blank">更多</a>
				</div>
				</dt>
				<dd class="bpad10">
				<?php if($GLOBALS['show']){foreach($GLOBALS['show'] as $v){?>
				<div class="leftimg_txt">
					<div class="leftimg borEB">
						<a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>
						" target="_blank"><img src="<?php echo $v['thumb'];?>" alt="<?php echo $v['goods_name'];?>
						" width="90" height="76" /></a>
					</div>
					<table>
					<tr>
						<td>
							<span class="leftname">
							<a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>" target="_blank">
							<?php echo $v['title'];?>
							</a></span>
							<span class="leftprice gray62">成交价：￥<?php echo isset($v['orderinfo']['goods_info'])?$v['orderinfo']['goods_info']['nowprice']:'0';?>
							</span>
							<img src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=small&type=virtual";?>" width="13" height="13" class="index_avatar"  alt=""/>
							<div class="index_username">
								<span class="blue">
								<?php echo $v['username'];?>
								</span>
							</div>
						</td>
					</tr>
					</table>
				</div>
				<div class="clear">
				</div>
				<?php }}?>
				</dd>
			</dl>
			<div class="public_corner public_topleft2">
			</div>
			<div class="public_corner public_topright2">
			</div>
			<div class="public_corner public_bottomleft">
			</div>
			<div class="public_corner public_bottomright">
			</div>
		</div>
		<!--拍友晒单结束-->
	</div>
	<div class="index_right">
		<!--竞拍步骤开始-->
		<div class="step mar10">
			<span class="a">如何出价赢得商品？</span>
			<span class="b">注册会员买<?php echo $GLOBALS['setting']['site_money_name'];?>
			</span>
			<span class="c">选择商品巧出价</span>
			<span class="d">赢得商品即支付</span>
			<span class="e">验货晒单得奖励</span>
		</div>
		<!--必得喜推荐开始-->
		<div class="left_box02 borFC mar10">
			<dl>
				<dt class="black"><strong>今日热拍</strong></dt>
				<dd>
				<div class="hotbg">
					<div class="hotdivleft">
						<?php if($hot_list){foreach($hot_list as $k=>
						$v){$thumb=thumb($v['thumb']);?>
						<div class="recommend <?php if($k==0 || $k==1 || $k==3 || $k==4){?>ret01<?php }?><?php if($k==2 || $k==5){?>ret02<?php }?>" id="tag_rcbiding_<?php echo $v['goods_id'];?>" style="position:relative;">
							<div class="name" title="<?php echo $v['goods_name'];?>">
								<a href="<?php echo url('index','goods',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>
								[第<?php echo $v['goods_id'];?>
								-<?php echo $k;?>
								期]" target="_blank">
								<?php echo $v['goods_name'];?>
								</a>
							</div>
							<div class="img borEB">
								<div class="div_new">
								</div>
								<a href="<?php echo url('index','goods',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>
								[第<?php echo $v['goods_id'];?>
								期]" target="_blank"><img src="<?php echo $thumb[0];?>" alt="<?php echo $v['goods_name'];?>
								[第<?php echo $v['goods_id'];?>
								期]" width="150" height="126" /></a>
							</div>
							<div class="countdown black">
								<span name="lefttime" id="lasttime_<?php echo $v['goods_id'];?>">--:--:--</span><strong id="microtime_<?php echo $v['goods_id'];?>" style="font-size: 16px; right: 24px; bottom: -3px; position: absolute;"></strong>
							</div>
							<div class="price">
								<span class="gray62">当前价：</span><span class="yellow66 font16"><strong>￥<span id="nowprice_<?php echo $v['goods_id'];?>" name="price">-.-</span></strong>
								</span><br/>
								<span class="gray62">出价人：</span><span class="blue" id="currentuser_<?php echo $v['goods_id'];?>" name="username">--</span>
							</div>
							<div class="button">
								<?php if($v['button_status'] == 'ok'){?>
								<!--1.可以竞拍开始-->
								<div class="recbidbutton rcbid" id="bid_<?php echo $v['goods_id'];?>" onmouseup="this.className='recbuttonover'"  onmousemove="this.className='recbuttonover'" onmouseout="this.className='recbidbutton'" onmousedown="this.className='recbidbuttondown';setInfo('<?php echo $v['goods_id'];?>');">
									<a style="cursor: pointer;" href=""></a>
								</div>
								<!--可以结束-->
								<?php } elseif($v['button_status'] == 'nologin'){?>
								<!--2.未登录开始-->
								<div id="bid_<?php echo $v['goods_id'];?>" class="recbidlogin rcbid">
									<a href="javascript:Login_Dialog();" style="cursor:pointer;"></a>
								</div>
								<!--未登录结束-->
								<?php } elseif($v['button_status'] == 'needsign'){?>
								<!--3.需要报名开始-->
								<div id="bid_<?php echo $v['goods_id'];?>" class="recbidapply rcbid">
									<a href="javascript:Login_Dialog();" style="cursor:pointer;"></a>
								</div>
								<!--需要报名结束-->
								<?php } elseif($v['button_status'] == 'moneyerror'){?>
								<!--4.不足开始-->
								<div id="bid_<?php echo $v['goods_id'];?>" class="recbidcharge rcbid">
									<a href="javascript:Charge_Dialog();" style="cursor:pointer;"></a>
								</div>
								<!--不足结束-->
								<?php } elseif($v['button_status'] == 'needconfirm'){?>
								<!--5.手机未验证开始-->
								<div id="bid_<?php echo $v['goods_id'];?>" class="recbidphone rcbid">
									<a href="<?php echo url('user','mobile');?>" style="cursor:pointer;"></a>
								</div>
								<!--手机未验证结束-->
								<?php }?>
							</div>
						</div>
						<?php }}?>
					</div>
				</div>
				</dd>
			</dl>
			<div class="public_corner public_topleft6">
			</div>
			<div class="public_corner public_topright6">
			</div>
			<div class="public_corner public_bottomleft6">
			</div>
			<div class="public_corner public_bottomright6">
			</div>
			<div class="clear">
			</div>
		</div>
		<!--必得喜推荐结束-->
		<!--正在竞拍开始-->
		<div class="left_box03 borD6 mar10">
			<!--竞拍提示，开始-->
			<!--竞拍提示，结束-->
			<dl>
				<dt class="black"><strong>正在竞拍</strong></dt>
				<dd>
				<!--单个开始-->
				<?php if($normal_list){foreach($normal_list as $v){$thumb=thumb($v['thumb']);?>
				<div class="rightimg_txt" id="tag_biding_<?php echo $v['goods_id'];?>
					" style="position:relative;">
					<div class="leftimg borEB">
						<div class="div_xibi01">
						</div>
						<a href="<?php echo url('index','goods',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>"><img src="<?php echo $thumb[0];?>" width="90" height="76" /></a>
					</div>
					<table>
					<tr>
						<td>
							<span class="leftname">
							<a href="<?php echo url('index','goods',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>
							" target="_blank">
							<strong><?php echo $v['goods_name'];?>
							</strong>
							</a>
							<span style="color:gray;">[第<?php echo $v['goods_id'];?>
							期]</span>
							</span>
							<span class="leftprice">
							<span class="gray62">市场价：
							<span class="line-through">￥<?php echo $v['marketprice'];?>
							</span>&nbsp;&nbsp;当前价：
							<span class="yellow66 font14"><strong>￥<span id="nowprice_<?php echo $v['goods_id'];?>" name="price">-.-</span>
							</strong>
							</span>
							</span>
							</span>
							<img name="uhead" id="avatar_<?php echo $v['goods_id'];?>" data-uid="<?php echo $v['currentuid'];?>" src="<?php echo UC_API.'/avatar.php?uid='.$v['currentuid'].'&size=small';?>" width="13" height="13" class="index_avatar" alt="" />
							<div class="index_username">
								<span id="currentuser_<?php echo $v['goods_id'];?>" class="blue" name="username">--</span>
							</div>
						</td>
					</tr>
					</table>
					<div class="time black">
						<span name="lefttime" id="lasttime_<?php echo $v['goods_id'];?>">--:--:--</span><strong id="microtime_<?php echo $v['goods_id'];?>" style="font-size: 16px; right: 24px; bottom: -3px; position: absolute;"></strong>
					</div>
					<div class="button buttonpad">
						<?php if($v['button_status'] == 'ok'){?>
						<!--1.可以竞拍开始-->
						<div class="bidbutton bid" id="bid_<?php echo $v['goods_id'];?>"  onmouseup="this.className='bidbuttonover'"  onmousemove="this.className='bidbuttonover'" onmouseout="this.className='bidbutton'" onmousedown="this.className='bidbuttondown';setInfo('<?php echo $v['goods_id'];?>');" >
							<a style="cursor: pointer;" href="#"></a>
						</div>
						<!--可以结束-->
						<?php } elseif($v['button_status'] == 'nologin'){?>
						<!--2.未登录开始-->
						<div id="bid_<?php echo $v['goods_id'];?>" class="bidlogin bid">
							<a href="javascript:Login_Dialog();" style="cursor:pointer;"></a>
						</div>
						<!--未登录结束-->
						<?php } elseif($v['button_status'] == 'needsign'){?>
						<!--3.需要报名开始-->
						<div id="bid_<?php echo $v['goods_id'];?>" class="bidapply bid">
							<a href="javascript:Login_Dialog();" style="cursor:pointer;"></a>
						</div>
						<!--需要报名结束-->
						<?php } elseif($v['button_status'] == 'moneyerror'){?>
						<!--4.不足开始-->
						<div id="bid_<?php echo $v['goods_id'];?>" class="bidcharge bid">
							<a href="javascript:Charge_Dialog();" style="cursor:pointer;"></a>
						</div>
						<!--不足结束-->
						<?php } elseif($v['button_status'] == 'needconfirm'){?>
						<!--5.手机未验证开始-->
						<div id="bid_<?php echo $v['goods_id'];?>" class="bidphone bid">
							<a href="<?php echo url('user','mobile');?>" style="cursor:pointer;"></a>
						</div>
						<!--手机未验证结束-->
						<?php }?>
					</div>
				</div>
				<?php }}?>
				<!--单个结束-->
				</dd>
			</dl>
			<div class="public_corner public_topleft2">
			</div>
			<div class="public_corner public_topright2">
			</div>
			<div class="public_corner public_bottomleft">
			</div>
			<div class="public_corner public_bottomright">
			</div>
		</div>
		<?php if($goods_list){?>
		<input type="hidden" id="ids" value="<?php echo implode('-',$ids);?>">
		<input type="hidden" id="oids" value="<?php echo implode('-',$ids);?>">
		<textarea style="display:none;" id="goods_list"><?php echo json_encode($goods_list);?>
		</textarea>
		<script language="JavaScript">
          <!--
          getInfo();
          window.onload=function(){checkcomplete();}
          //-->
        </script>
		<?php }?>
		<div class="index_right mar10">
			<!--聊天室开始-->
			<div class="left_box05 borD6">
				<dl>
					<dt class="black"><strong>聊天室</strong></dt>
					<dd>
					<div id="divChatList">
					</div>
					<script language="JavaScript">
        <!--
            getChat("0",'ajax',true);
        //-->
        </script>
					</dd>
				</dl>
				<div class="public_corner public_topleft2">
				</div>
				<div class="public_corner public_topright2">
				</div>
				<div class="public_corner public_bottomleft">
				</div>
				<div class="public_corner public_bottomright">
				</div>
			</div>
			<!--聊天室结束-->
			<!--帮助中心开始-->
			<div class="left_box04 borD6">
				<dl>
					<dt class="black"><strong>帮助中心</strong></dt>
					<dd>
					<span class="txtz">若您有任何问题，都可以通过免费电话我们联系。我们将竭诚为您服务。</span>
					<ul class="helpbutton">
						<li class="t1"><a href="<?php echo url('article','help',array('id'=>10));?>" target="_blank">竞拍流程</a></li>
						<li class="t t1"><a href="<?php echo url('article','help',array('id'=>9));?>" target="_blank">竞拍规则</a></li>
						<li class="t t1"><a href="<?php echo url('article','help',array('id'=>11));?>" target="_blank">竞拍秘籍</a></li>
						<li class="t2"><a href="<?php echo url('article','help',array('id'=>18));?>" target="_blank">服务条款</a></li>
						<li class="t t2"><a href="<?php echo url('article','help',array('id'=>19));?>" target="_blank">安全承诺</a></li>
						<li class="t t2"><a href="<?php echo url('index','guestbook');?>" target="_blank">意见反馈</a></li>
					</ul>
					<div class="helpbutton tmar12">
						<img border="0" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/index4/tell_03.gif" alt="服务热线" >
						<h1 style="font-size:30px;color:#000;font-family:黑体"><?php echo isset($GLOBALS['setting']['site_tel'])?$GLOBALS['setting']['site_tel']:'00-000-000';?>
						</h1>
					</div>
					<div class="helpbutton mar10">
						<span class="email"></span>
						<span class="emailtxt">服务邮箱：<?php echo $GLOBALS['setting']['site_email'];?>
						</span>
					</div>
				</dl>
				<div class="public_corner public_topleft2">
				</div>
				<div class="public_corner public_topright2">
				</div>
				<div class="public_corner public_bottomleft">
				</div>
				<div class="public_corner public_bottomright">
				</div>
			</div>
			<!--帮助中心结束-->
		</div>
	</div>
	<div class="clear">
	</div>
</div>
<!--合作伙伴开始-->
<div class="container980">
	<div class="left_box06 borD6 mar10">
		<dl>
			<dt class="black"><strong class="left">合作伙伴</strong>
			<div class="more grayB4">
				<a href="/links/index.html" target="_blank">更多</a>
			</div>
			</dt>
			<dd>
			<ul class="imglink">
				<?php if(isset($GLOBALS['link'][2])){foreach($GLOBALS['link'][2] as $l){?>
				<li><a href="<?php echo $l['url'];?>" target="_blank"><img src="<?php echo $l['thumb'];?>" alt="<?php echo $l['title'];?>" /></a></li>
				<?php }}?>
			</ul>
			<div class="imgtxt">
				<?php if(isset($GLOBALS['link'][1])){foreach($GLOBALS['link'][1] as $l){?>
				<li><a href="<?php echo $l['url'];?>" target="_blank"><?php echo $l['title'];?>
				</a></li>
				<?php }}?>
			</div>
			</dd>
		</dl>
		<div class="public_corner public_topleft2">
		</div>
		<div class="public_corner public_topright2">
		</div>
		<div class="public_corner public_bottomleft">
		</div>
		<div class="public_corner public_bottomright">
		</div>
	</div>
	<div class="clear">
	</div>
</div>
<!--底部--><?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->
<script language="JavaScript">
<!--
	show_alert();
//-->
</script>