<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
header("content-type:text/html;charset=utf-8");
?>
<SCRIPT LANGUAGE="JavaScript">
<!--
	$('#slider').slider({
		range: 'min',
		max:5000,
		value: 10,
		stop:function(event,ui){$('#xib_count').val(ui.value);}
	});
	function verify_submit()
	{
		if($('#xib_count').val()>10)
		{
			$.post(site_root+"/tools/setguess.php",{'gid':'<?php echo $guessinfo["gid"];?>','dotype':'<?php echo $dotype;?>','money':$('#xib_count').val()},
			function(data){
				eval('var dataobj='+data);
				parseGuessData(dataobj);
			});
		}
		else
		{
			$('#error_tips').html('投入值不能小于10');
		}
	}
	function parseGuessData(obj)
	{
		if(obj.datastatus=='success')
		{
			mymoney=parseInt($('#my'+obj.dotype).html()>0?$('#my'+obj.dotype).html():0);
			addmoney=obj.money>0?obj.money:0;
			$('#my'+obj.dotype).html(mymoney+addmoney*1);
			$('#totalb').html(obj.gl);
			$('#dratio').html(obj.ddt);
			$('#sratio').html(obj.sdt);
			if(obj.dp<85)
			{
				$('#dpercentage').html(obj.dp+'%');
			}
			else
			{
				$('#dpercentage').html('');
			}
			if(obj.sp<85)
			{
				$('#spercentage').html(obj.sp+'%');
			}
			else
			{
				$('#spercentage').html('');
			}
			$('#dw').css('width',obj.dp+'%');
			$('#sw').css('width',obj.sp+'%');
			$('#dialog').html('<div class="zf_fail_bid01 pad10"><div class="fail_icons"></div><div class="fail_con"><span class="font16"><strong>投入成功！</strong></span><br><input type="button" onclick="javascript:Dialog_close();" value="确 定" onmouseout="this.className=\'layer_button tmar16\'" onmousemove="this.className=\'layer_button_over tmar16\'" name="" class="layer_button tmar16"></div><div class="clear"></div></div>');

		}
		else if(obj.datastatus=='parmnull')
		{
			$('#error_tips').html('参数为空');
		}
		else if(obj.datastatus=='nouser')
		{
			$('#error_tips').html('用户不存在');
		}
		else if(obj.datastatus=='nologin')
		{
			$('#error_tips').html('用户未登录');
		}
		else if(obj.datastatus=='nomoney')
		{
			$('#error_tips').html('您的金币不足');
		}
		else if(obj.datastatus=='errormoney')
		{
			$('#error_tips').html('金币不能小于10');
		}
		else if(obj.datastatus=='productnull')
		{
			$('#error_tips').html('竞拍已结束');
		}
	}
//-->
</SCRIPT>
<div class="wyt_main">
	<div class="wyt_tips">提示：猜猜成交价小游戏同一期竞猜投入最高为5,000<?php echo $GLOBALS['setting']['site_money_name'];?>。</div>
<div class="wyt_txt">
	<strong class="font14">请选择您要<span class="g_color1">投<?php echo $dotype=='single'?'单':'双';?></span>的<?php echo $GLOBALS['setting']['site_money_name'];?>数量</strong><br>
	<span class="grayB4">我当前<?php echo $dotype=='single'?'单':'双';?>已投入：<?php echo $dotype=='single'?$guessinfo['single_count']:$guessinfo['double_count'];?><?php echo $GLOBALS['setting']['site_money_name'];?></span>
</div>
<div class="wyt_txt">
	<span class="<?php echo $dotype=='single'?'wyt_dan_bg':'wyt_shuang_bg';?>"></span>
	<input type="text" id="xib_count" class="wyt_button" value="0">
	<span class="wyt_ds_txt"><?php echo $GLOBALS['setting']['site_money_name'];?></span><span class="red" id="error_tips"></span>
</div>	


<div class="clear"></div>
<div id="slider"></div>


<div class="submit_cancel" id="submit_cancel">
<input type="button" onmouseout="this.className='layer_button left'" onmousemove="this.className='layer_button_over left'" value="确 定" class="layer_button left" onclick="javascript:verify_submit();" name="提交"><span onclick="javascript:Dialog_close();" class="yellow14 cancel_txt cursor">取 消</span>

</div>