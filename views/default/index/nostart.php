<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/bid.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/global.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">
  <div class="left t8"> <span class="left name"><?php echo $info['goods_name'];?> <span class="grayB4">[第<?php echo $info['goods_id'];?>期]</span></span>
        <div class="figure left"><span class="font14b white" title="出价未获胜者可返还<?php echo $info['backmoney'];?>%<?php echo $GLOBALS['setting']['site_money_name'];?>">返币<?php echo $info['backmoney'];?>%</span></div>

       	        <div class="clear"></div>
    <div class="nbid_bg"></div>
    <div class="clear"></div>
  </div>
 
</div>
<div class="clear"></div>
<div class="container980" style="position:relative; z-index:6">
  <div class="bid_prompt" id="bidtips" style="display:none;"></div>
  <div class="nleft">
  <SCRIPT LANGUAGE="JavaScript">
	<!--
		function showbigimg(obj)
		{
			$('#small_'+obj).addClass('imghover');
			$('#big_'+obj).show();
		}
		function hidebigimg(obj)
		{
			$('#small_'+obj).addClass('img');
			$('#big_'+obj).hide('');
		}
	//-->
	</SCRIPT>
    <!--商品展示图片开始-->

    <div class="nleft_goods" >
      <div class="goods_pic" style="position:absolute;z-index:10;">
			<?php foreach($thumb as $k=>$img){?>
			<div class="goods_sigle" style="cursor:pointer;"> 
			<img src="<?php echo $img;?>" id="small_<?php echo $k;?>"  class="img" onmousemove="showbigimg(<?php echo $k;?>);" onmouseout="hidebigimg(<?php echo $k;?>);" width="63"  height="53"/>
			<div class="goods_magni bottom_right"></div>
			 <div class="goods_bigpica" id="big_<?php echo $k;?>" style="display:none;"> <img src="<?php echo SITE_ROOT;?>/<?php echo $img;?>" width="289" height="241"/> </div>
			</div>
			<?php }?>       
      </div>
    </div>

    <!--商品展示图片结束-->
    <!--状态1:用户已经登录-->
    <div class="nleft_game borD6">
      <dl>
        <dt><span class="font16 lmar10"><strong>猜猜成交价</strong></span><span class="grayB4"> 玩游戏，赢<?php echo $GLOBALS['setting']['site_money_name'];?></span>
</dt>
        <dd>
        <?php include(VIEWS_PATH."public/guess.php");?>
        </dd>
      </dl>
      <div class="public_corner public_topleft"></div>

      <div class="public_corner public_topright"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <!--状态1结束-->
    <!--状态2-->
    <!--状态2结束-->
  </div>

    <!--未登录,开始-->
  <!--右侧文字开始-->
  <div class="ncenter">
    <div class="ngoods_more">
        <p class="a grayB4">市场价<br /><span class="yellow66">￥<?php echo $info['marketprice'];?></span></p>
    <p class="b"><a href="<?php echo url('index','descgoods',array('id'=>$info['goods_id']));?>" target="_blank">商品详情</a></p>
        </div>
	
    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="376">
      <tr>
        <td valign="bottom"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="middle" height="300" align="center" >
			  <br/>
			  <div class="end_bigtxt03"  id="nostartid"></div>
			  <SCRIPT LANGUAGE="JavaScript">
			  <!--
				normal_time("<?php echo date('m/d/Y H:i:s',$info['starttime']);?>","nostartid",'竞拍已经开始');
			  //-->
			  </SCRIPT>
			  <div class="end_bigtxt03" style="font-size:25px;">竞拍即将开始</div>
                <!--文字列表开始-->
                <span class="end_txt03 mar_t03"> 结束时间：<span class="gray62"><?php echo date('Y-m-d H:i:s',$info['lasttime']);?></span><br />

                <span class="left"><span class="left">当&nbsp;前&nbsp;价：</span><strong class="red price20 left">￥<?php echo $info['nowprice'];?></strong></span><span class="pricedan"></span><br />
                

                市&nbsp;场&nbsp;价：<span class="gray62 line-through">￥<?php echo $info['marketprice'];?></span><br />
                立即节省：<span class="red_b" title="节省"><?php echo $info['marketprice']-$info['nowprice'];?></span> </span>
                <!--文字列表结束-->
                </td>
            </tr>
          </table></td>
      </tr>
      <tr>

        <td valign="bottom" height="50"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="bottom">
			  <!--竞拍规则开始-->
                <div class="bid_ruleimg mar10"> </div>
                <div class="bidend_ruletxt"> <span class="blue"><a href="/help/faq.html#huosheng" target="_blank">怎样才能收到获胜商品呢？</a></span><br />
                  <span class="blue"><a href="/help/faq.html#shaidan" target="_blank">什么是晒单奖励？</a></span> </div>

                <!--竞拍规则结束--></td>
            </tr>
          </table></td>
      </tr>
    </table>
  </div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function changtag(id)
	{
		ida=['chat','log','rank'];
		for(i in ida)
		{
			$('#'+ida[i]).removeClass('tab_current');
			$('#'+ida[i]).addClass('tab_other');
			$('#bid_'+ida[i]).hide();
		}
		$('#'+id).removeClass('tab_other');
		$('#'+id).addClass('tab_current');
		$('#bid_'+id).show();
	}
//-->
</SCRIPT>
  <!--右侧切换部分开始-->
  <div class="nright borD6">
    <dl>
      <dt id="nv">
        <div class="tab_current black" id="chat" onclick="changtag('chat');" title="聊天室">聊天室</div>
        <div class="tab_other black" id="log" onclick="changtag('log');" title="历史出价">出价记录</div>
        <div class="tab_other black" id="rank" onclick="changtag('rank');" title="出价排行">出价排行</div>

      </dt>
        <dd id="bid_log" style="display:none;">
		<ul class="bid_history">
		
		</ul>
      </dd>
        <dd id="bid_rank" style="display:none;">
		<ul class="bid_top">
		
		</ul>
      </dd>
            <dd id="bid_chat" style="display:block;">
        <ul class="bid_chat" id="ifrm_chat">
          
        </ul><div class="clear"></div>
      </dd>	<div class="clear"></div>
    </dl>	<div class="clear"></div>
    <div class="public_corner public_topleft"></div>
    <div class="public_corner public_topright"></div>
    <div class="public_corner public_bottomleft"></div>

    <div class="public_corner public_bottomright"></div>
  </div>
  <div class="clear"></div>
</div>
<!--右侧切换部分结束-->
<div class="clear"></div>

<!--商品详细介绍开始-->
<div class="container980">
  <div class="mainbox borD6 mar10">
    <dl>
      <dt> <span class="box_nav">商品详细介绍</span> </dt>
      <dd><?php echo $info['content'];?></dd>
    </dl>
    <span class="right rpad10" id="pd_top"><button id="pd_top" width="59" height="20" border="0" class="cursor" />回到顶部</button></span>
    <div class="corner topleft3"></div>
    <div class="corner topright"></div>
    <div class="corner bottomleft"></div>
    <div class="corner bottomright"></div>
  </div>
  <div class="clear"></div>

</div>
<!--商品详细介绍结束-->
<!--常见问题开始-->
<div class="container980">
  <div class="mainbox borD6 mar10">
    <dl>
      <dt> <span class="box_nav">常见问题</span> <span class="morefaq blue"><a href="/help/" target="_blank">其它常见问题</a></span> </dt>
      <dd>

        <div class="faq">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>竞拍成功后怎么做？</strong></span> </div>
          <div class="txt"> 首先恭喜您竞拍成功，进入“我的账户中心”您会看到刚刚竞拍成功的商品，在线支付成功后，必得喜客服人员会在两个工作日内与您联系确认订单和发货。 </div>
        </div>
        <div class="faq lmar10">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>没有竞拍成功怎么办？</strong></span> </div>

          <div class="txt"> 没关系，您可以在商品竞拍结束后的7天内使用“保价购买”方式来获得该商品，即可不浪费您使用过的<?php echo $GLOBALS['setting']['site_money_name'];?>，购得该商品。 </div>
        </div>
        <div class="faq mar10">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>付款后多长时间可以收到货？</strong></span> </div>
          <div class="txt l96"> 必得喜是国内最专业的商品竞拍平台，为您提供公开、公平、公正的娱乐竞拍服务。我们的货源是与国内知名商城合作，包括京东、新蛋、卓越、当当和淘宝商城，所有商品均正品行货。一般情况在您支付成功后的第二个工作日内商品发出，经过3－5天到达。</div>

        </div>
        <div class="faq lmar10 mar10">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>收到货后晒单如何奖励？</strong></span> </div>
          <div class="txt"> 凡是竞拍成功的实物商品，都可以参与晒单，向其他拍友展示您的成果。晒单需要至少两张商品图片，一段自己的竞拍感言或心得，必得喜管理员会根据您的晒单内容质量给出1000－3000不等<?php echo $GLOBALS['setting']['site_money_name'];?>奖励。图片中若是有您的靓照，奖励翻倍。</div>
        </div>
      </dd>

    </dl>
    <div class="faqimg"> 
      <div class="clear"></div>
    </div>
    <span class="right rpad10"><button id="faq_top" width="59" height="20" border="0" class="cursor" />回到顶部</button></span>
    <div class="corner topleft3"></div>
    <div class="public_corner public_topright1"></div>
    <div class="public_corner public_bottomleft"></div>

    <div class="public_corner public_bottomright"></div>
  </div>
  <div class="clear"></div>
</div>

<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->