<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/bid.css" rel="stylesheet" type="text/css" />

<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<link href="<?php echo STATIC_ROOT;?>jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="<?php echo STATIC_ROOT;?>jquery/jquery.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?>jquery/jquery-ui.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/global.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">
  <div class="left t8"> <span class="left name"><?php echo $info['goods_name'];?> <span class="grayB4">[第<?php echo $info['goods_id'];?>期]</span></span>
        <div class="figure left"><span class="font14b white" title="出价未获胜者可返还<?php echo $info['backmoney'];?>%<?php echo $GLOBALS['setting']['site_money_name'];?>">返币<?php echo $info['backmoney'];?>%</span></div>

       	        <div class="clear"></div>
    <div class="nbid_bg"></div>
    <div class="clear"></div>
  </div>
 
</div>
<div class="clear"></div>
<div class="container980" style="position:relative; z-index:6">
  <div class="bid_prompt" id="bidtips" style="display:none;"></div>
  <div class="nleft">
  <SCRIPT LANGUAGE="JavaScript">
	<!--
		function showbigimg(obj)
		{
			$('#small_'+obj).addClass('imghover');
			$('#big_'+obj).show();
		}
		function hidebigimg(obj)
		{
			$('#small_'+obj).addClass('img');
			$('#big_'+obj).hide('');
		}
	//-->
	</SCRIPT>
    <!--商品展示图片开始-->

    <div class="nleft_goods" >
      <div class="goods_pic" style="position:absolute;z-index:10;">
			<?php foreach($thumb as $k=>$img){?>
			<div class="goods_sigle" style="cursor:pointer;"> 
			<img src="<?php echo $img;?>" id="small_<?php echo $k;?>"  class="img" onmousemove="showbigimg(<?php echo $k;?>);" onmouseout="hidebigimg(<?php echo $k;?>);" width="63"  height="53"/>
			<div class="goods_magni bottom_right"></div>
			 <div class="goods_bigpica" id="big_<?php echo $k;?>" style="display:none;"> <img src="<?php echo $img;?>" width="289" height="241"/> </div>
			</div>
			<?php }?>       
      </div>
    </div>

    <!--商品展示图片结束-->
    <!--状态1:用户已经登录-->
    <div class="nleft_game borD6">
      <dl>
        <dt><span class="font16 lmar10"><strong>猜猜成交价</strong></span><span class="grayB4"> 玩游戏，赢<?php echo $GLOBALS['setting']['site_money_name'];?></span>
                  </dt>
        <dd>

         <?php include(VIEWS_PATH.'public/guess.php');?>
        </dd>
      </dl>
      <div class="public_corner public_topleft"></div>

      <div class="public_corner public_topright"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <!--状态1结束-->
    <!--状态2-->
    <!--状态2结束-->
  </div>
          <!--竞拍结束,开始-->

    <!--未登录,开始-->
  <!--右侧文字开始-->
  <div class="ncenter">
    <div class="ngoods_more">
        <p class="a grayB4">市场价<br /><span class="yellow66">￥<?php echo $info['marketprice'];?></span></p>
        <p class="b"><a href="<?php echo url('index','descgoods',array('id'=>$info['goods_id']));?>" target="_blank">商品详情</a></p>
    </div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="376">
      <tr>
        <td valign="bottom"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="middle" height="300" align="center" ><div class="end_bigtxt03" >竞拍已结束</div>
                <!--文字列表开始-->
                <span class="end_txt03 mar_t03"> 结束时间：<span class="gray62"><?php echo date('Y-m-d H:i:s',$info['lasttime']);?></span><br />

                <span class="left"><span class="left">成&nbsp;交&nbsp;价：</span><strong class="red price20 left">￥<?php echo $info['nowprice'];?></strong></span><span class="<?php substr($info['nowprice'],-1,1)%2?'pricedan':'priceshuang';?>"></span><br />
                <span class="left"><span class="left">获&nbsp;胜&nbsp;者：</span><img width="13" height="13" class="bid_avatar" src="<?php echo UC_API."/avatar.php?uid=".$info['currentuid']."&size=middle&type=virtual";?>"><span class="blue"><?php echo $info['currentuser'];?></span>（使用<span class="red"><?php echo $currentcount['price'];?></span><?php echo $GLOBALS['setting']['site_money_name'];?>）</span><br />

                市&nbsp;场&nbsp;价：<span class="gray62 line-through">￥<?php echo $info['marketprice'];?></span><br />
                立即节省：<span class="red_b" title="节省"><?php echo $info['marketprice']-$info['nowprice'];?></span> </span>
                <!--文字列表结束-->
                </td>
            </tr>
          </table></td>
      </tr>
      <tr>

        <td valign="bottom" height="50"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="bottom"><!--竞拍规则开始-->
                <div class="bid_ruleimg mar10"> </div>
                <div class="bidend_ruletxt"> <span class="blue"><a href="/help/faq.html#huosheng" target="_blank">怎样才能收到获胜商品呢？</a></span><br />
                  <span class="blue"><a href="/help/faq.html#shaidan" target="_blank">什么是晒单奖励？</a></span> </div>

                <!--竞拍规则结束--></td>
            </tr>
          </table></td>
      </tr>
    </table>
  </div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function changtag(id)
	{
		ida=['chat','log','rank'];
		for(i in ida)
		{
			$('#'+ida[i]).removeClass('tab_current');
			$('#'+ida[i]).addClass('tab_other');
			$('#bid_'+ida[i]).hide();
		}
		$('#'+id).removeClass('tab_other');
		$('#'+id).addClass('tab_current');
		$('#bid_'+id).show();
	}
//-->
</SCRIPT>
  <!--右侧切换部分开始-->
  <div class="nright borD6">
    <dl>
      <dt id="nv">
        <div class="tab_current black" id="chat" onclick="changtag('chat');" title="聊天室">聊天室</div>
        <div class="tab_other black" id="log" onclick="changtag('log');" title="历史出价">出价记录</div>
        <div class="tab_other black" id="rank" onclick="changtag('rank');" title="出价排行">出价排行</div>

      </dt>
        <dd id="bid_log" style="display:none;">
		<ul class="bid_history">
		<?php foreach($bidlog_list as $v){?>
		<li> <span class="a"></span> <span class="uesrname" id="luname_1" title="<?php echo $v['username'];?>"><?php echo $v['username'];?></span> <span class="ip grayB4" id="lip_1"><?php echo parseIp($v['ip']);?></span> <span id="lmobile_1" class="lmobile grayB4" title="<?php echo $v['address'];?>"><?php echo $v['address'];?></span> <span class="price red"> ￥<span id="lprice_1"><?php echo $v['price'];?></span> </span> </li>
		<?php }?>
		</ul>
        <div class="more_history blue"><a target="_blank" href="<?php echo url('user','buylog',array('gid'=>$info['goods_id']));?>">>>查看更多出价记录</a></div>
      </dd>
        <dd id="bid_rank" style="display:none;">
		<ul class="bid_top">
		<?php foreach($bidlog_count as $k=>$v){
			$a=array('a','b','c','d');
			$s=$k<4?$a[$k]:'d';
		?>
		<li> <span class="<?php echo $s;?>"><?php echo ($k+1);?></span> <span class="uesrname" id="runame_<?php echo $v['id'];?>" title="<?php echo $v['username'];?>"><?php echo $v['username'];?></span> <span class="ip grayB4" id="rmobile_<?php echo $v['id'];?>"><?php echo parseIP($v['ip']);?></span> <span class="price grayB4"> <span id="rcost_1" title="出价<?php echo $v['count'];?>次"><?php echo $v['count'];?></span> </span> </li>
		<?php }?>
		</ul>
        <div class="more_history blue"><a target="_blank" href="<?php echo url('user','buylog',array('gid'=>$info['goods_id']));?>">>>查看更多出价记录</a></div>
      </dd>
            <dd id="bid_chat" style="display:block;">
        <ul class="bid_chat" id="ifrm_chat">
          <div id="divChatList" name="chat_list">
</div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	getChat("<?php echo $info['goods_id'];?>",'ajax',false);
//-->
</SCRIPT>
<?php if(time()-$info['lasttime']<1800){?>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function doface(obj)
	{
		setChat('<?php echo $info['goods_id'];?>',$(obj).attr('title'),'<?php echo $info['goods_name'];?>',$(obj).attr('data'));
		$('#face').hide();
	}
	function checklen(obj)
	{
		var len=$(obj).val().length;
		if(len<30)
		{
			$('#msgerror').html('目前还可以输入'+(30-len-1)+'个字');
		}
	}
//-->
</SCRIPT>
<div id="bid_message" class="bid_message">
<div class="wide">
  <div class="l">发表留言<span name="spMsg" id="msgerror" class="red lmar6"></span></div>
  <span class="left greentime" id="spChatLeftTime"></span>
</div>
	<span class="left">
	<input type="text" name="txt_content" maxlength="30" <?php if(!$GLOBALS['session']->get('uid')){?>class="notxtContent txtContent" onclick="Login_Dialog();" value="您还未登陆，点此登陆"<?php } else{?>onkeydown="return checklen(this);" class="txtContent"<?php }?> id="txtContent">
	<div title="表情" style="cursor: pointer;" class="emotionsbutton" onclick="$('#face').toggle();"></div>
	<input class="sendbutton" type="button" id="btnSubmit" value=""  name="chat_submit"  onmousemove="this.className='sendbutton_over'" data="0" onmouseout="this.className='sendbutton'" onclick="<?php if($GLOBALS['setting']['site_chatmoney']){?>Warn_Dialog(this);<?php }?>setChat('<?php echo $info['goods_id'];?>',$('#txtContent').val(),'<?php echo $info['goods_name'];?>');$('#txtContent').val('');$(this).attr('data','1');"/>
</span>
</div>
<?php } else{?>
<div class="bid_message" id="bid_message">
<div class="wide">
<div class="l">发表留言<span class="red lmar6" name="spMsg"></span></div>
<span id="spChatLeftTime" class="left greentime"></span></div>
<span class="left"> <span id="txtContent" class="notxtContent" >竞拍结束已过半小时，不能聊天了哦！</span> </span>

</div>
<?php }?>
        </ul>	<div class="clear"></div>
      </dd>	<div class="clear"></div>
    </dl>	<div class="clear"></div>
    <div class="public_corner public_topleft"></div>
    <div class="public_corner public_topright"></div>
    <div class="public_corner public_bottomleft"></div>

    <div class="public_corner public_bottomright"></div>
  </div>
  <div class="clear"></div>
</div>
<!--右侧切换部分结束-->
<div class="clear"></div>
<?php if($showlist[1]){?>
<!--拍友晒单开始-->
<div class="container980">

  <div class="mainbox borD6 mar10">
    <dl>
      <dt> <span class="box_nav">拍友晒单</span> </dt>
      <dd>
        <?php foreach($showlist[1] as $v){?>
        <!--标题开始-->
        <div class="showsheet_title"> <span class="title black_b">标题</span> <span class="view black_b">回复/浏览</span> <span class="username black_b">拍友</span> <span class="time black_b">时间</span> </div>

        <!--标题结束-->
        <div class="showsheet"> <span class="title">
          <div class="ss_height"> <span class="aa"></span> <span class="blue"> <a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" target="_blank" ><?php echo $v['title'];?></a></span> </div>
          </span> <span class="view"><?php echo $v['comments'];?>/<?php echo $v['hits'];?></span> <span class="username blue"><?php echo $v['username'];?></span> <span class="time"><?php echo date('Y-m-d',$v['updatetime']);?></span>

          <ul class="showsheetimg">
            <?php 
            $pic=getUploadPic($v['content'],3);
            foreach($pic as $key=>$val){
            ?>
            <li> <a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" target="_blank"><img src="<?php echo $val;?>" width="93" height="76"/></a> </li>
            <?php }?>
          </ul>
        </div>
        <?php }?>
      </dd>
    </dl>
    <span class="right rpad10 bpad8"> <a href="<?php echo url('index','show');?>" target="_blank"> 查看更多 </a> </span>

    <div class="corner topleft3"></div>
    <div class="public_corner public_topright1"></div>
    <div class="public_corner public_bottomleft"></div>
    <div class="public_corner public_bottomright"></div>
  </div>
  <div class="clear"></div>
</div>
<!--拍友晒单结束-->
<?php }?>
<!--参与竞拍会员开始-->
<div class="container980">
  <div class="mainbox borD6 mar10">

    <dl>
      <dt> <span class="box_nav">参与竞拍会员</span> </dt>
    </dl>
    <div class="biduser pad10" id="ranks">
	<?php foreach($bidlog_list as $v){?>

	<!--本期其它参与者，开始-->
	<span class="biduser_a" style=""> <span class="biduser_avatar" title="godhand"> <img src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=middle&type=virtual";?>" width="48" height="48" alt="" /> </span> <span class="blue txtcenter biduser_a"><?php echo $v['username'];?></span> </span>

	<!--本期其它参与者，结束-->
	<?php }?>
	</div>
    <span class="right rpad10">
    <button id="rank_view_all" alt="查看全部" width="59" height="20" border="0" class="cursor" >查看全部</button>
    <button id="rank_hide_all" alt="收起全部" width="59" height="20" border="0" style="cursor:pointer;display:none;" />收起全部</button>
    </span>
    <div class="corner topleft3"></div>
    <div class="corner topright"></div>
    <div class="corner bottomleft"></div>
    <div class="corner bottomright"></div>
  </div>
  <div class="clear"></div>

</div>
<!--参与竞拍会员结束-->
<!--商品详细介绍开始-->
<div class="container980">
  <div class="mainbox borD6 mar10">
    <dl>
      <dt> <span class="box_nav">商品详细介绍</span> </dt>
      <dd><?php echo $details['content'];?></dd>
    </dl>
    <span class="right rpad10"><button id="pd_top" width="59" height="20" border="0" class="cursor" />回到顶部</button></span>
    <div class="corner topleft3"></div>
    <div class="corner topright"></div>
    <div class="corner bottomleft"></div>
    <div class="corner bottomright"></div>
  </div>
  <div class="clear"></div>

</div>
<!--商品详细介绍结束-->
<!--常见问题开始-->
<div class="container980">
  <div class="mainbox borD6 mar10">
    <dl>
      <dt> <span class="box_nav">常见问题</span> <span class="morefaq blue"><a href="/help/" target="_blank">其它常见问题</a></span> </dt>
      <dd>

        <div class="faq">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>竞拍成功后怎么做？</strong></span> </div>
          <div class="txt"> 首先恭喜您竞拍成功，进入“我的账户中心”您会看到刚刚竞拍成功的商品，在线支付成功后，必得喜客服人员会在两个工作日内与您联系确认订单和发货。 </div>
        </div>
        <div class="faq lmar10">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>没有竞拍成功怎么办？</strong></span> </div>

          <div class="txt"> 没关系，您可以在商品竞拍结束后的7天内使用“保价购买”方式来获得该商品，即可不浪费您使用过的<?php echo $GLOBALS['setting']['site_money_name'];?>，购得该商品。 </div>
        </div>
        <div class="faq mar10">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>付款后多长时间可以收到货？</strong></span> </div>
          <div class="txt l96"> 必得喜是国内最专业的商品竞拍平台，为您提供公开、公平、公正的娱乐竞拍服务。我们的货源是与国内知名商城合作，包括京东、新蛋、卓越、当当和淘宝商城，所有商品均正品行货。一般情况在您支付成功后的第二个工作日内商品发出，经过3－5天到达。</div>

        </div>
        <div class="faq lmar10 mar10">
          <div class="title"> <span class="a"></span> <span class="left blue"><strong>收到货后晒单如何奖励？</strong></span> </div>
          <div class="txt"> 凡是竞拍成功的实物商品，都可以参与晒单，向其他拍友展示您的成果。晒单需要至少两张商品图片，一段自己的竞拍感言或心得，必得喜管理员会根据您的晒单内容质量给出1000－3000不等<?php echo $GLOBALS['setting']['site_money_name'];?>奖励。图片中若是有您的靓照，奖励翻倍。</div>
        </div>
      </dd>

    </dl>
    <span class="right rpad10"><button id="faq_top" width="59" height="20" border="0" class="cursor" />回到顶部</button></span>
    <div class="corner topleft3"></div>
    <div class="public_corner public_topright1"></div>
    <div class="public_corner public_bottomleft"></div>

    <div class="public_corner public_bottomright"></div>
  </div>
  <div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->