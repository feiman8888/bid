<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/showinfo.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<script type="text/javascript">
var gURL = "/bid/g";
var BID = 0;
BID = 8465;
$(document).ready(function(){
	var $txtTitle 	= $("#txtTitle");
	var $txtContent = $("#txtContent");
	var $btnSubmit 	= $("#btnSubmit");
	var $spMsg 		= $("#Msg");
	
	//提交
	$btnSubmit.click(function(){
		var title = $.trim($txtTitle.val());
		if(title.length < 4 || title.length > 50){
			$spMsg.html("请填写标题，长度在4-50字符之间");
			$txtTitle.focus();
			return false;
		}
		
		var content = $.trim($txtContent.val());
		if(content.length < 5 || content.length > 1000){
			$("#Msg2").html("请填写内容，长度在5-1000字符之间");
			$txtContent.focus();
			return false;
		}
		if($.trim($('#checkcode').val()).length<1)
		{
			$("#Msg3").html("请填写验证码");
			$('#checkcode').focus();
			return false;
		}
		$spMsg.html("");

		$("form").submit();
	});
});

	//引用
	function Quote(id){
		var quoteContent = $("#divContent_" + id).html();
		var oldContent = '';
		//$("#txtContent").val();
		//var lou = $("#ftLou_" + id).html();
		var userName = $("#ftUser_" + id).html();

		//<br />替换成换行
		var rgExp = new RegExp("<br>","g");
    		quoteContent = quoteContent.replace(rgExp,"")
		var quote = "引用" + userName + ":\r\n" + quoteContent + "\r\n";
		$("#txtContent").val(oldContent + quote).focus();
	}
	//回复
	function Reply(id){
		var oldContent = '';
		//$("#txtContent").val();
		//var lou = $("#ftLou_" + id).html();
		var userName = $("#ftUser_" + id).html();
		var rpy = "回复" + "" + userName + ":" ;
		$("#txtContent").val(oldContent + rpy).focus();
	}
	
</script>

<?php include(VIEWS_PATH."public/header.php");?>
  <!--中间内容开始-->
  <div id="main">
    <div class="blank10"></div>
    <div class="s_title blue"><a href="/">首页 </a> > <a href="<?php echo url('index','show');?>">拍友晒单</a> > <A HREF="<?php echo $showinfo['url'];?>" target="_blank"><?php echo $showinfo['order_info']['goods_name'];?></A> > 查看晒单</div>

    <div class="blank10"></div>
    <!--左边内容-->
    <div class="m_left">
      <!-- 商品信息开始-->
      <div class="left_box2">
        <dl>
          <dt class="goods_bt"><span class="yellow66">【第<?php echo $showinfo['order_info']['goods_id'];?>期】</span><A HREF="<?php echo $showinfo['url'];?>" target="_blank"><?php echo $showinfo['order_info']['goods_name'];?></A></dt>
          <dd class="bpad10">

            <div class="goods_pic"> <a href="<?php echo $showinfo['url'];?>"><img src="<?php echo $thumb[0];?>" alt="<?php echo $showinfo['order_info']['goods_name'];?>" width="107" height="95" class="imgPIC" /></a> <span class="b"> 市 场 价：<span class="line-through">￥<?php echo $marketprice;?></span><br />
              成 交 价：<span class="yellow66 font16"><strong>￥<?php echo $nowprice;?></strong></span><span class="yellow66" style="padding-left:10px;">节省<?php echo $marketprice-$nowprice;?></span><br />
             <span class="ie6touxiang"> 获 胜 者：<img height="13" width="13" class="sl_avatar" src="<?php echo UC_API."/avatar.php?uid=".$showinfo['uid']."&size=small&type=virtual";?>" id="uhead_312"><span class="blue"><?php echo $currentuser;?></span>（使用<span class="yellow66 fontb"><?php echo $money['money'];?></span><?php echo $GLOBALS['setting']['site_money_name'];?>）</span>

            结束时间：<?php echo date('Y-m-d H:i:s',$showinfo['order_info']['goods_info']['lasttime']);?> </span> </div></dd>

        </dl>
        <div class="public_corner public_topleft"></div>
        <div class="public_corner public_topright"></div>
        <div class="public_corner public_bottomleft"></div>
        <div class="public_corner public_bottomright"></div>
      </div>
      <!-- 商品信息结束-->
      <!-- 内容部分开始-->
      <div class="left_box3 mar10">

        <dl>
          <dt>
            <div class="cor2_title"> 
			<span class="avatar_middle"><img src="<?php echo UC_API."/avatar.php?uid=".$showinfo['uid']."&size=middle&type=virtual";?>" width="66" height="66" /></span> 
			<div class="c"> 
			<div class="titlec font16">
			<strong><?php echo $showinfo['title'];?></strong>
			</div>
			<div class="titlex">
              <span class="blue left" id="ftUser_1323"><?php echo $showinfo['username'];?></span> <span class="grayB4 left" style="padding-left:5px;">秀于：<?php echo date('Y-m-d H:i:s',$showinfo['updatetime']);?></span><span class="right yellow66 jiang">奖励<?php echo $showinfo['givemoney'];?><?php echo $GLOBALS['setting']['site_money_name'];?></span>

                            </div> 
			  </div>
			  </div>
          </dt>
          <dd>
            <div class="cor2_connet"> 
			<span id="divContent_1323"><?php echo $showinfo['content'];?></span>
			</div>
                        <div class="number"> <span class="left">回复 <span class="yellow66"><?php echo $showinfo['comments'];?></span>　浏览 <span class="yellow66"><?php echo $showinfo['hits'];?></span></span>

              <div class="right"> 
                <button class="button1" type="button" onclick = "javascript:Reply(1323);">回复</button>
                <button class="button2" type="button" onclick="javascript:Quote(1323);" >引用</button>
                <button class="button3" type="button" onclick="javascript:window.scroll(0,0);" >TOP</button>
              </div>
              <div class="blank10"></div>
            </div>
          </dd>
        </dl>

        <div class="public_corner public_topleft"></div>
        <div class="public_corner public_topright"></div>
        <div class="public_corner public_bottomleft"></div>
        <div class="public_corner public_bottomright"></div>
      </div>
      <!-- 内复部分结束-->
      <!-- 快速回复开始-->
      <div class="s_login"> <span class="font14 fontb left" style="margin:10px 10px 0 10px;">拍友回复</span> <span class="left">

        <input class="login_put" name="" type="button" value="发表回复" onclick="javascript:location.href='#names';"/>
        </span>
                <?php if(!$GLOBALS['session']->get('uid')){?><span class="left" style="line-height:35px;">请先 <a href="javascript:Login_Dialog();"><span class="fontb yellow66">登录</span></a> 再发表</span><?php }?>
                <span class="right blue" style="line-height:35px; padding-right:10px;">【共<?php echo $showinfo['comments'];?>条回复】</span>
        <div class="public_corner public_topleft"></div>
        <div class="public_corner public_topright"></div>

        <div class="public_corner public_bottomleft"></div>
        <div class="public_corner public_bottomright"></div>
      </div>
      <!--快速回复结束-->
      <!-- 回复部分开始-->
	  <div id="commentlist">
	  <?php if($comment){?>
	  <?php foreach($comment as $key=>$val){?>
	  <div class="people">
        <div class="showsheetinfo"> <span class="avatar_small"><img src="<?php echo UC_API."/avatar.php?uid=".$val['uid']."&size=small&type=virtual";?>"></span>
          <div class="tit"> 
		  <span class="tit_txt"><strong class="font16"><?php echo $val['title'];?></strong></span> 
		  </div>
          <div class="t"> 
			  <span id="ftUser_<?php echo $val['id'];?>" class="blue left"><?php echo $val['username'];?></span> 
			  <span style="padding-left: 5px;" class="grayB4 left">发表于：<?php echo date('Y-m-d H:i:s',$val['updatetime']);?></span> 
		  </div> 
		  <div class="floor"> 
			  <span class="blue"><a href="javascript:Reply('<?php echo $val['id'];?>');">回复</a></span><span class="blue"><a href="javascript:Quote('<?php echo $val['id'];?>');">引用</a></span><span onclick="javascript:window.scroll(0,0);" style="cursor: pointer;" class="blue">TOP</span> 
		  </div> 
		  </div>
        <div class="sub">
          <p><span id="divContent_<?php echo $val['id'];?>"><?php echo $val['content'];?></span></p>
        </div>
      </div>
	  <?php }}?>
	  </div>
       <div class="pages">
        <table align="center" class="mar10"  >
          <tbody>
            <tr>

              <td>
			  <div class="list_page"><?php echo $pageinfo;?></div></td>
            </tr>
          </tbody>
        </table>

      </div>
      <!-- 回复部分结束-->
      <!-- 回复框开始-->
	  <?php if($GLOBALS['session']->get('uid')){?>
	<FORM METHOD="POST" ACTION="<?php echo SITE_ROOT;?>/index.php?con=index&act=comment" data-type="ajax">
	<INPUT TYPE="hidden" NAME="commit" value="1"><INPUT TYPE="hidden" NAME="sid" value="<?php echo $_GET['id'];?>">
      <div class="left_box mar10">
        <dl>
          <dt class="black"><strong class="left">发表回复</strong></dt>
          <dd>
            <div class="comment"> <a name="names"></a>
              <div class="c1"><span class="left">主题：</span>
                <input type="text" id="txtTitle" value="回复:<?php echo $showinfo['title'];?>" name="title" class="k_text1">
                
                <span class="sl_Msg" id="Msg"></span></div>
              <div class="c1"><span class="left">内容：</span>
                <textarea id="txtContent" name="content" class="k_text2"></textarea>
              </div>
              <span class="s1_Msg" id="Msg2"></span>
			  <SCRIPT LANGUAGE="JavaScript">
			  <!--
				function changecode(obj)
				  {
					obj.src=site_root+"/tools/showimgcode.php?rand="+Math.random();
				  }
			  //-->
			  </SCRIPT>
			   <div class="c1"><span class="left"> 验证码：</span>
                <input TYPE="text" size="8" style="width:80px;" name="checkcode" id="checkcode" class="k_text1"/>
                <img id="txt_code" src="" onclick="getCode();"/></div>
				<span class="sl_Msg3" id="Msg3"></span>
				</div>
                        <input type="button" id="btn_submit" value="确认发表">
              </dd>
        </dl>
        <div class="public_corner public_topleft"></div>
        <div class="public_corner public_topright"></div>
        <div class="public_corner public_bottomleft"></div>
        <div class="public_corner public_bottomright"></div>
      </div>
	  </form>
	  <?php }?>
      <div class="blank10"></div>
    </div>
    <!--回复框结束-->
    <!--右边内容开始-->
    <div class="m_right">
      <!--往期回顾开始-->

      <div class="right_box borD6">
        <dl>
          <dt class="black"> <strong class="left">往期竞拍回顾</strong> </dt>
          <dd>
			<ul class="ul_list">
			<?php if($showlist[0]){foreach($showlist[0] as $oldgoods){?>

			<li><img src="<?php echo UC_API."/avatar.php?uid=".$oldgoods['currentuid']."&size=middle&type=virtual";?>" width="13" height="13" /><span class="blue"><?php echo $oldgoods['currentuser'];?></span> 以<span class="yellow66">￥<?php echo $oldgoods['nowprice'];?></span> 赢得商品 <span class="grayB4">[第<?php echo $oldgoods['goods_id'];?>期]</span></li>
			<?php }}?>
			</ul>
          </dd>
        </dl>
        <div class="public_corner public_topleft"></div>

        <div class="public_corner public_topright"></div>
        <div class="public_corner public_bottomleft"></div>
        <div class="public_corner public_bottomright"></div>
      </div>
      <!--往期回顾结束-->
      <!--拍友晒单开始-->
      <div class="right_box borD6 mar10">
        <dl>
			<dt class="black">

			  <strong class="left">拍友晒单</strong>
			  <div class="publicmore grayB4"><a href="<?php echo url('index','show');?>" target="_blank">更多</a></div>
			</dt>
			<dd class="bpad10">
			<?php if($GLOBALS['show']){foreach($GLOBALS['show'] as $v){?>
			<div class="leftimg_txt">
			<div class="leftimg borEB"><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>"><img src="<?php echo $v['thumb'];?>" width="90"  height="76" /></a></div>
			<table class="txt right">
			<tr>

			<td><span class="leftname"><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>"><?php echo $v['title'];?></a> </span> <span class="leftprice gray62">成交价：￥<?php echo $v['orderinfo']['goods_info']['nowprice'];?></span>
			<div class="winuser"> <img  class="sl_avatar"  src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=small&type=virtual";?>" width="13" height="13"/> <span class="blue"><?php echo $v['username'];?></span> </div></td>
			</tr>
			</table>

			</div>
			<?php }}?>
			</dd>
        </dl>
        <div class="public_corner public_topleft"></div>
        <div class="public_corner public_topright"></div>
        <div class="public_corner public_bottomleft"></div>
        <div class="public_corner public_bottomright"></div>
      </div>

      <!--拍友晒单结束-->
      <!--成功的竞拍开始-->
      <div class="right_box borD6 mar10">
        <dl>
          <dt class="black"> 
			  <strong class="left">成功的竞拍</strong>
			  <div class="publicmore grayB4"><a href="/bidend" target="_blank">更多</a></div> 
		  </dt>
          <dd class="bpad10">
		  <?php foreach($goods_success as $val){$thumb=thumb($val['thumb']);?>
              <div class="leftimg_txt">
              <div class="leftimg borEB"><a href="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>"><img src="<?php echo $thumb[0];?>" width="90"  height="76" /></a></div>
              <div class="txt"> <span class="leftname40"> <a href="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>"> <strong><?php echo $val['goods_name'];?></strong> </a> <span style="color:gray;">[第<?php echo $val['goods_id'];?>期]</span> </span> <span class="winuser">获胜者：<img height="13" width="13" class="sl_avatar" src="<?php echo UC_API."/avatar.php?uid=".$GLOBALS['session']->get('uid')."&size=small&type=virtual";?>"><span class="blue"><?php echo $val['currentuser'];?></span></span> <span class="leftprice"> 成交价：<span class="yellow66 fontb font14">￥<?php echo $val['nowprice'];?></span> </span> </div>
			
            </div>
			<?php }?>
             </dd>
        </dl>
        <div class="public_corner public_topleft"></div>
        <div class="public_corner public_topright"></div>
        <div class="public_corner public_bottomleft"></div>
        <div class="public_corner public_bottomright"></div>
      </div>
      <!--成功的竞拍结束-->

      <!--常见问题拍开始-->
      <div class="right_box borD6 mar10">
        <dl style="height:140px;">
          <dt class="black"><strong>常见问题</strong></dt>
          <dd>
            <ul class="problem">
              <li><a href="/help/register.html" target="_blank">如何注册会员？</a></li>
              <li><a href="/help/dzpayment.html" target="_blank">如何购买<?php echo $GLOBALS['setting']['site_money_name'];?>参与竞拍？</a></li>

              <li><a href="/help/my-account.html" target="_blank">如何查看我订单状态？</a></li>
              <li><a href="/help/faq.html" target="_blank">如何获得免费<?php echo $GLOBALS['setting']['site_money_name'];?>？</a></li>
            </ul>
          </dd>
        </dl>
        <div class="public_corner public_topleft"></div>
        <div class="public_corner public_topright"></div>
        <div class="public_corner public_bottomleft"></div>

        <div class="public_corner public_bottomright"></div>
      </div>
      <!--常见问题结束-->
    </div>
    <!--右边内容结束-->
  </div>
<!--底部-->
<script>
  getCode();
  function addComment(dataobj)
  {
    if(dataobj.datastatus=='success')
    {
      $('#commentlist').append('<div class="people"><div class="showsheetinfo"> <span class="avatar_small"><img src="<?php echo UC_API."/avatar.php?uid='+dataobj.uid+'&size=small&type=virtual";?>"></span><div class="tit"> <span class="tit_txt"><strong class="font16">'+dataobj.title+'</strong></span> </div><div class="t"> <span id="ftUser_'+dataobj.id+'" class="blue left">'+dataobj.username+'</span> <span style="padding-left: 5px;" class="grayB4 left">发表于：'+dataobj.updatetime+'</span> </div> <div class="floor"> <span class="blue"><a href="javascript:Reply('+dataobj.id+');">回复</a></span><span class="blue"><a href="javascript:Quote('+dataobj.id+');">引用</a></span><span onclick="javascript:window.scroll(0,0);" style="cursor: pointer;" class="blue">TOP</span> </div> </div><div class="sub"><p><span id="divContent_'+dataobj.id+'">'+dataobj.content+'</span></p></div></div>');
      $('#txtTitle').val('');
      $('#txtContent').val('');
      $('#checkcode').val('');
    }
    else if(dataobj.datastatus=='needpass')
    {
      alert('提交成功,评论正在审核中....');
      $('#txtTitle').val('');
      $('#txtContent').val('');
      $('#checkcode').val('');
    }
  }
  $('#btn_submit').click(function(){
        var cb = $(this).attr('data-cb');
		$("form[data-type='ajax']").ajaxSubmit({
			success:function(data){
                if(cb){
                  eval(cb+'('+res+')');
                } else {
                  if(data.code == 0){
                     if(data.data){
                       addComment(data.data);
                     } else {
                       alert(data.msg);
                     }
                  } else {
                    alert(data.msg);
                  }
                }
				
			}
		});
	});
</script>
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->