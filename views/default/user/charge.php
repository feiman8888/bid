<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>
<div class="mem_charge mar10 borFC" style="z-index:3">
      <dl>
        <dt><strong>购买<?php echo $GLOBALS['setting']['site_money_name'];?></strong></dt>
		
        <form id="form1" action="<?php echo SITE_ROOT;?>/index.php" method="get" target="_blank" onsubmit="btnChargeConfirm_click();">
		  <INPUT TYPE="hidden" NAME="con" value="user"><INPUT TYPE="hidden" NAME="act" value="pay">
          <dd style="position:relative;"> 
            <div class="aboutxibi">
              <?php echo $GLOBALS['setting']['site_money_name'];?>是商品竟拍和竞猜游戏的道具，仅供游戏娱乐使用；<br /><?php echo $GLOBALS['setting']['site_money_name'];?>不能够代替法定货币，不能兑换商品，不能替换运费，一旦售出，不能兑换法定货币。
			</div>
            <?php if($charge_list) {?>
            <span class="txt1 t8">
            <?php foreach($charge_list as $k=>$v){?>
			<input name="moneyid" id="cb<?php echo $v['oldprice'];?>" <?php echo $k==0?'checked':'';?> value="<?php echo $v['id'];?>" type="radio" class="radio" title="<?php echo $v['nowprice'];?>" onclick="$('#needpay').html(this.title);"/>
			<label for="cb<?php echo $v['oldprice'];?>" class="label"><?php echo $v['oldprice'];?></label>
			<?php }?>
            </span>
            <div class="txt1 t8"><span class="msg_charge"></span><strong>需支付金额： <span class="red font16" id="needpay"><?php echo $charge_list[0]['nowprice'];?></span> 元</strong></div>
            <?php } else {?>
            <div class="txt1 t8"><span class="msg_charge"></span><strong>暂无充值卡</strong></div>
            <?php }?>

            <span class="txt mem_t7"><strong>选择付款方式：</strong>(支持全国<span class="blue"><a href="https://www.alipay.com/static/bank/index.htm" target="_blank">65</a></span>家金融机构)</span>
            <div class="txt1 t8">
              <input id='pay_alipay' name="gateway" style="cursor:pointer;" value="alipay" type="radio" checked="true" class="radio1" />
              <label for='pay_alipay' class="label"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/layer/alipay.gif" id="img_zhifubao" style="cursor:pointer;" class="mem_charge_img"  border="0" alt="支付宝" /></label>

			  <input id='pay_bank' name="gateway" style="cursor:pointer;" value="bank" type="radio" class="radio1" />
              <label for='pay_bank' class="label"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/layer/alipay.gif" id="img_zhifubao" style="cursor:pointer;" class="mem_charge_img"  border="0" alt="银行转帐" /></label>
              </div>
			<?php if($charge_list) {?>
            <span class="txt mar10">
            <input type="submit" id="btnSubmit" value="确定购买"  class="mem_button03 mem_t8" onmousemove="this.className='mem_button03_over mem_t8'" onmouseout="this.className='mem_button03 mem_t8'" />
            </span> 
            <?php }?>
            <span class="charge_txt"> 亲爱的用户，为了顺利完成购买，请您务必注意：<br />
            1、为了保障您的权益，请确认您要购买<?php echo $GLOBALS['setting']['site_money_name'];?>的帐号，一旦购买成功，系统将不提供购买修正服务。<br />
            2、如有任何无法解决的问题请拨打免费客服电话：400-6666-4434</span> </dd>

         </form>
      </dl>
      <div class="public_corner public_topleft6"></div>
      <div class="public_corner public_topright6"></div>
      <div class="public_corner public_bottomleft6"></div>
      <div class="public_corner public_bottomright6"></div>
    </div>
  </div>
  <div class="clear"></div>

</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->