<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>

<ul class="bidinfo_nav mar10">
	<li class="other"><a href="<?php echo url('user','intro');?>" ><span>修改个人信息</span></a></li>
	<li class="thisclass"><a href="<?php echo url('user','password');?>"><span>修改密码</span></a></li>
	
	</ul>
	<SCRIPT LANGUAGE="JavaScript">
	<!--
		function checksubmit()
		{
			if($('#oldPwd').val()=='')
			{
				$('#message>span').html('旧密码不能为空');
				return false;
			}
			if($('#newPwd').val()=='')
			{
				$('#message>span').html('新密码不能为空');
				return false;
			}
			if($('#renewPwd').val()=='')
			{
				$('#message>span').html('重复新密码不能为空');
				return false;
			}
			if($('#renewPwd').val()!=$('#newPwd').val())
			{
				$('#message>span').html('两次密码不一致');
				return false;
			}
			return true;
		}
	//-->
	</SCRIPT>
	<div class="editdatil mem_borD6">

	  <form name="form1" action="<?php echo SITE_ROOT;?>/index.php" method="post" onsubmit="return checksubmit();">
	  <INPUT TYPE="hidden" NAME="commit" value="1"><INPUT TYPE="hidden" NAME="con" value="user"><INPUT TYPE="hidden" NAME="act" value="password">
		 <div id="message"><span class="message red"></span></div>
		 <fieldset class="fieldset">
             <p class="p">
              <label class="left">原密码：</label>
              <input type="password" name="oldPwd" id="oldPwd"　maxlength="15" value=""  class="input" />

            </p>
             <p class="p">
              <label class="left">新密码：</label>
              <input type="password" name="newPwd" id="newPwd"　maxlength="15" value=""  class="input" />

            </p>
			 <p class="p">
              <label class="left">重复新密码：</label>
              <input type="password" name="renewPwd" id="renewPwd"　maxlength="15" value=""  class="input" />

            </p>
            <span class="mem_t5 left">
              <input type="submit" value="提交修改" id="btn_submit" class="mem_button01" />

              <input type="reset" value="重 置" id="btn_reset" class="mem_button02 lmar10" />
            </span>
           </fieldset>
		</form>	
			<div class="public_corner public_bottomleft"></div>
			<div class="public_corner public_bottomright"></div>
	</div>
	
	</div>
<div class="clear"></div>
</div>

<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->