<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/login.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>
<form id="form1" action="<?php echo SITE_ROOT;?>/index.php" method="post">
<INPUT TYPE="hidden" NAME="con" value="user"><INPUT TYPE="hidden" NAME="act" value="changpassword"><INPUT TYPE="hidden" NAME="commit" value="1">
  <div class="container980">
    <div class="login_main">
      <div class="login_left">
        <div class="login">
          <dl>
            <dt><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/login/lost.gif" width="170" height="38" />
              <div class="corner topleft"></div>
              <div class="corner topright"></div>
            </dt>
            <dd>
              <div id="message" class="login_txt"  >
              </div>
              <fieldset>
              <p>
                <label for="txt_mobile">新密码：</label>
                <input type="text" class="inputlogin" tabindex="1" name="password" id="txt_password" />
              </p>
              <p>
                <label for="txt_mobile">重复新密码：</label>
                <input type="text" class="inputlogin" tabindex="2" name="password2" id="txt_password2" />
              </p>
			  <p>
                <label for="txt_mobile">短信验证码：</label>
                <input type="text" class="inputlogin" tabindex="2" name="mobilecode" id="txt_mobilecode" />
              </p>
              <p>
                <input type="submit" tabindex="3" id="btn_submit" name="btn_submit" value="" class="loginsumbit" style="background-position:-4px -185px;"/>
              </p>
              <span class="forget"> <a href="<?php echo url('user','login');?>"><span class="blue">重新登录？</span></a> </span>
              </fieldset>
            </dd>
            <div class="corner bottomleft"></div>
            <div class="corner bottomright"></div>
          </dl>
        </div>
      </div>
      <div class="login_right">
<span class="a"><strong class="FF4000">便宜有好货：</strong><span class="gray8">用低于市场的价格赢取时尚潮流的正版行货！</span></span> 
<span class="a t3"><strong class="FF4000">诚信可信赖：</strong><span class="gray8">尊重数据事实，绝不失信与我们的每一个用户！</span></span>
<span class="a t3"><strong class="FF4000">购物娱乐化：</strong><span class="gray8">变革无趣的购买生活，让您的购物流程娱乐化！</span></span>
<span class="a t3"><strong class="FF4000">免费得<?php echo $GLOBALS['setting']['site_money_name'];?>：</strong><span class="gray8">手机免费注册即可获得500<?php echo $GLOBALS['setting']['site_money_name'];?>！</span></span>
        <p class="registersumbit t3"> <a href="<?php echo url('user','register');?>"><button type="button" alt="现在就注册" width="137" height="38" />现在就注册</button></a> </p>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</form>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->