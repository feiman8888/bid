<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<link href="<?php echo STATIC_ROOT;?>jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="<?php echo STATIC_ROOT;?>jquery/jquery.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?>jquery/jquery-ui.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
<?php include(VIEWS_PATH."public/user_info.php");?>

    <ul class="bidinfo_nav mar10">
    <li class="thisclass"><a href="<?php echo url('user');?>" ><span>我参与的竞拍</span></a></li>
    <li class="other"><a href="<?php echo url('user','gain');?>"><span>成功的竞拍</span></a></li>
    <li class="other"><a href="<?php echo url('order','list');?>"><span>我的订单</span></a></li>
	<li class="other"><a href="<?php echo url('order','list',array('type'=>1));?>"><span>积分换购</span></a></li>
	<li class="other"><a href="<?php echo url('order','show');?>"><span>我的晒单</span></a></li>
  </ul>
  <div class="bidinfo_nav_txt"> <span class="d_w11">商品</span> <span class="d_w22">竞拍价（元）</span> <span class="d_w33">出价人</span> <span class="d_w44">出价/<?php echo $GLOBALS['setting']['site_money_name'];?></span> <span class="d_w55">状态</span>

    <div class="public_corner public_bottomleft"></div>
    <div class="public_corner public_bottomright"></div>
  </div>
        <!--竞拍结束-->
		<?php if($bidinfo){foreach($bidinfo as $k=>$v){$thumb=thumb($v['goods']['thumb'])?>
			<div class="bidinfo_main mar10 borD6">
			<dl>
			<dt> <strong>第<?php echo $v['goods']['goods_id'];?>期</strong> <span class="grayB4 lmar10">共<?php echo $v['goods']['bidlog'];?>人参与</span> <?php if($v['goods']['goods_status']>-1){?><span class="grayB4 lmar10">结束时间：<?php echo date('Y-m-d H:i:s',$v['goods']['lasttime']);?></span><?php }?> <span class="blue lmar10"> 
			<?php if($v['goods']['goods_status']>-1){?>
			<a href="<?php echo url('index','complete',array('id'=>$v['goods']['goods_id']));?>" target="_blank">详细</a>
			<?php } else{?>
			<a href="<?php echo url('index','goods',array('id'=>$v['goods']['goods_id']));?>" target="_blank">详细</a>
			<?php }?>
			</span> </dt>

			<dd class="d_w1 l_bor1"> <span class="borEB img"> <?php if($v['goods']['goods_status']>-1){?>
			<a href="<?php echo url('index','complete',array('id'=>$v['goods']['goods_id']));?>" target="_blank">
			<?php } else{?>
			<a href="<?php echo url('index','goods',array('id'=>$v['goods']['goods_id']));?>" target="_blank">
			<?php }?> <img src="<?php echo $thumb[0];?>" class="imgbor left" width="90" height="76" alt="<?php echo $v['goods']['goods_name'];?>" /> </a>
			<!--结束-->
			</span> 
			<table>
			<tr>
			<td>
			<span class="name blue"> <?php if($v['goods']['goods_status']>-1){?>
			<a href="<?php echo url('index','complete',array('id'=>$v['goods']['goods_id']));?>" target="_blank">
			<?php } else{?>
			<a href="<?php echo url('index','goods',array('id'=>$v['goods']['goods_id']));?>" target="_blank">
			<?php }?> <?php echo $v['goods']['goods_name'];?> </a> </span> <span class="price">市场价：￥<?php echo $v['goods']['marketprice'];?></span> 
			</td>

			</tr>
			</table>
			</dd>
			<dd class="d_w2 l_bor1">￥<?php echo $v['goods']['nowprice'];?></dd>
			<dd class="d_w3 l_bor1">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><span class="blue"><?php echo $v['goods']['currentuser'];?></span>

			<br />
			<!--竞拍结束-->
			<?php if($v['goods']['goods_status']>-1){?>赢得<?php } else{?>当前领先<?php }?>
			</td>
			</tr>
			</table>
			</dd>
			<dd class="d_w4 l_bor1"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr height="24">

			<td>出价：<?php echo $v['count'];?>次</td>
			</tr>
			<tr height="24">
			<td><?php echo $GLOBALS['setting']['site_money_name'];?>：<?php echo $v['money'];?></td>
			</tr>
			</table>

			</dd>
			<dd class="d_w5">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td>
			<?php if($v['goods']['goods_status']>-1){?>
			<div class="status"><span class="grayB4">竞拍已结束</span><br/>
			<?php if(isset($v['returnlog']['resultmoney']) && $v['returnlog']['resultmoney']>0){?>已返币<?php echo $v['returnlog']['resultmoney'];?> <?php }?></div>
			
			<div class="membutton_f">
			<?php if(($v['goods']['currentuid']!=$GLOBALS['session']->get('uid'))){?>
			
			<p id="bid_<?php echo $v["gid"];?>"><?php if(!$v['returnlog']){?>您可以选择<br/>
			<?php if($v['goods']['backmoney']>0){?><input class="membutton_bg" type="button" onclick="javascript:bid_returnb('<?php echo $v["gid"];?>');" value="竞拍返币"><?php }?>

			<?php if($v['goods']['lasttime']>=time()-259200){?><input type="button" class="chabtn" onclick="javascript:Order_Dialog('<?php echo $v["gid"];?>');" value="保价购买" title="成功用户不能使用保价购买"/><?php } else{?>超出7天不能再保价购买了<?php }?><?php }?></p>
			<?php } elseif($v['goods']['goods_status']>1){?>
			<input type="button" class="chabtn" onclick="javascript:Order_Dialog('<?php echo $v["gid"];?>');" value="全价购买"/>
			<?php } elseif($v['goods']['goods_status']<2){?>
			<input type="button" class="chabtn" onclick="javascript:Order_Dialog('<?php echo $v["gid"];?>');" value="下订单"/>
			<?php }?>
			</div>   
			<?php } else{?>
			<div class="status"> <span class="red">正在竞拍中</span>
                  <!--参与的竞拍还在竞拍中-->
            </div>
			<div class="membutton"> <a href="<?php echo url('index','goods',array('id'=>$v['goods']['goods_id']));?>" target="_blank"> 继续出价 </a> </div>

			<?php }?>
			</td>

			</tr>
			</table>
			</dd>
			</dl>
			<!--竞拍结束-->
			<div class="public_corner public_topleft"></div>
			<div class="public_corner public_topright"></div>
			<div class="public_corner public_bottomleft"></div>
			<div class="public_corner public_bottomright"></div>
			</div>
	<?php }}?>

        <div class="pages" >
      <table align="center" class="mar10"  >
        <tbody>
          <tr>

            <td>
			<div class="list_page"> <?php echo $pageinfo;?></div>
              </td>
          </tr>
        </tbody>

      </table>
    </div>
  </div>
  <div class="clear"></div>
</div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function bid_returnb(gid)
	{
		$.post(site_root+"?con=order&act=returnmoney&gid="+gid,{},
		function(data){
			if(data.code == 1){
              alert('返利成功');
              $('#bid_'+data.data.id).html('竞拍已返币'+data.data.backmoney);
            } else{
              alert(data.msg);
            }
		},'json');
	}
//-->
</SCRIPT>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->