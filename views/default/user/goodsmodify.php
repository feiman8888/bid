<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<?php include(VIEWS_PATH."public/header.php");?>
<SCRIPT LANGUAGE="JavaScript">
<!--
i=0;
function addmthumb()
{
	
	if(i<3)
	{
		$('#thumbarea').append('<div><INPUT TYPE="file" NAME="local_thumb[]" style="width:220px;"><input type="button" value="-" class="normal_button" onclick="$(this).parent().remove();"/></div>');
		
		i++;
	}
	else
	{
		alert('只允许四张图片');
	}
}
function deletethumb(file,actionobj)
{
	if(confirm('确认删除？'))
	{
		var url=site_root+"/index.php?con=user&act=deleteImg";
		url=url+"&rand="+Math.random();
		$.get(url, {img:file},  
				function(data){
					if(actionobj!='null')
					{
						if(parseInt(data)==1)
						{
							alert('删除成功');
							$('#'+actionobj).remove();
						}
						else
						{
							alert('删除不成功');
						}
					}
			});
	}
}
//-->
</SCRIPT>
<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>

<ul class="bidinfo_nav mar10">
	<li class="other"><a href="<?php echo url('user','goods');?>" ><span>正在竞拍</span></a></li>
    <li class="other"><a href="<?php echo url('user','complete');?>"><span>成功的竞拍</span></a></li>
    <li class="thisclass"><a href="<?php echo url('user','goodsmodify');?>"><span>发布竞拍</span></a></li>
	</ul>
	
	<div class="editdatil mem_borD6">

	<form enctype="multipart/form-data" action="<?php echo SITE_ROOT;?>/index.php?con=user&act=goodsmodify" method="post">
	<INPUT TYPE="hidden" NAME="commit" value="1">
    <INPUT TYPE="hidden" NAME="updateid" value="<?php echo $goods['goods_id'];?>">
		 <div id="message"><span class="message red"></span></div>
		 <fieldset class="fieldset">
		   <p class="p">
              <label class="left">商品名称：</label>
              <INPUT TYPE="text" class="input"  NAME="goods_name" id="subject"  size="70" value="<?php echo $goods['goods_name'];?>"> </p>

           <p class="p">
             <label class="left">所属分类：</label>
              <SELECT NAME="cateid">
		<?php if($goods['cateid']){?><option value="<?php echo $goods['cateid'];?>"><?php echo $GLOBALS['cate'][$goods['cateid']]['catename'];?></option><?php }?>
		<?php foreach($GLOBALS['cate'] as $k=>$v){?>
		<option value="<?php echo $k;?>"><?php echo $v['catename'];?></option>
		<?php }?>
	  </SELECT></p>

           <p class="p">
              <label class="left">开始日期：</label>
              <INPUT TYPE="text" class="input"  NAME="starttime" id="starttime" value="<?php echo date('Y-m-d H:i:s',$goods['starttime']?$goods['starttime']:time()+86400);?>"><INPUT TYPE="button" VALUE="选择" ONCLICK="HS_setDate(document.getElementById('starttime'))"></p>
             <p class="p">
              <label class="left">结束日期：</label>
              <INPUT TYPE="text" class="input"  NAME="lasttime" id="lasttime" value="<?php echo date('Y-m-d H:i:s',$goods['lasttime']?$goods['lasttime']:time()+172800);?>"><INPUT TYPE="button" VALUE="选择" ONCLICK="HS_setDate(document.getElementById('lasttime'))">

            </p>
             <p class="p">
            <label class="left">缩略图：<INPUT TYPE="button" VALUE=" + " class="normal_button" ONCLICK="addmthumb();"></label>
             <INPUT TYPE="file" NAME="local_thumb[]">
			 </p>
			 <div id="thumbarea" style="padding-left:130px;"></div>
			  <div class="clear"></div>
			   <?php 
			   if($goods['thumb']){
			   $thumb=explode(',',$goods['thumb']);
			   foreach($thumb as $k=>$v){
			   ?>
			   <div style="float:left;width:100px;height:140px;overflow:hidden;text-align:center;" id="thumb<?php echo $k;?>"><img onclick="window.open(this.src,'','');" src="<?php echo SITE_ROOT;?>/<?php echo $v;?>" width="90px" height='76px'/><br/><INPUT TYPE="button" VALUE="删除" ONCLICK="deletethumb('<?php echo $v;?>','thumb<?php echo $k;?>');"></div>
			   <?php }}?>
			     <div class="clear"></div>
             <p class="p">
             <label class="left">原价：</label>
             <INPUT TYPE="text" class="input"  NAME="marketprice" value="<?php echo $goods['marketprice'];?>">
			 </p>
            <p class="p">
              <label class="left">现价：</label>
			  <INPUT TYPE="text" class="input"  NAME="nowprice" value="<?php echo $goods['nowprice'];?>">
            </p>
            <div>
              <label class="left">特别提示：</label>
			  <textarea NAME="content" cols="40" rows="8"><?php echo $goods['content'];?></textarea>
            </div>
            <div class="clear"></div>
			<DIV ID="" CLASS="">
			<label class="left">详情：</label>
				 <DIV ID="" style="float:left;">
					<?php echo $this->edit($goods['details'],'details','details');?>
				 </DIV>
			</DIV>
            <span class="mem_t5 left">
              <input type="submit" value="提交修改" id="btn_submit" class="mem_button01" />

              <input type="reset" value="重 置" id="btn_reset" class="mem_button02 lmar10" />
            </span>
           </fieldset>
		</form>	
			<div class="public_corner public_bottomleft"></div>
			<div class="public_corner public_bottomright"></div>
	</div>
	
	</div>
<div class="clear"></div>
</div>

<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->