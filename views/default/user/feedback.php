<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">
	
<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
<?php include(VIEWS_PATH."public/user_info.php");?><ul class="bidinfo_nav mar10">
	<li class="thisclass"><a href="/member/myfeedback" ><span>我的意见反馈</span></a></li>
	</ul>
	<div class="feedback_admin mem_borD6">
			<ul class="feedback">
	<div class="feedback_w">
	<?php if($feedback){foreach($feedback as $v){?>
		<li><strong class="feedback_titile"><?php echo $v['title'];?></strong></li>
		<li class="feedback_time grayB4"><?php echo date("Y-m-d H:i:s",$v['updatetime']);?></li>
					<?php if($v['isgood']){?><li class="feedback_green" style="width:100px;">已采纳,奖励<?php echo $v['givemoney'];?><?php echo $GLOBALS['setting']['site_money_name'];?><?php } else{?><li class="feedback_gray" style="width:100px;">未采纳</li><?php }?></li>
						<li class="feedback_content"><?php echo $v['content'];?></li>
		<?php }}?>
			</ul>
		<div class="pages" >
      <table align="center" class="mar10"  >
        <tbody>
          <tr>
            <td>
	<div class="list_page"><?php echo $pageinfo;?></div>
	  </td>
          </tr>
        </tbody>
      </table>
    </div>
    	
			<div class="public_corner public_bottomleft"></div>
			<div class="public_corner public_bottomright"></div>
	</div>
	
	</div>
<div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->