<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>
<ul class="bidinfo_nav mar10">
	<li class="thisclass"><a href="/member/smsnotify" ><span>我的短信订阅</span></a></li>
	</ul>
	<div class="mem_box bpad10">
	<div class="mem_boxx">
	<div class="bidinfo_nav_txt01">

		<span class="sms_w1 txtcenter lmar10" style="width:300px;">竞拍</span>
		<span class="sms_w2 txtcenter">订阅时间</span>
		<span class="sms_w3 txtcenter">发送时间</span>
		<span class="sms_w3 txtcenter">状态</span>
	</div>
	 <?php if($smslist){foreach($smslist as $k=>$v){?>
	 <div class="mag_main">
		<span class="sms_w1 txtleft" style="width:300px;">
		<a href="<?php echo url('index','goods',array('id'=>$v['gid']));?>" target="_blank">

		<span class="blue">[第<?php echo $v['goods_id'];?>期]</span> <?php echo $v['goods_name'];?></a>
					<span class=" red"><?php echo $v['status']?'已发送':'未发送';?></span>
				</span>
		<span class="sms_w2 txtcenter"><?php echo date('Y-m-d H:i:s',$v['updatetime']);?></span>
		
		<span class="sms_w3 txtcenter "><?php echo date('Y-m-d H:i:s',$v['sendtime']);?></span>
		<span class="sms_w3 txtcenter "><?php echo $v['status']>0?'已':'未';?>发送</span>
	</div>
   <?php }}?>
		
	<div class="pages" >
      <table align="center" class="mar10"  >
        <tbody>
          <tr>
            <td>
			<div class="list_page"><?php echo $pageinfo;?></div>

              </td>
          </tr>
        </tbody>
      </table>
    </div></div>
	<div class="public_corner public_bottomleft"></div>
	<div class="public_corner public_bottomright"></div>
	</div>
	
	</div>

	<div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->