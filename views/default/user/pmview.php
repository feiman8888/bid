<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>
<script type="text/javascript">
$(document).ready(function(){
	$(".delete").click(function(){
		if(!confirm('确认要删除信件！'))
		{
			return false;
		}
			});
});
</script>
<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>
<ul class="bidinfo_nav mar10">
	<li class="thisclass"><a href="<?php echo url('user','message');?>" ><span>站内信</span></a></li>
	
	</ul>
	<div class="mem_box bpad10">
	<div class="bidinfo_nav_txt01">
		
		<span class="x_w7 blue mem_t33"><a href="<?php echo url('user','message');?>" >返回</a></span>
		<span class="x_w7 blue txtcenter"><a href="<?php echo url('user','pmdelete',array('id'=>$pm['id']));?>" >删除</a></span>
		<span class="x_w6 blue"></span>
	</div>
	<div class="bidinfo_nav_txt02">
		<div class="msg_txt">
			<strong class="font14"><?php echo $pm['title'];?></strong><br>
发件人：<strong class="green"><?php echo !empty($pm['from_uid'])?$pm['from_uid']:$GLOBALS['setting']['site_title'];?></strong>
					<span class="grayB4">时间：<?php echo date("Y-m-d H:i:s",$pm['updatetime']);?></span>
		</div>
	</div>
	
	<div class="msg_txt_main"><?php echo $pm['content'];?></div>
	
	<div class="public_corner public_bottomleft"></div>
	<div class="public_corner public_bottomright"></div>
	</div>
	</div>
	<div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->