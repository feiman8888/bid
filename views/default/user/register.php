<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/register.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="<?php echo STATIC_ROOT;?>jquery/jquery.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?>jquery/jquery-ui.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?>jquery/jquery.form.min.js"></SCRIPT>
<script type="text/javascript">
//2.是否是手机
function is_mobile(numstr)
{
	var reg = new RegExp("^1[0-9]{10}$");
	return reg.test(numstr);
}

//验证Email格式是否正确
function is_email(mail){
	var reg = /^([a-zA-Z0-9]+[_|\_|\.]?[-]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    return reg.test(mail);
}

//3.验证状态
var validate = {
		"mobile"	:false,
		"userName"	:false,
		"passWord"	:false,
		"rePassWord":false,
		"email"		:false
};

var rePost = '';
$(document).ready(function() {

//1.创建jQuery对象
var $mobile 		= $("#txt_mobile");
var $mobileMsg 		= $("#mobile_message");

var $userName 		= $("#txt_username");
var $userNameMsg 	= $("#username_message");

var $email 			= $("#txt_email");
var $emailMsg		= $("#email_message");

var $passWord		= $("#txt_password");
var $passWordMsg	= $("#pwd_message");

var $rePassWord		= $("#txt_repassword");
var $rePassWordMsg	= $("#repwd_message");

var $imgcode=$('#txt_imgcode');
var $imgcodeMessage=$('#imgcode_message');

var $submit			= $("#btn_submit");
var $register_code  = $("#txt_verifycode");

//4.验证方法
//验证手机
var funValicateMobile = function(){

	var mobile = $mobile.val();
	if(mobile == "")
	{
		validate["mobile"] = false;
		//$mobileMsg.html("");
		$mobileMsg.html("<span class='tipsred'><span class='img'></span>手机不能为空！</span>");
		return;
	}
	else
	{
		if(!is_mobile(mobile)){
			validate["mobile"] = false;
			setTimeout(function(){
				$mobileMsg.html("<span class='tipsred'><span class='img'></span>手机格式不正确！</span>");
			},100);
		}else{
			$.post(site_root+"?con=user&act=check_register&dotype=mobile", {"val":mobile},
					function(dataobj){
						eval('var data='+dataobj);
						if(data.datastatus == 'success'){
							validate["mobile"] = true;
							$mobileMsg.html("<span class='tipssuccess'></span>");
						}else{
							message={'error':'手机格式不正确','haved':'手机已经存在'};
							showmessage=eval('message.'+data.message);
							validate["mobile"] = false;
							$mobileMsg.html('<span class="tipsred"><span class="img"></span>'+showmessage+'</span>');
						}
						funValidateSuccess();
					});
		}
	}
	funValidateSuccess();
};

//验证会员名
var funValidateUserName = function()
{
	var userName = $userName.val();
	var nameLength = userName.replace(/[^\x00-\xff]/g, 'xx').length;

	if(userName == "")
	{
		validate["userName"] = false;
		$userNameMsg.html('<span class="tipsred"><span class="img"></span>用户名不能为空</span>');	
	}
	else
	{
		if(nameLength < 4 || nameLength > 10){
			validate["userName"] = false;
			$userNameMsg.html("<span class='tipsred'><span class='img'></span>共" + nameLength + "个字符，要求4-10个字符，一个汉字为两个字符。</span>");
		}else{
		$.post(site_root+"?con=user&act=check_register&dotype=username", {"val":userName},
			function(dataobj){
				eval('var data='+dataobj);
				if(data.datastatus == 'success') {
					validate["userName"] = true;
					$userNameMsg.html("<span class='tipssuccess'></span>");
				}else{
					message={'haved':'用户名已经被注册'};
					showmessage=eval('message.'+data.message);
					validate["userName"] = false;
					$userNameMsg.html('<span class="tipsred"><span class="img"></span>'+showmessage+'</span>');
				}
				funValidateSuccess();
			});
		}
	}
	funValidateSuccess();
};

//验证密码
var funValidatePassWord = function()
{
	var passWord = $passWord.val();
	if(passWord == "")
	{
		validate["passWord"] = false;
		$passWordMsg.html("");
	}
	else
	{
		if(passWord.length < 6 || passWord.length > 16) {
			validate["passWord"] = false;
			$passWordMsg.html("<span class='tipsred'><span class='img'></span>密码在6-16个字符内！</span>");
		}else{
			validate["passWord"] = true;
			$passWordMsg.html("<span class='tipssuccess'></span>");
		}
	}
	funValidateSuccess();
};

//验证确认密码
var funValidateRePassWord = function()
{
	if($rePassWord.val() == "")
	{
		validate["rePassWord"] = false;
		$rePassWordMsg.html("");
	}
	else
	{
		if($rePassWord.val() != $passWord.val()){
			validate["rePassWord"] = false;
			$rePassWordMsg.html("<span class='tipsred'><span class='img'></span>两次密码输入不一致！</span>");
		}else{
			validate["rePassWord"] = true;
			$rePassWordMsg.html("<span class='tipssuccess'></span>");
		}
	}
	funValidateSuccess();
};

//验证电子邮箱
var funValidateEmail = function()
{ 
	var email = $email.val();
	if(email == "")
	{
		validate["email"] = false;
		$emailMsg.html("<span class='tipsred'><span class='img'></span>邮箱不能为空！</span>");
	}
	else
	{
		if(!is_email(email)){
			validate["email"] = false;
			setTimeout(function(){
				$emailMsg.html("<span class='tipsred'><span class='img'></span>邮箱格式不正确！</span>");
			},100);
		}else{
			$.post(site_root+"/index.php?con=user&act=check_register&dotype=email", {"val":email},
			function(dataobj){
				eval('var data='+dataobj);
				if(data.datastatus == 'success') {
					validate["email"] = true;
					$emailMsg.html("<span class='tipssuccess'></span>");
				}else{
					message={'haved':'邮箱已被注册','error':'邮箱格式不正确'};
					showmessage=eval('message.'+data.message);
					validate["email"] = false;
					$emailMsg.html('<span class="tipsred"><span class="img"></span>'+showmessage+'</span>');
				}
				funValidateSuccess();
			});
			
		}
	}
	funValidateSuccess();
};

var funValidateCode = function()
{
	var register_code = $register_code.val();
	if(register_code.length < 4){
		$register_code.focus();
		$("#verifyCode").html("<span class='tips'><span class='img'></span>请输入验证码。</span>");
		return false;
	}
}

//判断是否全部通过验证
var funValidateSuccess = function(){
	for(var v in validate){
		if(!validate[v]){
			$submit.attr({'disabled':'disabled','class':'nosubmit'});		//禁止提交
			return false;
		}
	}
	$submit.removeAttr("disabled").attr('class','submit');		//允许提交
	return true;
};

	funValidateSuccess();

	//绑定事件
	$mobile.focus(function(){
		if(validate["mobile"] == false){
			$mobileMsg.html("<span class='tips'><span class='img'></span>手机号码可用于登陆<?php echo $GLOBALS['setting']['site_title'];?>，注册完全免费。</span>");
		}
	}).blur(funValicateMobile);
	
	$userName.focus(function(){
		if(validate["userName"] == false){
			$userNameMsg.html("<span class='tips'><span class='img'></span>4-10个字符，一个汉字为两个字符。</span>");
		}
	}).blur(funValidateUserName);
	
	$passWord.focus(function(){
		if(validate["passWord"] == false){
			$passWordMsg.html("<span class='tips1'><span class='img1'></span>6-16个字符。为了您的账户安全，在设置密码时请尽量使用英文字母、数字和符号的组合。</span>");
		}
	}).blur(funValidatePassWord);
	
	$rePassWord.focus(function(){
		if(validate["rePassWord"] == false){
			$rePassWordMsg.html("<span class='tips'><span class='img'></span>请再输入一次密码。</span>");
		}
	}).blur(funValidateRePassWord).keyup(function(){
			if($rePassWord.val() == $passWord.val()){
				funValidateRePassWord();
			}
	});

	$email.focus(function(){
		if(validate["email"] == false){
			$emailMsg.html("<span class='tips'><span class='img'></span>请输入您常用的邮箱。</span>");
		}
	}).blur(funValidateEmail);

	$("#txt_mobile,#txt_username,#txt_email,#txt_password,#txt_repassword").focus(function(){
		$(this).parent().addClass("bgHeighLight").removeClass("main");
	}).blur(function(){
		$(this).parent().addClass("main").removeClass("bgHeighLight");
	});
	
	//提交
	$("form").submit(function(){
		var success = funValidateSuccess();
		if(!success){
			return false;
		}
			});

	if(rePost == 'true'){
		validate["mobile"] = true;
		validate["userName"] = true;
		validate["email"] = true;
		$mobileMsg.html("<span class='tipssuccess'></span>");
		$userNameMsg.html("<span class='tipssuccess'></span>");
		$emailMsg.html("<span class='tipssuccess'></span>");
		funValidateSuccess();
	}
	else
	{
		$mobile.focus();
	}
});

</script>

<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">

<div class="container980">
<div class="register_left">
	<div class="left_box01a mar10 borD6"> 
	<div class="left_box011"> 
	<form method="post" data-type='ajax' action="<?php echo SITE_ROOT;?>/index.php" id="form1">
	<INPUT TYPE="hidden" NAME="<?php echo $cname;?>" value="1"><INPUT TYPE="hidden" NAME="con" value="user"><INPUT TYPE="hidden" NAME="act" value="register"/>
      <div class="tableregister">
	          <div class="main">
          <label>手&nbsp;&nbsp;机&nbsp;&nbsp;号：</label>
          <input type="text" id="txt_mobile" name="mobile" tabindex="1" class="inputregister">
          <div id="mobile_message"></div>
        </div>
        <div class="main">
          <label>会&nbsp;&nbsp;员&nbsp;&nbsp;名：</label>
          <input type="text" id="txt_username" name="username" tabindex="2" class="inputregister">
          <div id="username_message"></div>
        </div>
        <div class="main">
          <label>电子邮箱：</label>
          <input type="text" id="txt_email" name="useremail" tabindex="3" class="inputregister">
          <div id="email_message"></div>
        </div>
        <div class="main">
          <label>登录密码：</label>
          <input type="password" id="txt_password" name="userpassword" tabindex="4" class="inputregister">
          <div id="pwd_message"></div>
        </div>
        <div class="main">
          <label>确认密码：</label>
          <input type="password" id="txt_repassword" name="txt_repassword" tabindex="5" class="inputregister">
          <div id="repwd_message"></div>
        </div>
		<?php if($GLOBALS['setting']['site_imgcode']){?>
		<div class="main">
          <label>验证码：</label>
           <TABLE>
		  <TR>
			<TD> <input type="text" id="txt_verifycode" name="imgcode" tabindex="5" maxlength="6" class="inputregister"></TD>
			<TD><img src="" id="txt_code" onclick="getCode();"/></TD>
		  </TR>
		  </TABLE>
          <div id="verifyCode"></div>
        </div>
		<?php }?>
       <input type="button" class="nosubmit" value="" id="btn_submit" tabindex="6" disabled="">
      </div>
    </form>
    <div class="clear"></div>
    <div class="agreements">
      <ul>
        <li>
          <h3><?php echo $GLOBALS['setting']['site_title'];?>服务协议</h3>
          <div class="content"> <strong>总则</strong><br>
            欢迎访问<?php echo $GLOBALS['setting']['site_url'];?>(<?php echo $GLOBALS['setting']['site_title'];?>)。并使用本网站为您提供的各项服务。以下所述条款和条件即构成用户与本公司就使用服务所达成的协议，一旦用户确认本服务协议后，本服务协议即在用户和Bidcms之间产生法律效力。请用户务必在注册之前认真阅读全部服务协议内容，如有任何疑问，可向Bidcms咨询。无论用户事实上是否在注册之前认真阅读了本服务协议，只要用户点击协议正本下方的“确认”按钮并按照注册程序成功注册为用户，用户的行为仍然表示其同意并签署了本服务协议。<br>
            本公司有权随时修改下述条款和条件，并只需公示于Bidcms网站，而无需征得用户的事先同意。修改后的条款于公示时即时生效。在本公司修改服务条款后，用户继续使用服务应被视作用户已接受了修改后的条款。<br>
            <strong>一、用户及用户注册</strong><br>
            1、用户必须是具备完全民事行为能力的自然人。无民事行为能力人、限制民事行为能力人册为Bidcms用户并参与Bidcms提供的服务，其与Bidcms之间的服务协议自始无效，Bidcms一经发现，有权立即注销该用户。<br>
            2、在登记过程中，用户将选择注册名和密码。注册名的选择应遵守法律法规及社会公德。用户必须对自己的密码保密，用户将对自己注册名和密码下发生的所有活动承担责任。如发现任何人未经您同意使用您的注册名跟密码，用户需立即通知我公司;对该行为造成的损害，我公司不负任何责任。<br>
            <strong>二、用户的权利和义务</strong><br>
            1、用户应当保证在使用Bidcms各项服务中遵守法律法规，尊重社会公德，遵守诚实信用原则，不扰乱网络秩序;<br>
            2、用户有权根据本协议参与竞拍购物等其他无特殊限制的服务，但不得实施任意违背竞拍原则的作弊手段;<br>
            3、用户必须按照申请注册会员的表格，真实、准确、完整的填写资料;维持并及时更新资料，使其保持真实、准确、完整和反应当前情况。倘若因用户提供的资料不真实、不准确造成的经济损失，由用户自行承担。<br>
            4、用户发表的言论、评价应当真实，不得侮辱、诽谤和恶意评价他人;<br>
            5、用户同意接收来自Bidcms发出的邮件或信息。Bidcms保留通过邮件和短信的形式，对本网站注册、竞拍购物用户发送身份验证等告知服务的权利。如果用户在Bidcms注册、参与竞拍，表明用户已默示同意接受此项服务。如果用户不想接收来自Bidcms的邮件和短信，用户可以向Bidcms客服提出退阅申请，并注明您的E-mail地址、手机号或相关地址信息，Bidcms会在收到申请后为您办理退阅。<br>
            <strong>三、我公司的权利和义务</strong><br>
            1、我公司有义务在现有技术上维护整个平台的正常运行，并努力提升和改进技术;<br>
            2、对于您在Bidcms的不当行为或任何我公司认为应当终止服务的情况，我公司有权删除信息或终止服务，且无须征得您的同意;<br>
            3、您使用Bidcms务时，本网站有权接收并记录您的个人信息，包括但不限于IP地址、网站Cookie中的资料及您要求取用的网页记录等;<br>
            4、对于用户竞拍得到的产品，我公司有义务在用户付款后第一时间寄出。<br>
            <strong>四、隐私权</strong><br>
            我公司不公开您选择保密的注册信息及其他个人信息。但在下列情况下，我公司有权全部或部分披露您的保密信息。<br>
            1、根据法律规定，或应行政机关、司法机关要求，向第三人或行政机关、司法机关披露;<br>
            2、您出现违反Bidcms站规则，需要向第三方披露的;<br>
            3、根据法律和Bidcms站规则，其他我公司认为适合披露的。<br>
            <strong>五、管辖</strong><br>
            本条款将适用中华人民共和国的法律，因本协议或本公司服务所引起的或与其有关的任何争议，应向Bidcms所在地人民法院提起诉讼。 </div>
        </li>
      </ul>
    </div></div>
		<div class="public_corner public_topleft"></div>
		<div class="public_corner public_topright"></div>
		<div class="public_corner public_bottomleft"></div>
		<div class="public_corner public_bottomright"></div>
	</div>
</div>
  <div class="register_right">
    <div class="right_box02 borD6">
      <dl>
        <dt><strong class="left">最新成交</strong>

          <div class="publicmore grayB4"> <a href="<?php echo url('index','history');?>" target="_blank"> 更多 </a></div>
        </dt>
        <dd>
             <?php foreach($goods_success as $val){$thumb=thumb($val['thumb']);?>
				<!--单个开始-->
				<div class="leftimg_txt">
				<div class="leftimg borEB"><a href="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>" title="<?php echo $val['goods_name'];?>"  target="_blank"><img src="<?php echo $thumb[0];?>" alt="<?php echo $val['goods_name'];?>" width="70" /></a></div>
				<table>
				<tr>
				<td>
				<span class="leftname">
				<a href="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>" title="<?php echo $val['goods_name'];?>" target="_blank">
				<?php echo $val['goods_name'];?>							</a>
				</span>
				<span class="leftprice">
				<span class="gray62">获胜者：</span><span class="blue"><?php echo $val['currentuser'];?></span>
				</span>
				<span class="leftprice">
				<span class="gray62">成交价：</span><span class="yellow66 font14"><strong>￥<?php echo $val['nowprice'];?></strong></span>
				</span>
				</td></tr></table>
				</div>
				<!--单个结束-->
			<?php }?>
                  </dd>
      </dl>
      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <div class="right_box borD6 mar10 bpad10">

      <dl>
        <dt><strong>在线客服</strong></dt>
        <dd>
          
          <div class="helpbutton tmar12"><img border="0" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/index4/tell_03.gif" alt="服务热线"><h1 style="font-size:30px;color:#000;font-family:黑体"><?php echo isset($GLOBALS['setting']['site_tel'])?$GLOBALS['setting']['site_tel']:'00-000-000';?></h1></div>
          <div class="helpbutton mar10"> <span class="email"></span> <span class="emailtxt">服务邮箱：<?php echo $GLOBALS['setting']['site_email'];?></span</div>
        </dd>
      </dl>

      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
    getCode();
	$('#btn_submit').click(function(){
		$("form[data-type='ajax']").ajaxSubmit({
			success:function(res){
                var data = JSON.parse(res);
				if(data.code == 0){
					if(data.url!=''){
                      window.location.replace(data.url);
                    } else {
                      window.location.replace('/index.php');
                    }
				} else {
                  alert(data.msg);
                }
				
			}
		}); 
	});
</script>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->