<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
?>
<div class="top_bg">
<div class="game_con">
<?php if(isset($guess)){?>
<?php if($guess['win']){?>
<!--结束提示哪个胜-->
<?php if($guess['wintype']=='single'){?><div class="odd_winbg"></div><?php } else{?><div class="even_winbg"></div><?php }?>
<!--结束提示哪个胜-->
<?php }?>
<!--单进度条开始-->
<div class="even_odd g_color1">
<strong class="font14 left odd_t">单</strong>
<div class="odd_zero" onmouseover="$('#sover').show();" onmouseout="$('#sover').hide();"  title="<?php echo number_format($guess['single_count']);?><?php echo $GLOBALS['setting']['site_money_name'];?>">
<div class="even_zero_over" id="sover" style="display:none;"></div>
<div style="" class="even_odd_wide" id="singletips">
<div style="width: <?php echo $guess['single_p'];?>%;" class="odd"  id="sw">
</div>
<?php if($guess['single_p']<=80){?>
<div id="spercentage" class="percentage"><?php echo $guess['single_p'];?>%</div>
<?php }?>
</div>
</div>
<span class="left odd_t">赢率：</span><strong id="sratio" class="left odd_t"><?php echo $guess['single_discount'];?></strong> </div>
<!--单进度条结束-->
<!--双进度条开始-->
<div class="even_odd g_color2">
<strong class="font14 left even_t">双</strong>
<div class="even_zero" onmouseover="$('#dover').show();" onmouseout="$('#dover').hide();" title="<?php echo number_format($guess['double_count']);?><?php echo $GLOBALS['setting']['site_money_name'];?>">
<div class="even_zero_over" id="dover" style="display:none;"></div>
<div style="" class="even_odd_wide" id="doubletips">

<div style="width: <?php echo $guess['double_p'];?>%;" class="even" id="dw">

</div>
<?php if($guess['double_p']<=80){?>
<div id="dpercentage" class="percentage"><?php echo $guess['double_p'];?>%</div>
<?php }?>
</div>
</div>
<span class="left even_t">赢率：</span><strong id="dratio" class="left even_t"><?php echo $guess['double_discount'];?></strong> </div>
<!--双进度条结束-->
<!--投入区-->
<?php if($guess['win']){?>
<!--结束-->
<div class="g_more grayB4">总投入：<strong class="yellow66 font14"><?php echo number_format($guess['guesstotal']);?></strong><?php echo $GLOBALS['setting']['site_money_name'];?> <a href="javascript:GetGuess_Dialog('<?php echo $info['goods_id'];?>');"><span class="blue_under">投入详情</span></a></div>
<?php } else{?>
<!--正在进行-->
<div class="g_more">当前总投入：<span class="yellow66"><strong id="totalb"><?php echo number_format($guess['guesstotal']);?></strong></span><?php echo $GLOBALS['setting']['site_money_name'];?> <?php if($guess['login']){?><span style="" class="blue_under" id="link_view"><a href="javascript:GetGuess_Dialog('<?php echo $info['goods_id'];?>');">投入详情</a></span><?php }?></div>
<!--投入区结束-->
<?php }?>
</div>
<!--提示区-->
<?php if($guess['win']){
?>
<!--结束-->
<div class="jc_end">竞猜已结束！<span class="g_color1"><?php echo $guess['wintype']=='single'?'单':'双';?></span>获胜！

</div>
<?php } else{?>
<!--正在进行-->
<span class="gametxt"><span class="imgbg"></span>功夫再高，也怕失足！</span>
<!--提示区结束-->
<?php }?>
</div>

<!--会员区-->
<?php if(!$guess['login']){
if($guess['win'] == 1){
?>
<!--已结束未登录-->
<div class="bottom_bg_end"><div class="cj_left_bg"></div>
<div class="game_in2"> <span class="grayB4">您还未登录</span><span class="grayB4">，<a href="javascript:Login_Dialog();"><span class="blue">登录</span></a>查看您的投入详情!</span> </div>
</div>
<?php } else{?>
<!--正在进行未登录-->
<div class="bottom_bg">
<div class="game_in2"> <span class="grayB4">请您先<a target="_blank" href="<?php echo url('user','register');?>"><span class="blue_under">注册</span></a>或者<a href="javascript:Login_Dialog();"><span class="blue_under">登录</span></a>再参与游戏!</span>
  <div class="clear"></div>
</div></div>
<?php }
} else{?>
<?php if($guess['win'] == 1){?>
  <?php if($guess['you'] == 0){?>
  <!--已结束未参与-->
  <div class="bottom_bg_end">                                                 			
  <div class="cj_left_bg"></div>
  <div class="game_in2 grayB4">您未参与本期竞猜游戏！</div>
  </div>
  <?php } else { ?>
  <div class="bottom_bg_end">
  <div class="game_info">
  我的投入<br />
  <span class="font14"><strong class="g_color1">单</strong> <span id="mysingle"><?php echo $guess['my_single'];?></span><?php echo $GLOBALS['setting']['site_money_name'];?></span> 
  <span class="font14 lmar10"><strong class="g_color2">双</strong> <span id="mydouble"><?php echo $guess['my_double'];?></span><?php echo $GLOBALS['setting']['site_money_name'];?></span> 
  </div>
  <div class="jc_win"> <span class="yellow66"><strong class="font20"><?php echo $guess['giveusermoney'];?></strong> <?php echo $GLOBALS['setting']['site_money_name'];?>!</span> </div>
  </div>
  <?php }?>
<?php } else {?>
<!--已经登录,显示规则-->
<div class="bottom_bg">
<div class="game_info2">
  <div class="dan_shuang jc_bor">
	<div class="jc_button">
	  <span id="mysingle"><?php echo $guess['my_single'];?></span><?php echo $GLOBALS['setting']['site_money_name'];?>
	  <div class="leftbg"></div>
	  <div class="rightbg"></div>
	</div>
	<div class="jc_txt"> <input type="button" onmouseout="this.className='jc_dan'" onmousemove="this.className='jc_dan_over'" class="jc_dan" onclick="SetGuess_Dialog('<?php echo $info['goods_id'];?>','single');">
	  <div class="centerbg"></div>
	</div>
  </div>
  <div class="dan_shuang">
	<div class="jc_button"> <span id="mydouble"><?php echo $guess['my_double'];?></span><?php echo $GLOBALS['setting']['site_money_name'];?>
	  <div class="leftbg"></div>
	  <div class="rightbg"></div>
	</div>
	<div class="jc_txt"> <input type="button" onmouseout="this.className='jc_shuang'" onmousemove="this.className='jc_shuang_over'" class="jc_shuang" onclick="SetGuess_Dialog('<?php echo $info['goods_id'];?>','double');">
	  <div class="centerbg"></div>
	</div>
  </div>
</div>
</div>
<!--会员区结束-->

<?php }}}?>