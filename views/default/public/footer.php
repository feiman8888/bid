<div id="toast" class="toast"></div>
<!--帮助中心开始-->
<div class="container980">
  <div class="help_center borD6 mar10">
    <dl>
      <span class="imghelp"></span>
      <dt><strong>新手上路</strong></dt>
      <dd><a href="<?php echo url('article','help',array('id'=>5));?>" target="_blank">用户注册</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>6));?>" target="_blank">用户验证</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>7));?>" target="_blank">账户管理</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>17));?>" target="_blank">服务协议</a></dd>
    </dl>
    <dl>
      <span class="img1"></span>
      <dt><strong>关于竞拍</strong></dt>
      <dd><a href="<?php echo url('article','help',array('id'=>10));?>" target="_blank">竞拍流程</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>9));?>" target="_blank">竞拍规则</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>11));?>" target="_blank">竞拍秘籍</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>12));?>" target="_blank">购买<?php echo $GLOBALS['setting']['site_money_name'];?></a></dd>
    </dl>
    <dl>
      <span class="img2"></span>
      <dt><strong>服务保证</strong></dt>
	  <dd><a href="<?php echo url('article','help',array('id'=>16));?>" target="_blank">放心承诺</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>18));?>" target="_blank">隐私条款</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>19));?>" target="_blank">信任与安全</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>21));?>" target="_blank">常见问题</a></dd>
    </dl>
    <dl>
      <span class="img3"></span>
      <dt><strong>关于我们</strong></dt>
      <dd><a href="<?php echo url('article','help',array('id'=>22));?>" target="_blank">关于我们</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>23));?>" target="_blank">联系我们</a></dd>
      <dd><a href="<?php echo url('index','guestbook');?>" target="_blank">建议\投诉</a></dd>
      <dd><a href="<?php echo url('article','help',array('id'=>23));?>" target="_blank">客服服务</a></dd>
    </dl>
    <div class="public_corner public_topleft"></div>
    <div class="public_corner public_topright"></div>
    <div class="public_corner help_bottomleft"></div>
    <div class="public_corner help_bottomright"></div>
  </div>
  <div class="clear"></div>
</div>
<!--帮助中心结束-->
<div class="newbottom">
  <p class="footertxt"><span class="left ft1"><?php echo $GLOBALS['setting']['site_copyright'];?>&nbsp;&nbsp;<?php echo $GLOBALS['setting']['site_benai'];?>&nbsp;&nbsp;电话：<?php echo $GLOBALS['setting']['site_tel'];?>&nbsp;&nbsp;服务邮箱：<?php echo $GLOBALS['setting']['site_email'];?> <?php echo $GLOBALS['setting']['site_tongji'];?> </span><span class="right ft2"><a onclick="window.scroll(0,0);"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/head/top.gif" class="img2bottom" alt="返回顶部" style="cursor:pointer" /></a> <a href="<?php echo SITE_ROOT;?>"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/head/home.gif" class="img2bottom" alt="返回首页" /></a></span></p>
  <div class="public_corner bottom_bottomleft"></div>
  <div class="public_corner bottom_bottomright"></div>
  <div class="clear"></div>
   <ul class="bot_zj">
 	<li><a href="http://www.ectrustchina.com/seal/splash/1002188.htm" target="_blank"><img src="https://gss3.bdstatic.com/-Po3dSag_xI4khGkpoWK1HF6hhy/baike/s%3D220/sign=6a5ca0ded143ad4ba22e41c2b2035a89/060828381f30e9240bf6c14a4c086e061d95f77f.jpg" alt="中国电子商务诚信单位" /></a></li>
	<li class="zjt"><a href="http://www.315-ts.com/tousu2.php" target="_blank"><img src="https://gss2.bdstatic.com/-fo3dSag_xI4khGkpoWK1HF6hhy/baike/s%3D220/sign=de44d720ad4bd11300cdb0306aaea488/29381f30e924b899bfd04a6b6e061d950b7bf6f0.jpg" alt="315消费电子投诉中心"/></a></li>
	<li class="zjt"><a href="http://www.alipay.com/" target="_blank"><img src="https://gss3.bdstatic.com/7Po3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268/sign=07cab45a367adab43dd01c45b3d5b36b/908fa0ec08fa513dd9446bac3d6d55fbb3fbd9d2.jpg" alt="支付宝特约商家" /></a></li>
   </ul>
</div>

</div>

<div id="dialog" style="display:none;"><IMG SRC="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/032.gif" WIDTH="32" HEIGHT="32" BORDER="0" ALT=""></div>

<div style="z-index:100;background-color: rgb(0, 0, 0); visibility: visible; display: none; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; width:100%;filter:alpha(opacity=50); opacity: 0.5; -moz-opacity:0.5;" id="blurdiv"></div>
<!--警告-->
<div id="dialog_wran" style="display:none;"><div class="dialogdiv01">
<span class="call_imgbg"></span>
	<div class="call_time">您超过5分钟未进行任何操作。<br>“继续”或关闭窗口则会激活。</div>
	<div class="call_time_button" onclick="time_continue();"><input type="button" value="继 续" onclick="time_continue();" onmouseout="this.className='layer_button mar10 left'" onmousemove="this.className='layer_button_over mar10 left'" class="layer_button mar10 left" name=""></div>
	<div class="clear"></div>
</div></div>
<!--警告-->
<!--结束-->
<div id="dialog_complete" style="display:none;"><div class="dialogdiv01">
<div class="refresh"></div>
<div class="refreshtxt">点击“确定”按钮则刷新页面<br>点击“取消”按钮则保留当前页面</div>
<div class="refresh_button"><input type="button" value="确 定" onclick="javascript:window.location.reload();" onmouseout="this.className='layer_button'" onmousemove="this.className='layer_button_over'" class="layer_button" name=""><input type="button" onclick="javascript:Dialog_close();" onmouseout="this.className='layer_button lmar10'" onmousemove="this.className='layer_button_over lmar10'" value="取 消" class="layer_button lmar10" name=""></div>
	<div class="clear"></div>
</div></div>
<!--结束-->
</body>
</html>