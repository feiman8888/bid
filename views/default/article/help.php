<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/help.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/imgscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>


<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">
 <!--左侧导航开始-->
 <div class="help t1"></div>
  <div class="help_left">
    <dl id="nav">
     <?php if($group){foreach($group as $k=>$v){?><dt><strong><?php echo $k;?></strong></dt>
	 <?php foreach($v as $val){?>
	 <dd><a href='<?php echo $val['url'];?>'><?php echo $val['title'];?></a></dd>
	 <?php }?>
	 <?php }}?>
    </dl>
    <div class="corner topleft" ></div>
    <div class="corner topright"></div>
    <div class="corner bottomleft"></div>
    <div class="corner bottomright"></div>
  </div>
   <!--左侧导航结束-->
    <div class="t1 help_right_position"><span class="left"><strong><a href="/">首页</a> > <a href="<?php echo url('article','help');?>">帮助中心</a> >&nbsp;<?php echo $article['title'];?></strong></span></div>

	
  <div class="help_right">
   <?php echo $article['content'];?>
  
   <div class="clear"></div>
</div>

<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->