<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/article.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>
  <div class="container980" style="padding-bottom:10px;">
    <div class="bulletinlist">
      <ul><?php if($articlelist){foreach($articlelist as $k=>$v){?>
                <li> <span class="left"><a href="<?php echo url('article','view',array('id'=>$v['id']));?>" target="_blank"><?php echo $v['title'];?></a></span> <span class="right time"><?php echo date('Y-m-d H:i:s',$v['updatetime']);?></span> </li>
<?php }}?>
  </ul>
<div class="page"><?php echo $pageinfo;?></div>
    </div>

  </div>


<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->