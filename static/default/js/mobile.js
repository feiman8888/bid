//是否是手机
function is_mobile(numstr)
{
var reg = new RegExp("^1[0-9]{10}$");
return reg.test(numstr);
}
$(document).ready(function(){

var $txtCheckCode = $("#txt_checkcode");
var secondSpan = 100;			//间隔秒数
var waitSecond = secondSpan;	//间隔秒数
var setInt = null;				//计时器
var firstReq = true;

//显示按钮文本
var display = function(){
	
	if(waitSecond > 0){
		waitSecond--;
		$('#btn_send').attr("value","重新发送(" + waitSecond + ")");
	}else{
		$('#btn_send').removeAttr("disabled").attr("value","重新发送");
		//停止倒计时
		clearInterval(setInt);
		waitSecond = secondSpan;
	}
};

//获取短信验证码
var funGetCheckCode = function(){
	$.post(site_root+"?con=user&act=sendsms", {},
		function(res){
            if(res.code == 0){
              data = res.data;
              if(data.datastatus=='success'){
                  //获取成功
                  if(!firstReq){
                      $("#sp_send").text("手机验证码发送成功,请注意查收");	
                      $("#one_send").css("display","none");
                      $('#btn_send').css("display","none");
                      $('#five_send').css("display","block");
                  }else{
                      firstReq = false;
                  }
              }
              else if(data.datastatus=='errormobile')
              {
                //发送失败
                $("#sp_send").text('手机号码错误');
              }
              else if(data.datastatus=='nologin')
              {
                      $("#sp_send").text('未登录');
              }
              else if(data.datastatus=='failed')
              {
                      $("#sp_send").text('发送错误,错误代码为：'+data.code);
              }
              else if(data.datastatus=='limit'){
                waitSecond = 0;
                $('#main_send').css("display","none");
                $('#five_send').css("display","block");
                $('#forbid_send').css("display","block");
                $('#mobiletipsimg').attr("className","mobiletipsimg01");
                $("#showtip").css("color","red").text("24小时内发送的验证短信超过次数限制,请稍后再试。");
              }
            }
		},"json");
};

//点击发送验证按钮
$('#btn_send').click(function(){
	$(this).attr('disabled','disabled');
	funGetCheckCode();
	setInt = setInterval(display,1000);
});

$txtCheckCode.focus(function(){
	$(this).parent().parent().addClass("bgHeighLight");
}).blur(function(){
	$(this).parent().parent().removeClass("bgHeighLight");
});

//提交验证
$("form").submit(function(){
	if($txtCheckCode.val() == ""){
		$txtCheckCode.focus();
		return false;
	}else{
		$("#div_CheckCodeMsg").html("");
	}
});

//修改
$("#btnEditMobile").click(function(){
	$.post(site_root+"/tools/changemobile.php", {"newmobile":$('#txt_newMobilePhone').val()},
		function(data){
			if(data.datastatus=='success'){
				//获取成功
					$("#mobile").text($('#txt_newMobilePhone').val());
					$('#spNewMobile').hide();
				}
				else if(data.datastatus=='errormobile'){
					alert('请正确填写11位手机号码');
				}
				else if(data.datastatus=='likemobile')
				{
					alert('此号码已经注册过了');
				}
				else if(data.datastatus=='nologin')
				{
					alert('请先登录');			
				}
		},"json");
});

$("#spChangeMobile").click(function(){
	var cmd = $(this).html();
	if(cmd == "更换号码"){
		$(this).html("取消更换");
		$('#spNewMobile').show();
		$('#txt_newMobilePhone').select();
		$('#hd_ChangeMobile').val("1");
	}else if(cmd == "取消更换"){
		$(this).html("更换号码");
		$('#spNewMobile').hide();
		$('#txt_newMobilePhone').val("");
		$('#hd_ChangeMobile').val("0");
	}		
});

$("#btnCancel").click(function(){
	$("#spChangeMobile").click();
});

			
});