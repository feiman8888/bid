// 导航栏配置文件
var outlookbar=new outlook();
var t;

t=outlookbar.addtitle('项目设置','项目管理',1)
outlookbar.additem('项目设置',t,'?con='+adminpath+'&act=setting&type=goods')

t=outlookbar.addtitle('类别管理','项目管理',1)
outlookbar.additem('类别管理',t,'?con='+adminpath+'&act=cate')

t=outlookbar.addtitle('竞拍管理','项目管理',1)
outlookbar.additem('添加竞拍项目',t,'?con='+adminpath+'&act=goodsmodify')
outlookbar.additem('正在进行的竞拍',t,'?con='+adminpath+'&act=goods&start=1')
outlookbar.additem('未审核竞拍',t,'?con='+adminpath+'&act=goods&type=nopass')
outlookbar.additem('完成竞拍项目',t,'?con='+adminpath+'&act=complete')
outlookbar.additem('初始化竞拍项目',t,'?con='+adminpath+'&act=cache')


t=outlookbar.addtitle('积分商城','项目管理',1)
outlookbar.additem('添加积分商品',t,'?con='+adminpath+'&act=giftmodify')
outlookbar.additem('积分商品管理',t,'?con='+adminpath+'&act=gift')

t=outlookbar.addtitle('交易管理','项目管理',1)
outlookbar.additem('竞价统计',t,'?con='+adminpath+'&act=completelist')
outlookbar.additem('竞价明细',t,'?con='+adminpath+'&act=buylog')

//t=outlookbar.addtitle('秒杀商城','项目管理',1)
//outlookbar.additem('添加秒杀项目',t,'?con='+adminpath+'&act=readsecondmodify')
//outlookbar.additem('秒杀项目管理',t,'?con='+adminpath+'&act=readsecond')

t=outlookbar.addtitle('订单管理','项目管理',1)
outlookbar.additem('订单管理',t,'?con='+adminpath+'&act=order')
outlookbar.additem('晒单管理',t,'?con='+adminpath+'&act=ordershow')

t=outlookbar.addtitle('基本设置','系统设置',1)
outlookbar.additem('站点信息',t,'?con='+adminpath+'&act=setting&type=site')
outlookbar.additem('邮箱/短信设置',t,'?con='+adminpath+'&act=setting&type=email')
outlookbar.additem('SEO设置',t,'?con='+adminpath+'&act=setting&type=seo')
outlookbar.additem('模板设置',t,'?con='+adminpath+'&act=setting&type=template')

outlookbar.additem('支付方式设置',t,'?con='+adminpath+'&act=setting&type=payment')

t=outlookbar.addtitle('充值卡管理','系统设置',1)
outlookbar.additem('充值卡管理',t,'?con='+adminpath+'&act=charge')

t=outlookbar.addtitle('文章管理','系统设置',1)
outlookbar.additem('添加文章',t,'?con='+adminpath+'&act=articlemodify')
outlookbar.additem('文章管理',t,'?con='+adminpath+'&act=article')
outlookbar.additem('文章分类管理',t,'?con='+adminpath+'&act=article_cate')


t=outlookbar.addtitle('链接管理','系统设置',1)
outlookbar.additem('友情链接管理',t,'?con='+adminpath+'&act=link')

t=outlookbar.addtitle('会员管理','会员管理',1)
outlookbar.additem('普通会员',t,'?con='+adminpath+'&act=normaluser')
outlookbar.additem('管理员',t,'?con='+adminpath+'&act=adminuser')
outlookbar.additem('商家',t,'?con='+adminpath+'&act=selluser')
outlookbar.additem('系统会员',t,'?con='+adminpath+'&act=autouser')

outlookbar.additem('添加会员',t,'?con='+adminpath+'&act=usermodify')


t=outlookbar.addtitle('互动管理','会员管理',1)
outlookbar.additem('留言管理',t,'?con='+adminpath+'&act=guestbook')
outlookbar.additem('晒单评论管理',t,'?con='+adminpath+'&act=comment')
outlookbar.additem('聊天管理',t,'?con='+adminpath+'&act=chat')
outlookbar.additem('短信订阅',t,'?con='+adminpath+'&act=smslist&status=2')

t=outlookbar.addtitle('交易管理','会员管理',1)
outlookbar.additem('资金记录',t,'?con='+adminpath+'&act=moneylog')

outlookbar.additem('收货地址',t,'?con='+adminpath+'&act=address')

t=outlookbar.addtitle('邮件管理','推广管理',1)
outlookbar.additem('邮件群发',t,'?con=email')

t=outlookbar.addtitle('广告管理','推广管理',1)
outlookbar.additem('广告管理',t,'?con='+adminpath+'&act=ad')

t=outlookbar.addtitle('代金券管理','推广管理',1)
outlookbar.additem('代金券管理',t,'?con='+adminpath+'&act=card')		
outlookbar.additem('批量生成代金券',t,'?con='+adminpath+'&act=createcard')

t=outlookbar.addtitle('常用操作','管理首页',1)
outlookbar.additem('添加竞拍项目',t,'?con='+adminpath+'&act=goodsmodify')
outlookbar.additem('竞拍项目管理',t,'?con='+adminpath+'&act=goods')
outlookbar.additem('订单管理',t,'?con='+adminpath+'&act=order')
