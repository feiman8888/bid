<?php
/*
	[Bidcms.com!] (C)2009-2011 Bidcms.com.
	This is NOT a freeware, use is subject to license terms
	$author limengqi
	$Id: userclass.php 2016-03-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class user_base_class extends model
{
	public $table='user';
	public $definition=array(
		'primary'=>'uid'
	);
    public $fields = array(
    	'uid'=>'','username'=>'','email'=>'','pwd'=>'','money'=>'','hash'=>'','usertype'=>'','usercard'=>'','score'=>'','ip'=>'','address'=>'','mobile'=>'','friendid'=>'','updatetime'=>'','lastlogin'=>'','lastip'=>'','verification'=>'','mobilev'=>'','mobilecount'=>'','province'=>'','city'=>'','area'=>'','cusaddress'=>'','realname'=>'','identity'=>'','ucid'=>'','sex'=>'','telphone'=>'','friendreturn'=>'','avatargive'=>''
    );
    public function update_cache_data($data){
      global $cache;
      if($GLOBALS['userinfo']['uid']>0){
        $key = 'user'.($GLOBALS['userinfo']['uid']%10).':'.$GLOBALS['userinfo']['uid'];
        foreach($data as $k=>$v){
          $GLOBALS['userinfo'][$k] = $v;
        }
        $cache->set($key,$GLOBALS['userinfo']);
        return $this->where('uid',$GLOBALS['userinfo']['uid'])->update_data($data);
      }
      return false;
    }
}
