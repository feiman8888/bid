<?php
/*
	[Bidcms.com!] (C)2009-2011 Bidcms.com.
	This is NOT a freeware, use is subject to license terms
	$author limengqi
	$Id: userclass.php 2016-03-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class article_base_class extends model
{
	public $table='article';
	public $definition=array(
		'primary'=>'id',
		'join'=>'cateid'
	);
    public $fields = array(
    	'id'=>'','cateid'=>'','title'=>'','content'=>'','updatetime'=>'','custom_group'=>''
    );
}
