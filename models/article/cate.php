<?php
/*
	[Bidcms.com!] (C)2009-2011 Bidcms.com.
	This is NOT a freeware, use is subject to license terms
	$author limengqi
	$Id: userclass.php 2016-03-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class article_cate_class extends model
{
	public $table='article_cate';
    public $fields = array(
    	'id'=>'','catename'=>'','issystem'=>'','cate_en'=>''
    );
    public function get_cache_list(){
      global $cache;
      $article_cate = $cache->get('article_cate');
      if(empty($article_cate)){
        $article_cate = $this->get_page(array('index'=>'id'));
        $cache->set('article_cate',$article_cate,86400);
      }
      return $article_cate;
    }
}
