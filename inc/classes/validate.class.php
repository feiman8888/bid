<?php
/*
	[Bidcms.Com!] (C)2009-2011 Bidcms.Com.
	This is NOT a freeware, use is subject to license terms

	$Id: model.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class validate {
	public function required($v,$param){
		if(!empty($v)){
			return true;
		}
		return '不能为空';
	}

	public function between($v,$param){
		$s = explode('-',$param);
		$min = intval(isset($s[0])?$s[0]:0);
		$max = intval(isset($s[1])?$s[1]:10);
		if($v>=$min && $v<=$max){
			return true;
		}
		return '值应该大于'.$min.'，小于'.$max;
	}
}

?>