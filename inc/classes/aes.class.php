<?php
class Aes {

    private $hex_iv = '*0o@!1&^oiO1l*#$'; # converted JAVA byte code in to HEX and placed it here

    private $key = 'wpx@!1&^oI45*#@$'; #Same as in JAVA

    function __construct() {
    }

    function encrypt($str) {

        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

        mcrypt_generic_init($td, $this->key, $this->hexToStr($this->hex_iv));

        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

        $pad = $block - (strlen($str) % $block);

        $str .= str_repeat(chr($pad), $pad);

        $encrypted = mcrypt_generic($td, $str);

        mcrypt_generic_deinit($td);

        mcrypt_module_close($td);

        return base64_encode($encrypted);

    }

    function decrypt($code) {

        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

        mcrypt_generic_init($td, $this->key, $this->hexToStr($this->hex_iv));

        $str = mdecrypt_generic($td, base64_decode($code));

        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

        mcrypt_generic_deinit($td);

        mcrypt_module_close($td);

        return $this->strippadding($str);

    }

    public function encode($input,$key='')
    {
		$key=!empty($key)?$key:$this->key;
		$input=$this->hexToStr($input);
		//$key=hash('sha256', $key, true);
		$data = openssl_encrypt($input, 'AES-128-CBC', $key, OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, $this->hex_iv);
        return $data;
    }

    public function decode($input,$key='')
    {
		$key=!empty($key)?$key:$this->key;
		$input=$this->hexToStr($input);
		//$key=hash('sha256', $key, true);
        $decrypted = openssl_decrypt($input, 'AES-128-CBC', $key, OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, $this->hex_iv);
        return $decrypted;
    }
	
    /*

      For PKCS7 padding

     */

    private function addpadding($string, $blocksize = 16) {

        $len = strlen($string);

        $pad = $blocksize - ($len % $blocksize);

        $string .= str_repeat(chr($pad), $pad);

        return $string;

    }

    private function strippadding($string) {

        $slast = ord(substr($string, -1));

        $slastc = chr($slast);

        $pcheck = substr($string, -$slast);

        if (preg_match("/$slastc{" . $slast . "}/", $string)) {

            $string = substr($string, 0, strlen($string) - $slast);

            return $string;

        } else {

            return false;

        }

    }

    function hexToStr($hex)
    {

        $string='';

        for ($i=0; $i < strlen($hex)-1; $i+=2)

        {

            $string .= chr(hexdec($hex[$i].$hex[$i+1]));

        }

        return $string;
    }
	function strToHex($string)//�ַ���תʮ������
    { 
        $hex="";
        $tmp="";
        for($i=0;$i<strlen($string);$i++)
        {
            $tmp = dechex(ord($string[$i]));
            $hex.= strlen($tmp) == 1 ? "0".$tmp : $tmp;
        }
        $hex=strtoupper($hex);
        return $hex;
    }
}
