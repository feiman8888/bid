<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: mysql.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class bidcms_mysql {

	var $version = '';
	var $querynum = 0;
	var $link = null;

	function connect($dbhost, $dbuser, $dbpw, $dbname = '', $timeout = 5, $halt = TRUE, $dbcharset2 = '') {
		$host=explode(":",$dbhost);
		$dsn="mysql:host=".$host[0].";port=".(isset($host[1])?$host[1]:3306).";dbname=".$dbname;
		
		try {
			$this->link =new PDO($dsn, $dbuser, $dbpw, array(PDO::ATTR_TIMEOUT => $timeout, PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
		} catch (PDOException $e) {
			$this->halt('Can not connect to MySQL server'.$e->getMessage());
		}
		// UTF-8 support
		if ($this->link->exec('SET NAMES \'utf8\'') === false){
			$this->halt('PrestaShop Fatal error: no utf-8 support. Please check your server configuration.');
		}


	}

	function fetch_array($query, $result_type = MYSQL_ASSOC) {
		return mysql_fetch_array($query, $result_type);
	}

	function fetch_first($sql) {
		return $this->fetch_array($this->query($sql));
	}

	function result_first($sql) {
		return $this->result($this->query($sql), 0);
	}

	function query($sql, $type = '') {
		return $this->link->query($sql);
	}

	function affected_rows() {
		return rowCount();
	}

	function error() {
		return $this->link->errorInfo();
	}

	function errno() {
		return $this->link->errorCode();
	}

	function result($query, $row = 0) {
		$query = @mysql_result($query, $row);
		return $query;
	}

	function num_rows($query) {
		$query = mysql_num_rows($query);
		return $query;
	}

	function num_fields($query) {
		return mysql_num_fields($query);
	}

	function free_result($query) {
		return mysql_free_result($query);
	}

	function insert_id() {
		return ($id = mysql_insert_id($this->link)) >= 0 ? $id : $this->result($this->query("SELECT last_insert_id()"), 0);
	}

	function fetch_row($query) {
		$query = mysql_fetch_row($query);
		return $query;
	}

	function fetch_fields($query) {
		return mysql_fetch_field($query);
	}
	function list_fields($query)
	{
		$fields=array();
		$columns = mysql_num_fields($query);
		for ($i = 0; $i < $columns; $i++) {
			$fields[]=mysql_field_name($query, $i);
		} 
		return $fields;
	}
	
	function version() {
		if(empty($this->version)) {
			$this->version = mysql_get_server_info($this->link);
		}
		return $this->version;
	}

	function close() {
		unset($this->link);
	}
	function __destruct()
	{
		$this->close();
	}
	function halt($message = '', $sql = '') {
		$timestamp=time();
		if($message) {
		$errmsg = "<b>BIDCMS info</b>: $message<br/>";
		}
		$errmsg .= "<b>Time</b>: ".gmdate("Y-n-j g:ia", $timestamp)."<br/>";
		$errmsg .= "<b>Script</b>: ".$_SERVER['PHP_SELF']."<br/>";
		if($sql) {
			$errmsg .= "<b>SQL</b>: ".htmlspecialchars(str_replace($GLOBALS['bidcmstable_prefix'],'',$sql))."<br/>";
		}
		$errmsg .= "<b>Error</b>:  ".$this->error()."<br/>";
		$errmsg .= "<b>Errno.</b>:  ".$this->errno();

		echo $errmsg;
		//exit($errmsg);
	}
}

?>
